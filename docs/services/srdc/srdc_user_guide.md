---
title: SRDC User Guide
keywords: SRDC User Guide
tags: [SRDC User Guide]
---

# User Guide Overview

!!! summary
    This page provides a general overview of how to use the SRDC Virtual Machine (VM) and High Performance Computing (HPC) services. Details are limited to protect data security on the platform, but an information handbook will be provided to those approved for an SRDC allocation.

## Obtaining an Account

Interested parties can schedule a consultation with SRDC consultants <a href="mailto:brc@berkeley.edu">brc@berkeley.edu</a> to determine data security level and discuss data security agreements. Upon approval, an SRDC allocation will be assigned to the participating faculty member under their Faculty Computing Allowance or Condo. 

## Passwords 

Once you have been approved for work on the SRDC, your CalNet credentials will give you access to SRDC through a private portal. 
 
## Logging in
 
The SRDC is accessible via a secure gateway with multi-factor authentication. Once logged on to the portal, proceed to your service, either HPC or VM.
 
## Transferring Data
 
The SRDC team works with researchers to facilitate secure file and data ingress and egress with SRDC services. We support a variety of data transfer options across platforms, including the use of Globus. Note that Windows VM, Linux VM, and SRDC have unique data transfer requirements outlined in the information handbook provided to individuals onboarded to the SRDC.

## Computing

=== "HPC"
    
    #### Installing Software and Running Jobs

    SRDC, like Savio, provides a variety of software installed by the system administrators and SRDC consultants, including compilers, interpreters, and tools for machine learning, statistical analysis, data visualization, bioinformatics, computational biology, and other sciences. Berkeley’s [Savio documentation](https://docs-research-it.berkeley.edu/services/high-performance-computing/user-guide/software/accessing-software/) provides details on available software and modules.
    
    When you initially log into a cluster, you'll land on a login node. Here you can edit scripts, compile programs and use the SLURM job scheduler to submit jobs to run on one or more of the cluster's many compute nodes. Berkeley’s [Savio documentation](https://docs-research-it.berkeley.edu/services/high-performance-computing/user-guide/) provides details on how to submit HPC computing jobs.

=== "VM"
    
    #### Interactive Computing

    SRDC virtual machines behave like standard Windows or Linux interactive computing environments. VMs are made available for researchers who require, or prefer, a specific operating system. Software modules, libraries, and visualization tools will be pre-installed by the SRDC support team.






















