---
title: SRDC System Overview
keywords: SRDC System Overview
tags: [SRDC System Overview]
---

# System Overview

The SRDC Platform supports data storage, management, and computation designed for compute-intensive research on large and sensitive data. SRDC features dedicated infrastructure, housed in the secure campus data center, as well as personnel to support research and enforce the necessary privacy and security policies and procedures.

## Technology & Equipment
* High performance computing, accessible to researchers under their Faculty Computing Allowance. Researchers may purchase additional HPC nodes to add to the system.
* Windows and Linux virtual machines, accessible to researchers under their Faculty Computing Allowance. Researchers may purchase additional virtual machine hardware to add to the system.
* A dedicated, large-scale, and high performance storage system. Researchers may purchase additional storage to add to the system.

## SRDC High-Performance Computing (HPC) Cluster Overview

The SRDC HPC cluster offers a CPU compute pool comprising 40 nodes, each featuring Intel Xeon Gold 6230 CPUs, 364 GB of RAM, 20 cores running at 2.1 GHz, and support for 16 floating-point operations per clock cycle. Job submissions to these nodes are under the partition name of “srdc”.

#### GPUs on SRDC HPC

Alongside the CPU-only nodes, the SRDC cluster also features 48 GeForce GTX 1080 Ti GPUs distributed across 12 nodes. Each node contains 8 CPUs and 4 GPUs with a total of 64 GB of CPU RAM and 11 GB of GPU RAM per GPU. SRDC’s GPUs allow for concurrent operations, memory mapping, and coordinated kernel launches.

### SRDC HPC Hardware Configuration

<table id="hardware-config-table" align="center" border="1" cellspacing="0">

<tbody>

<tr>
<th style="text-align:center">Partition</th>
<th style="text-align:center">Nodes</th>
<th style="text-align:center">Node List</th>
<th style="text-align:center">CPU Model</th>
<th style="text-align:center"># Cores / Node</th>
<th style="text-align:center">Memory / Node</th>
<th style="text-align:center">Infiniband</th>
<th style="text-align:center">Specialty</th>
<th style="text-align:center">Scheduler Allocation</th>
</tr>

<tr>
<td style="text-align:center; vertical-align:middle">srdc</td>
<td style="text-align:center; vertical-align:middle">40</td>
<td style="text-align:center; vertical-align:middle">n00[00-39].srdc.srdc0</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon Gold 6230</td>
<td style="text-align:center; vertical-align:middle">20</td>
<td style="text-align:center; vertical-align:middle">364 GB</td>
<td style="text-align:center; vertical-align:middle">4x EDR</td>
<td style="text-align:center; vertical-align:middle">-</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr>

<tr>
<td style="text-align:center; vertical-align:middle">srdc_GTX1080TI</td>
<td style="text-align:center; vertical-align:middle">12</td>
<td style="text-align:center; vertical-align:middle">n00[40-51].srdc.srdc0</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon ES-2623</td>
<td style="text-align:center; vertical-align:middle">8</td>
<td style="text-align:center; vertical-align:middle">64 GB</td>
<td style="text-align:center; vertical-align:middle">4x FDR</td>
<td style="text-align:center; vertical-align:middle">4x GeForce GTX 1080 Ti per Node<br>(11 GB of GPU RAM per GPU)<br>(48 GPUs total)</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr>

</tbody></table>

## SRDC Virtual Machines (VM) Overview

SRDC also offers Linux and Windows VM computing environments for workflows that
necessitate a graphical user interface (GUI) or Windows-specific software. SRDC
VMs are hosted on one of eight servers running [VMware ESXi bare-metal
hypervisors](https://www.vmware.com/products/esxi-and-esx.html) to flexibly meet
our VM users' computing needs. The table below provides additional details.

### SRDC VM Hardware Configuration

<table id="hardware-config-table" align="center" border="1" cellspacing="0">

<tbody>

<tr>
<th style="text-align:center">Servers</th>
<th style="text-align:center">Hypervisor</th>
<th style="text-align:center">CPU Model</th>
<th style="text-align:center"># Cores / Host</th>
<th style="text-align:center">Memory / Host</th>
<th style="text-align:center">Storage</th>
<th style="text-align:center">Interconnect</th>
<th style="text-align:center">Specialty</th>
</tr>

<tr>
<td style="text-align:center; vertical-align:middle">8</td> 
<td style="text-align:center; vertical-align:middle">VMware ESXi</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon Gold 6254</td>
<td style="text-align:center; vertical-align:middle">72 (shared)</td>
<td style="text-align:center; vertical-align:middle">766 GB (shared)</td>
<td style="text-align:center; vertical-align:middle"><a href="https://technology.berkeley.edu/storage">bIT Performance and Utility tiers</a> (managed by SRDC)</td>
<td style="text-align:center; vertical-align:middle">32GB Fibre Channel</td>
<td style="text-align:center; vertical-align:middle">Graphical user interface<br>Linux VM<br>Windows VM</td>
</tr>

</tbody></table>


## Storage on SRDC

Storage allocations for project data are sized based on project needs and storage availability for Linux and Windows SRDC environments.

In Linux SRDC environments, users can utilize working scratch space, subject to a 12TB quota and a purge policy. Larger storage allocations may be available via MOU. Research groups can expand their storage on the GPFS if subject to space limitations. The cost of expansion is $25.8K for 100TB usable as of December 2023.

Please contact us at <a href="mailto:brc@berkeley.edu">brc@berkeley.edu</a> for a consultation.


## Platform Support & Research Facilitation

The SRDC is managed by system administrators and SRDC consultants, who monitor the OS and software, review security requirements and compliance, identify computational workflows, and help onboard new users. The Information Security Office works closely with SRDC staff to ensure security through monitoring and intrusion detection.
