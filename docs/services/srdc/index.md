---
title: Secure Research Data Computing
keywords: Secure Research Data
tags: [Secure Research Data Computing]
---

# SRDC at Berkeley

!!! summary
    The Secure Research Data and Compute (SRDC) Platform is a secure computing environment developed for researchers working with highly sensitive (P4) data. The SRDC includes high performance computing (HPC), computing on virtual machines (VMs) with Linux and Windows servers, and protected storage for both options. A combined approach of providing data management and computation support helps researchers integrate data management best practices into their larger research workflows. Through a coalition of partners from across campus, the SRDC Platform has been designed for use by a broad range of researchers, without compromising usability or performance.

## Mission

The need for data management and compute platforms for researchers working with sensitive data is campus-wide and growing; large, sensitive, and protected health and population datasets are increasingly utilized in research, across disciplines. These data must be securely transferred, stored, and accessed according to federal, state, and institutional requirements, emphasizing the need for a comprehensive and integrated approach, supported by UC Berkeley.

The SRDC Platform, developed and operated by RTL and a coalition of partners at Berkeley, is made available to researchers across campus, providing a broad range of environments for researchers facing unique computational and data security challenges. Based on the fundamental concepts of the [Faculty Compute Allowance](https://docs-research-it.berkeley.edu/services/high-performance-computing/getting-account/faculty-computing-allowance/) and the [Condo Model](https://docs-research-it.berkeley.edu/services/high-performance-computing/condos/condo-cluster-service/), the SRDC leverages Research IT’s existing experience developing and operating research computing services, such as Savio and AEoD, for the campus research community.

## SRDC Use Cases

It is important to understand appropriate data collection practices, data analysis tools, and data storage requirements. Researchers working with protected health information (PHI) including data protected under HIPAA, other highly sensitive human subjects data, controlled unclassified information (CUI), or other high risk data coming from third parties are strong candidates for SRDC. 

[Data classification and security](https://docs-research-it.berkeley.edu/services/research-data/data-classification-security/) can be determined through a consultation with an SRDC team member.

## SRDC use at Berkeley

SRDC has seen broad adoption across schools and disciplines at UC Berkeley, enabling researchers to work on projects otherwise inaccessible, given security limitations on other platforms and services. Researchers are using SRDC to study foster care, the impact of community doula care, genetic causes of T cell deficiencies, and many other research topics.

## Consulting & User Support

Research IT offers consulting for UC Berkeley researchers. We are here to help understand your needs, match you to appropriate resources, and help you get started using them. Our diverse team is made up of experts in both data and computing from a wide variety of domains, including SRDC. 

Consultants are available to help via email <a href="mailto:brc@berkeley.edu">brc@berkeley.edu</a> or by visiting our
SRDC consultants during [office hours](https://research-it.berkeley.edu/consulting).

Berkeley’s SRDC is supported by:

|      | Domain / Discipline |
| ------ | --------- |
| **Jason Christopher** | SRDC Lead |
| **Karen Fernsler** | HPC System Administrator |
| **Joleen Locanas** | Service Administrator |
| **Ronald Sprouse** | VM System Administrator |
| | |
| **BRC Domain Consultants** | Multidisciplinary Team |













