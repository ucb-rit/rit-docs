---
title: SRDC Condo
keywords: SRDC Condo
tags: [SRDC Condo]
---

# SRDC Condo Model

By becoming a Condo Partner, through purchasing and contributing compute nodes or storage to the SRDC Platform, researchers and their groups obtain priority access to resources equivalent to their contribution. The Condo Model helps make excess capacity available to other researchers when not in use. Details about the Condo Program follow those established for the [Savio Condo Cluster](https://docs-research-it.berkeley.edu/services/high-performance-computing/condos/condo-cluster-service/) program.
 
SRDC generally provides hardware support for five years, at the end of which hardware is retired and can be returned to the researcher or other arrangements can be discussed.

| SRDC Component | Component |
|---|---|
|SRDC HPC | Researchers can purchase HPC compute nodes in configurations that are supported by the SRDC Platform. Nodes can be purchased at cost and are maintained over a 5-year lifecycle. |
|SRDC VM| Researchers can purchase VM compute nodes in configurations that are supported by the SRDC Platform. Nodes can be purchased at cost and are maintained over a 5-year lifecycle.|
|SRDC Storage| Researchers can purchase additional storage to be added to the SRDC secure file storage system, available to both HPC and VM environments.|
