---
title: SRDC FCA
keywords: SRDC FCA
tags: [SRDC FCA]
---

# Faculty Computing Allowances
 
!!! summary
    Thanks to strategic support from campus leadership, the SRDC Platform provides a baseline amount of capacity at no cost to campus researchers via the Faculty Computing Allowance (FCA). As with other RTL services, researchers will be able to use grants or other funds to add compute and storage capacity to the system for their projects, and excess capacity will be made available to other researchers (i.e., the “Condo Model”). Please <a href="mailto:brc@berkeley.edu">email us</a> for more information.
 
!!! info "Definitions"
    * **Core Hour**: The use of a single processor core for one hour of wall-clock time.
    * **Service Unit (SU)**: One SU is equivalent to one “core hour”: that is, the use of one processor core, for one hour of wall-clock time, on one of SRDC’s standard, current generation compute nodes. For more information, please see the documentation page for [Savio Service Units](https://docs-research-it.berkeley.edu/services/high-performance-computing/user-guide/running-your-jobs/service-units-savio/).

## FCA on SRDC

| High-Performance Computing (HPC) | Virtual Machine (VM) |
|---|---|
|150K HPC Service Units (SU) per year.|SRDC Virtual Machines will be sized according to the needs of the project, up to 16 cores and 128GB RAM. More RAM may be available under special circumstances. Utilization of VM environments will be monitored, with a maximum of 150K core hours per year.|

Researchers are eligible for any combination of either SRDC service with their allocation of core hours (150K). If the annual allocation is used up within a year, projects can purchase additional SUs. At the end of the year, faculty can apply to renew their accounts with another 150K SU Faculty Allocation. Researchers needing more compute capacity are encouraged to consider the [SRDC Condo Program](https://docs-research-it.berkeley.edu/services/high-performance-computing/condos/).


| Faculty Allocation for Storage |
|----|
|Storage allocations for project data are sized based on project needs and storage availability for Linux and Windows SRDC environments.<br><br>In Linux SRDC environments, users can utilize working scratch space, subject to a 12TB quota and a purge policy. Larger storage allocations may be available via MOU. Research groups can expand their storage on the GPFS if subject to space limitations. The cost of expansion is $25.8K for 100TB usable as of December 2023.<br><br>Please contact us at brc@berkeley.edu for a consultation.|




