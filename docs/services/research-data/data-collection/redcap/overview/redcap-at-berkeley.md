---
title: REDCap at Berkeley
keywords: 
tags: [redcap]
---

# REDCap at Berkeley

!!! summary
    REDCap — short for Research Electronic Data Capture — is a secure web-based application to support online and offline data collection for research studies and operations across a variety of disciplines. REDCap at Berkeley is a campus-subsidized service available to all UC Berkeley researchers at no cost. This service is approved for highly sensitive data (classified as P4 using the Data Classification Standard). It is managed by Research IT and Research Data Management, and hosted by Research, Teaching, and Learning (RTL). Contact us and we’ll help determine if REDCap is the right fit for your research needs.

## Mission

Our mission is to support UC Berkeley research efforts through the use of a secure, user-friendly, and robust data collection and management platform.

Secure data collection is vital across research disciplines to protect and manage sensitive data. Equally important is the need for practical, adaptable, and easy-to-learn solutions for researchers. We are working to establish a highly collaborative, interdisciplinary network of REDCap users across the Berkeley campus and the greater University of California research communities.

## About REDCap

Developed by Vanderbilt University with researchers’ needs in mind, REDCap is a data collection platform used worldwide. Researchers can create instruments (data entry forms and surveys) for various study designs, including those requiring data from multiple sites as well as those that are longitudinal or repeating in nature, and can customize and manage projects directly, anytime, anywhere on desktops, laptops, tablets or smartphones. REDCap offers many other benefits, including the seamless process of exporting data in PDF or CSV formats. A web browser (e.g., Firefox, Safari and Chrome) is all you need to access REDCap; no software installation required.

<img alt="" src="../../../../../../img/Features_of_REDCap.png" style="height:619px; width:875px">

## Global Community

The global REDCap community is made up of collaborators in over one hundred countries.

<iframe
  width="600"
  height="450"
  style="border:0"
  loading="lazy"
  allowfullscreen
  referrerpolicy="no-referrer-when-downgrade"
  src="https://redcap.vanderbilt.edu/consortium/map_fullscreen.php">
</iframe>

## Access for UC Berkeley researchers

REDCap is offered as a campus subsidized service for all UC Berkeley Researchers. Research IT and Research Data Management are available to support the collection and management of data. Contact us to discuss your project.

## Consulting & User Support

To learn more about REDCap, please review the resources available in the User Guide and Trainings and Tutorials sections of this site. Click on the links in the navigation menu on the left side of this page. Consultants are available to help you at the <a href="https://research-it.berkeley.edu/consulting">Research IT virtual office hours</a> or via email <a href="mailto:redcap-support@lists.berkeley.edu">redcap-support@lists.berkeley.edu</a>.

## REDCap Project Team

The project team provides support to users in terms of account management, project level requests, system maintenance, training and consultation via office hours or email:

|      | Domain / Discipline |
| ------ | --------- |
| **Rick Jaffe** | Research IT / RTL |
| **Erin Foster** | Research IT / RTL |
| **Jonathan Felder** | Dev Ops / RTL |
| **Noah Baker** | Research IT Domain Consultant |
| **Wan Nurul Naszeerah** | Research IT Domain Consultant |

