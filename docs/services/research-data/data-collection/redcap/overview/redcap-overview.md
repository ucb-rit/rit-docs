---
title: REDCap Overview
keywords: 
tags: [redcap]
---

# REDCap Overview

## Uses

REDCap was developed by researchers for researchers in 2004. Since then, REDCap has matured into a data collection platform that can help researchers create instruments such as data entry forms and surveys for various study designs, including those requiring data from multiple sites as well as those that are longitudinal or repeating in nature. A web browser (Examples: FireFox, Safari and Chrome) is all researchers need to access REDCap; no software installation is needed.

## Features

REDCap has become a gold standard data collection tools because of the following useful features:

## Consulting & User Support

Research IT offers consulting for UC Berkeley researchers. We are here to help understand your needs, match you to appropriate resources, and help you get started using them. Our diverse team is made up of experts in both data and computing from a wide variety of domains, including REDCap. Consultants are available to help you at the virtual office hours or via email (redcap-support@lists.berkeley.edu).

Additional resources are also available through the university’s library guide and within this documentation.

## Global Community

<iframe
  width="600"
  height="450"
  style="border:0"
  loading="lazy"
  allowfullscreen
  referrerpolicy="no-referrer-when-downgrade"
  src="https://redcap.vanderbilt.edu/consortium/map_fullscreen.php">
</iframe>
