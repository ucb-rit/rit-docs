---
title: Citing REDCap
keywords: 
tags: [redcap]
---

# Citing REDCap

For researchers that use UCB’s REDCap platform to collect, gather, and/or manage data for their research, please use the following language to acknowledge use of this platform in publications, presentations, and/or grant applications:

!!! Citation
    This project made use of the UC Berkeley REDCap platform, which is developed and operated by Research, Teaching, and Learning (RTL), specifically Research IT and Dev Ops. More information about REDCap at UC Berkeley can be found <a href="../../../../../../services/research-data/data-collection/redcap/overview/redcap-at-berkeley">here</a>.
Please contact us at <a href="mailto:redcap-support@lists.berkeley.edu">redcap-support@lists.berkeley.edu</a> if you have any questions.
