---
title: Special Features within REDCap Projects
keywords: 
tags: [redcap]
---

# Special Features within REDCap Projects

Every REDCap project is capable of utilizing special features, some of which can be enabled by standard users and some that can only be enabled by REDCap administrators. Below is a listing of these features. Note, videos are from older versions of REDCap, but functionality is consistant.
(Video credits: Project REDCap; Table design: University of Chicago)

|Feature|Description|Watch Video|
|------|------|------|
|Defining Events in Longitudinal Projects|Defining events is a requirement of the Longitudinal module. That module allows any data collection instrument(s) to be completed multiple times for each record. The module uses an event grid to define the timepoints used for data collection. This video illustrates how to define the timepoints (also called events).|<a href="https://redcap.vanderbilt.edu/consortium/videoplayer.php?video=define_events02.flv&title=Defining+Longitudinal+Events+%285+min%29&text=&referer=REDCAP_PUBLIC" target="_blank">Defining Events Longitudinal Projects</a> (5 min)|
|Designating Instruments for Events in Longitudinal Projects|Designating instruments is an optional step of the Longitudinal module. That module allows any data collection instrument(s) to be completed multiple times for each record. The module uses an event grid to define the timepoints used for data collection. This video illustrates how to designate which instruments are completed at each timepoint.|<a href="https://redcap.vanderbilt.edu/consortium/videoplayer.php?video=designate_instruments02.flv&title=Designating+Instruments+for+Events+%283+min%29&text=&referer=REDCAP_PUBLIC" target="_blank">Designing Instruments for Events in Longitudinal Projects</a> (3 min)|
|Repeatable instruments and events|In-depth overview of how to set up and utilize the Repeating Instruments and Repeating Events functionality in both classic and longitudinal projects.|<a href="https://redcap.vanderbilt.edu/consortium/videoplayer.php?video=repeating_forms_events01.mp4&title=REDCap:%20Repeating%20Instruments%20and%20Events" target="_blank">Repeatable Instruments and Events</a> (33 min)|
|REDCap Mobile App|The REDCap Mobile App is an app that can be downloaded onto a tablet or mobile device to allow REDCap data to be collected locally on that device without the need for an internet connection (either wifi or cellular signal). If a user downloads the app, they must explicitly be given mobile app privileges in a given REDCap project in order to set up the project on the app. Once a project has been set up on the app, data can be collected in an offline fashion and then later synced back to the REDCap server.|<a href="https://redcap.vanderbilt.edu/consortium/videoplayer.php?video=app_overview_01.mp4&title=REDCap+Mobile+App+%282+min%29&text=&referer=REDCAP_PUBLIC" target="_blank">REDCap Mobile App</a> (2 min)|
|Locking Records|The locking feature freezes data. Users can still view existing data, but they cannot modify it unless they have the needed User Rights permissions. This video illustrates the basics of how instruments are locked. Refer to the built-in help resources and locking applications for more detailed instructions about locking management.|<a href="https://redcap.vanderbilt.edu/consortium/videoplayer.php?video=locking02.flv&title=Locking+Records+%282+min%29&text=&referer=REDCAP_PUBLIC" target="_blank">Locking Records</a> (2 min)|
|Data Resolution Workflow|This advanced tool enables a powerful data query management system.|<a href="https://redcap.vanderbilt.edu/consortium/videoplayer.php?video=data_resolution_workflow01.swf&title=Data+Resolution+Workflow+-+Data+Queries+Module+%285+min%29&text=This+module+can+be+used+for+opening%2C+responding+to%2C+and+closing+data+queries+in+a+project+as%0A%09%09%09%09%09%09%09%09%09a+means+of+providing+a+well-documented+workflow+of+the+process+of+resolving+issues+in+the+project%27s+data.&referer=REDCAP_PUBLIC" target="_blank">Data Resolution Workflow</a> (5 min)|


