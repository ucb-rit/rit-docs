---
title: Building a project/Basic features
keywords: 
tags: [redcap]
---

# Building a project/Basic features

View these short videos to learn how to build and modify data collection instruments. Note, videos are from older versions of REDCap, but functionality is consistant.
(Video credits: Project REDCap; Table design: University of Chicago)

|Feature|Description|Watch Video|
|------|------|------|  
|Introduction to Instrument Development|An introduction to the Online Designer and Data Dictionary methods of instrument development.|<a href="https://redcap.vanderbilt.edu/consortium/videoplayer.php?video=intro_instrument_dev.mp4&title=Introduction+to+Instrument+Development+%286+min%29&text=&referer=REDCAP_PUBLIC" target="_blank">Intro to Instrument Development</a> (6 min)|
|Online Designer|This online tool is the quickest and most intuitive method for making instrument modifications.|<a href="https://redcap.vanderbilt.edu/consortium/videoplayer.php?video=online_designer01.flv&title=The+Online+Designer+%285+min%29&text=&referer=REDCAP_PUBLIC" target="_blank">Online Designer</a> (5 min)|
|Data Dictionary|The Data Dictionary is your project structure in a downloadable spreadsheet file. The spreadsheet can be modified and uploaded into REDCap to make instrument modifications. To see an example, download the Data Dictionary demonstration file.|<a href="https://redcap.vanderbilt.edu/consortium/videoplayer.php?video=redcap_data_dictionary02.flv&title=The+Data+Dictionary+%2810+min%29&text=This+10-minute+video+gives+a+thorough+overview+of+the+REDCap+data+dictionary+template+and+discusses+general%0A%09%09%09%09%09%09%09%09%09rules+and+formats+to+use+when+creating+and+editing+a+data+dictionary+for+a+REDCap+project.&referer=REDCAP_PUBLIC" target="_blank">Data Dictionary</a> (10 min)|
|Project Field Types|An illustration of some popular field types.|<a href="https://redcap.vanderbilt.edu/consortium/videoplayer.php?video=field_types02.flv&title=Project+Field+Types+%284+min%29&text=&referer=REDCAP_PUBLIC" target="_blank">Project Field Types</a> (4 min)|
|Applications Overview|A brief exploration of the most popular built-in tools.|<a href="https://redcap.vanderbilt.edu/consortium/videoplayer.php?video=applications_menu01.mp4&title=Applications%20Overview&referer=redcap.vanderbilt.edu" target="_blank">Applications Overview</a> (5 min)|
|The Calendar|This tool organizes your study by tracking upcoming milestones and events.|<a href="https://redcap.vanderbilt.edu/consortium/videoplayer.php?video=calendar02.flv&title=The+Calendar+%287+min%29&text=&referer=REDCAP_PUBLIC" target="_blank">The Calendar</a> (7 min)|
|Scheduling Module|This is an optional feature in longitudinal projects. The tool generates record-specific schedules based on a series of events or timepoints. Used with the calendar module, this tool greatly streamlines longitudinal data entry.|<a href="https://redcap.vanderbilt.edu/consortium/videoplayer.php?video=scheduling02.flv&title=Scheduling+Module+%287+min%29&text=&referer=REDCAP_PUBLIC" target="_blank">Scheduling Module</a> (7 min)|
|Data Access Groups for multi-site projects|The Data Access Groups feature assigns users to groups. Any user in a Data Access Group may only access their group's records. Each group is blinded to all other data/records. Users not in a group can access all data across all groups. This feature is especially useful for multi-site projects because each data collection site must usually be restricted from viewing other sites' records.|<a href="https://redcap.vanderbilt.edu/consortium/videoplayer.php?video=data_access_groups02.flv&title=Data+Access+Groups+for+Multi-site+Projects+%287+min%29&text=Utilized+mostly+for+multi-site+projects%2C+Data+Access+Groups+are+a+good+way+of+creating+user+groups%0A%09%09%09%09%09%09%09%09%09where+different+groups+cannot+view+records+that+belong+to+other+groups.&referer=REDCAP_PUBLIC" target="_blank">Data Access Groups</a> (7 min)|

