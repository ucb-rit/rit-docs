---
title: New User Instrument
keywords: 
tags: [redcap]
---

# New User Instrument

The UC Berkeley REDCap support team has produced a REDCap survey to guide users through REDCap fundementals.
After taking the survey, a download link will appear to allow you to import the project into your own REDCap instance.

You can take the survey <a href="https://redcap.berkeley.edu/surveys/?s=T83NMJWKWRFDY3HP" target="_blank">here</a>.

In your REDCap project, upload this instrument through the "Designer" mode when viewing "Data Collection Instruments". Click the "Upload" button to upload the zip file for this instrument.

This will create a new instrument for you to edit and explore. Instruments can later be deleted or maintained and hidden from survey participants.
