---
title: Pre-Data Collection
keywords: 
tags: [redcap]
---

# Pre-Data Collection

## Study Design

During this stage, REDCap users are highly recommended to have already formulated a general study design with clear objectives and methodology. There are several types of project formats in REDCap, each of which has its own advantages and disadvantages. Please refer to <a href="../../../../../../services/research-data/data-collection/redcap/trainings-and-tutorials/types-of-redcap-projects">Trainings and Tutorials</a> for more information about REDCap types of projects.

## Project Creation

Anyone with a CalNet based REDCap @ UC Berkeley account can request new research projects on REDCap. Any new project requires approval from the REDCap@UCBerkeley team prior to further customization. Please follow the steps below to request a project on REDCap and allow 2 business days for approval.

### Step 1: Log in using CalNet ID

After logging in, you should see UC Berkeley REDCap’s homepage.

<img alt="" src="../../../../../../img/redcap_step_1.png" style="height:391px; width:800px">

### Step 2: Click on “New Project”

To start, look for a tab at the top of the webpage called “New Project”.  Clicking on this tab will take you to the page needed to submit a request for a new project.

<img alt="" src="../../../../../../img/redcap_create.png" style="height:671px; width:800px">

### Step 3: Fill in the request form for a new project

To help the REDCap@UCBerkeley team understand your project and approve the request to use the platform, some information is needed.

!!! Projects
    <b>“Project title”</b> - Let us know how you would like this project to be called.

    <b>“Project purpose”</b> - Choose a suitable purpose from the drop-down box. You may choose to create one for practice if you would like to get familiar with REDCap. However, practice projects should not be used for data collection.

    <b>“Project notes”</b> - While this is optional, we encourage a brief description of the project to be included. It will be displayed on the page called “My Projects” and can only be seen by those with access to the specific project.

    <b>“Project creation option”</b> - Choose how you would like to get started.

    <b>Empty project (blank slate)</b> - This option allows you to build the project from scratch.

    <b>Upload a REDCap project XML file (CDISC ODM format)</b> - This option allows you to import an already existing project template. This option is encouraged if you are migrating a project to REDCap@UCBerkeley from another REDCap instance (for example, UCSF REDCap). If the ODM file originates from another system outside REDCap, REDCap will do its best to attempt to convert the data types and other attributes in the file to REDCap-equivalent data types and attributes, although they might not be exactly alike.

    <b>Use a template</b> - A template is useful for exploring the possibilities of REDCap or starting with examples of forms to use. Structures in the loaded template can be deleted or added as desired.

### Step 4: Click “Send Request” and Wait for Approval

After clicking “Send Request” at the bottom of the form, please allow 1-2 business days for approval. If you experience any delay in receiving approval, contact us directly via email at <a href="mailto:redcap-support@lists.berkeley.edu">redcap-support@lists.berkeley.edu</a>.

!!! Important Note
    REDCap projects will be approved under two use cases:

    If the “Project Purpose” is for “Practice/ Just for fun”, the projects will be approved shortly without consultation. These types of projects should not be used for data collection. A project originally created under “Practice / Just for fun” can be imported as an XML to create an official data collection project at a later date.

    If the “Project Purpose” has a reason other than “Practice/ Just for fun”, Research IT will reach out to the project creator prior to approval.

### Step 5: Prepare information for onboarding process

After creating an account and a new project on REDCap that is not for “Practice/Just for fun”, users will be contacted for an onboarding meeting with the REDCap@UC Berkeley Support Team.

We provide a REDCap Requirements Gathering survey to assess researcher needs. Please complete the survey <a href="https://redcap.berkeley.edu/surveys/?s=WJL3LKJF7XHM74F4" target="_blank">here</a>.

Please be prepared to provide details on the research project, including but not limited to:

* Name and department of Principal Investigator (PI)
* Name(s) of researcher(s) involved
* Title, brief description, and timeline of research project
* Research design (Cross-sectional or longitudinal? Single-arm or multi-arm? Other details on complexity?)
* Data security (Types of data collected? Data classification?)
* Data collection methods (Offline via mobile app requirement? Survey?)
* Data transfer from another system (if required)
* Data storage requirements

## Instrument Design + Test

Users are highly recommended to start designing the instruments or data collection forms as early as possible. We highly recommend watching reviewing Trainings and Tutorials, located here.

After designing the instruments, you are highly encouraged to test them. Please verify that all needed data can be collected correctly and appropriately before changing the status of the project to production mode (see more information about modes in During Data Collection section). Testing your instruments thoroughly minimizes the likelihood of making major changes later on.

## IRB Approval

If the research project involves human subjects or Protected Health Information (PHI), you will need to apply for clearance from your Institutional Review Board (IRB). If you are not sure, please seek clarification from UC Berkeley’s <a href="https://cphs.berkeley.edu/" target="_blank">Committee for Protection of Human Subjects (CPHS)</a>. You may need to include your drafted instruments from REDCap in the IRB application.

## To Data Collection

When a REDCap project has finalized its survey instruments, the REDCap support team will require 1-4 weeks to help review security requirements, user permissions, and perform instrument testing. Approval will be notified via email. Please contact REDCap @ UC Berkeley Support <a href="mailto:redcap-support@lists.berkeley.edu">redcap-support@lists.berkeley.edu</a> if you have any questions.

