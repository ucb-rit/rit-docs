---
title: Accounts
keywords: 
tags: [redcap]
---

# Accounts

## Obtaining an Account

Anyone with a CalNet ID has the ability to make a Berkeley REDCap account. Navigate to the <a href="https://redcap.berkeley.edu/" target="_blank">Berkeley REDCap website</a> and click the “Login using CalNet” link.
First-time users will be prompted to create an account with REDCap.

For security purposes, whenever possible we recommend that project team members have CalNet IDs, through the standard campus affiliate process when needed. However, when this is not feasible, non-UCB affiliated team members can be given access to UC Berkeley’s REDCap. The REDCap support team is responsible for creating accounts in these instances.

Please contact us <a href="mailto:redcap-support@lists.berkeley.edu">redcap-support@lists.berkeley.edu</a> with any questions about REDCap accounts.
