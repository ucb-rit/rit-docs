---
title: Data Collection
keywords: 
tags: [redcap]
---

# Data Collection

## Project Mode (Draft / Production / Inactive)
Once your project has been approved, you may choose to change the status of your project to one of the three modes: production, draft or inactive.

!!! Modes
    “Draft Mode” means you may modify an element of your data entry forms whilst continuing to collect data. Changes can only be reflected after receiving approval from a REDCap Administrator. All projects start in “Draft Mode” before moving to “Production Mode”.
    “Production Mode” means you may enter, review, and analyze real data. Projects must be reviewed by the REDCap support team prior to entering Production mode. It is not advisable to make major changes to your instruments once you have entered “Production Mode”. Nonetheless, you can return to “Draft Mode” to make changes if needed. Note that your project will need to be reviewed again before returning to “Production Mode”.
    “Inactive Mode” means no data entry or update can take place during this time. However, you may continue to access your data.

## Data Download

To download or export data, go to “Applications” and select “Data Exports, Reports, and Stats”. From here, you can download data from single instruments or all instruments under your account.

<img alt="" src="../../../../../../img/redcap_download_data_2.png" style="height:394px; width:875px">

Select “Export Data” to begin the data export from the desired instrument.

<img alt="" src="../../../../../../img/redcap_download_data_1.png" style="height:473px; width:875px">

Data collected from REDCap can be exported as the following formats:

* CSV (raw data or labeled)
* SPSS Statistical Software
* SAS Statistical Software
* R Statistical Software
* Stata Statistical Software
* CDISC ODM (XML)

The de-identification options are important if sensitive data will be moving off of Berkeley’s REDCap. If your project involves highly sensitive data (P4), Research IT consultants can help guide you through the best options for exporting data. The researcher is responsible for managing and implementing the proper data security protocols with all data under their control.

