---
title: User Guide Overview
keywords: 
tags: [redcap]
---

# User Guide Overview

## Workflow

Prior to using REDCap services, UC Berkeley researchers must create an account. REDCap usage can be divided into three separate stages: Pre-Data Collection, Data Collection, and Post-Data Collection.
The workflow below visualizes the stages in REDCap data collection, with researcher responsibilities seen above the line and REDCap service support seen below the line.

<img alt="" src="../../../../../../img/REDCap_Workflow_v2.png" style="width:800px">

## Accounts

<table border="0">
<tbody>
<tr>
<td align="top" width="150"><img alt="Key icon - representing password entry" height="129" width="134" class="media-element file-default image-style-none" data-delta="3" typeof="foaf:Image" src="../../../../../../img/key.png"></td>
<td><strong>Accounts</strong>. How to create accounts for UC Berkeley researchers and affiliates. See <a href="../../../../../../services/research-data/data-collection/redcap/user-guide/accounts">Accounts</a>.</td>
</tr>
</tbody></table>

## Pre-Data Collection

<table border="0">
<tbody>
<tr>
<td align="top" width="150"><img alt="Monitor icon - representing act of logging in" height="162" width="134" class="media-element file-default image-style-none" data-delta="4" typeof="foaf:Image" src="../../../../../../img/monitor_2_134px.png"></td>
<td><strong>Pre-Data Collection</strong>. Project creation, study design, instrument design, IRB approval, consulting and project approval. See <a href="../../../../../../services/research-data/data-collection/redcap/user-guide/pre-data-collection">Pre-Data Collection</a>.</td>
</tr>
</tbody></table>

## Data Collection

<table border="0">
<tbody>
<tr>
<td align="top" width="150"><img alt="File transfer icon" height="147" width="134" class="media-element file-default image-style-none" data-delta="15" typeof="foaf:Image" src="../../../../../../img/Binary-File-Plain_transfer_134px.png"></td>
<td><strong>Data Collection</strong>. Project modes, collecting data, monitoring progress and security, and downloading data. See <a href="../../../../../../services/research-data/data-collection/redcap/user-guide/data-collection">Data Collection</a>.</td>
</tr>
</tbody></table>

## Post-Data Collection

<table border="0">
<tbody>
<tr>
<td align="top" width="150"><img alt="File folder icon - signifying file storage" height="132" width="134" class="media-element file-default image-style-none" data-delta="11" typeof="foaf:Image" src="../../../../../../img/folder-grey_2_134px.png"></td>
<td><strong>Post-Data Collection</strong>. UC Berkeley data analysis and compute resources and project archival and deletion. See <a href="../../../../../../services/research-data/data-collection/redcap/user-guide/post-data-collection">Post-Data Collection</a>.</td>
</tr>
</tbody></table>
