---
title: Post-Data Collection
keywords: 
tags: [redcap]
---

# Post-Data Collection

## Data Archive or Delete

After completing the data collection, you may choose to move your project to indefinite hiatus (“Archived Mode”) or to complete deletion (“Delete Project”). We highly recommend you to delete the project after downloading and storing the project data dictionary and its data to a cloud environment.

## Data Analysis

Berkeley offers many different computing resources and services to facilitate the high-performance computing needs of your project. Data downloaded from REDCap can utilize the following services for analysis:

!!! Services
    ### Savio (High Performance Computing)

    The Savio institutional cluster forms the foundation of the Berkeley Research Computing (BRC) Institutional/Condo Cluster. Savio provides all researchers access to campus-subsidized compute time. By default, Savio provides a variety of software useful in data analysis. Users are able to install additional software under their own accounts. Click here for more information.

    ### Analytics Environments on Demand (AEoD)

    AEoD is a Windows-friendly virtual machine service for computation over moderately sensitive (P2/P3) data. AEoD is most suitable for researchers needing interactive computing using common software packages (Stata, ArcGIS, RStudio, etc.) in a familiar desktop environment. AEoD provides an easy-to-use Windows environment tailored to specific research project needs. The following screenshots are provided as reference to what an active Virtual Machine environment looks like. Click here for more information.

    ### Secure Research Data and Compute (SRDC)

    The Secure Research Data and Compute (SRDC) platform has been developed for researchers working with highly sensitive (P4) data. This includes high performance computing, computing on Windows and Linux virtual machines with desktop environments, and protected storage for both options.
    For example, researchers working with protected health information (PHI), other highly sensitive human subjects data, controlled unclassified information (CUI), or other high risk data coming from third parties are strong candidates for SRDC. Click here for more information.
