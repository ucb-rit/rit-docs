---
title: FAQ
keywords: 
tags: [redcap]
---

# Frequently Asked Questions

??? question "<span id="q-how-can-i-create-a-redcap-account-">Q: How can I create a REDCap account?<a class="headerlink" href="#q-how-can-i-create-a-redcap-account-" title="Permanent link">¶</a></span>"
	A:  Anyone with a CalNet ID has the ability to make a Berkeley REDCap account. Navigate to https://redcap.berkeley.edu/ and click the “Login using CalNet” link. First-time users will be prompted to create an account with REDCap. If you are experiencing a delay or if you wish to provide access to users who do not have CalNet ID, please contact us at <a href="mailto:redcap-support@lists.berkeley.edu">redcap-support@lists.berkeley.edu</a>.


??? question "<span id="q-can-i-give-access-to-my-team-members-with-no-calnet-id-">Q: Can I give access to my team members who do not have CalNet ID?<a class="headerlink" href="#q-can-i-give-access-to-my-team-members-with-no-calnet-id-" title="Permanent link">¶</a></span>"
	A:  For security purposes, whenever possible we recommend that project team members have CalNet IDs, through the standard campus affiliate process when needed. However, when this is not feasible, non-UCB affiliated team members can be given access to UC Berkeley’s REDCap. The REDCap support team is responsible for creating accounts in these instances so please reach out to <a href="mailto:redcap-support@lists.berkeley.edu">redcap-support@lists.berkeley.edu</a>.


??? question "<span id="q-how-does-redcap-differ-from-other-data-collection-tools-that-uc-berkeley-supports-">Q: How does REDCap differ from other data collection tools that UC Berkeley supports?<a class="headerlink" href="#q-how-does-redcap-differ-from-other-data-collection-tools-that-uc-berkeley-supports-" title="Permanent link">¶</a></span>"
	A:  UC Berkeley also supports Qualtrics and Google Forms for the creation of surveys and online forms. More information about both of these tools and their support at UC Berkeley can be found <a href="https://technology.berkeley.edu/surveys/" target="_blank">here</a>. REDCap differs in comparison to Qualtrics and Google Forms most notably in its support for building and managing databases for projects and its approval for highly sensitive data.


??? question "<span id="q-is-ucb-redcap-able-to-handle-p4-data-as-defined-in-the-ucb-data-classification-standard-">Q: Is UCB REDCap able to handle P4 data as defined in the UCB Data Classification Standard?<a class="headerlink" href="#q-is-ucb-redcap-able-to-handle-p4-data-as-defined-in-the-ucb-data-classification-standard-" title="Permanent link">¶</a></span>"
	A:  UC Berkeley REDCap is approved for collection or capture of P4 level data as defined in the UCB Data Classification Standard. Read more at the <a href="https://security.berkeley.edu/data-classification-standard" target="_blank">Berkeley Information Security Office website</a>.


??? question "<span id="q-can-i-create-a-project-without-having-institutional-review-board-irb-approval-">Q: Can I create a project without having institutional review board (IRB) approval?<a class="headerlink" href="#q-can-i-create-a-project-without-having-institutional-review-board-irb-approval-" title="Permanent link">¶</a></span>"
	A:  Not all projects that use REDCap will be collecting/gathering human subjects data, which means that the IRB process may not apply to all researchers using REDCap @ UC Berkeley. In the case that data collected involves human subjects, the IRB will need to review the project, and the use of REDCap @ UC Berkeley should be noted in the IRB protocol.
    If requiring IRB, researchers may create a project in REDCap prior to receiving IRB approval. However, before the project moves to Production (i.e., actively collecting data), IRB approval must be received.


??? question "<span id="q-can-i-request-for-more-storage-for-my-project-">Q: Can I request for more storage for my project?<a class="headerlink" href="#q-can-i-request-for-more-storage-for-my-project-" title="Permanent link">¶</a></span>"
	A:  All users of REDCap projects are given data storage capacity, allowing for more than 10 million text data entries. If you anticipate collecting or gathering an amount of data that requires 100GB+ of storage (for a single REDCap project), please contact the REDCap @ UC Berkeley support team at <a href="mailto:redcap-support@lists.berkeley.edu">redcap-support@lists.berkeley.edu</a>


??? question "<span id="q-for-how-long-can-i-keep-my-data-stored-in-redcap-">Q: For how long can I keep my data stored in REDCap?<a class="headerlink" href="#q-for-how-long-can-i-keep-my-data-stored-in-redcap-" title="Permanent link">¶</a></span>"
	A:  REDCap @ UC Berkeley should not be considered a long term storage option for research projects. Once data collection is complete for a given project, the expectation is that data gathered will be exported out of REDCap and the project will be put into “archive” status. The REDCap @ UC Berkeley team can assist researchers with exporting as well as determining appropriately secure long(er) term storage solutions depending on the sensitivity of the data.


More FAQs can be found on the official <a href="https://projectredcap.org/about/faq/" target="_blank">REDCap documentation</a>.

If you have issues or questions related to REDCap that are not answered, please reach out to the REDCap Support team at <a href="mailto:redcap-support@lists.berkeley.edu">redcap-support@lists.berkeley.edu</a>.
