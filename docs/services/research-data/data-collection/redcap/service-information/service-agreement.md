---
title: Service Agreement
keywords: 
tags: [redcap]
---

# Service Level Agreement

## Description of Service

REDCap at UC Berkeley is available at [https://redcap.berkeley.edu/](https://redcap.berkeley.edu/) and hosted by UC Berkeley on Amazon Web Services. Service and operations support for REDCap is provided by Research, Teaching, and Learning (Research IT & DevOps) including:

* Hosting REDCap in a UC Berkeley approved P4 infrastructure.

* Performing daily backups of REDCap database that are maintained for 30 days.

* Providing consulting on REDCap functionality and features to users.

## Service Hours and Availability

REDCap at UC Berkeley is continuously available, barring service upgrades and maintenance windows. Due to current funding and staffing, our service team support is typically limited to business days/hours (Monday - Friday, 9-5pm Pacific). Users may report any issues and requests for service to [redcap-support@lists.berkeley.edu](mailto:redcap-support@lists.berkeley.edu).

## Service Upgrades, Maintenance, and Downtime

The UC Berkeley REDCap service team maintains the Long-Term Support (LTS) version of REDCap, which has major updates generally twice a year and more frequent minor updates to address bug fixes and security patches.

The standing maintenance window for REDCap is the first Tuesday of the month from 10am-12pm Pacific. In the case of a "medium level" security patch, Fridays from 10am-12pm Pacific may also be used for updates.

!!! Warning
    In the advent of a "critical level" security patch, the service and operations team may have to immediately shutdown access to the system.

The service team will plan to notify users the week before if the monthly maintenance window will be used. Though typical downtime is no longer than an hour, users should not rely on having access to REDCap during the maintenance window(s) if maintenance is scheduled.

## Service Governance and Security

UC Berkeley REDCap is approved for collection or capture of P4 level data as defined in the [UCB Data Classification Standard](https://security.berkeley.edu/data-classification-standard). This includes data that may be subject to laws and regulations such as [HIPAA](https://cphs.berkeley.edu/hipaa/hipaa.html) or [GDPR](https://cphs.berkeley.edu/guide/gdpr.html).

!!! Advisory
    REDCap at UC Berkeley is not compliant with [21 CFR Part 11](https://www.fda.gov/regulatory-information/search-fda-guidance-documents/part-11-electronic-records-electronic-signatures-scope-and-application) in regards to e-consent or other expectations included in the regulation. If you have further questions regarding this or want to use REDCap to support 21 CFR Part 11 compliance, please contact [redcap-support@lists.berkeley.edu](mailto:redcap-support@lists.berkeley.edu) 

Users should report suspicious activity or suspected security incidents to the service team as soon as possible through email: [redcap-support@lists.berkeley.edu](mailto:redcap-support@lists.berkeley.edu)

## Cost of Service

REDCap and a basic level of support is currently offered at no cost to UC Berkeley researchers. 
If you plan to make extensive use of REDCap (for example, to support data collection for multi site collaboration; or as the primary, long term data collection platform for an institute/center), please contact [redcap-support@lists.berkeley.edu](mailto:redcap-support@lists.berkeley.edu) to make sure we can adequately support your needs.
