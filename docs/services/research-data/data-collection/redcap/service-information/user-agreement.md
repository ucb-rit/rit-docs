---
title: User Agreement
keywords: 
tags: [redcap]
---

# User Agreement

## User Responsibility and Data Governance

The REDCap service at UC Berkeley operates on a model of “shared responsibility” meaning that the REDCap team (service & operations) are committed to ensuring the security and functionality of the infrastructure and technical components while researchers are responsible for ensuring administrative and research practices and methodologies are up to date and consistent with campus policies.

Users are responsible for understanding and maintaining appropriate cybersecurity practices when working in REDCap and handling protected data. UC Berkeley’s Information Security Office provides resources and guidance on [cybersecurity “best practices”](https://security.berkeley.edu/education-awareness). 

## Institutional Review Board (IRB)

When including mention of REDCap in an IRB protocol, here are some sample phrases to consider including: 

* __Data Storage__: REDCap software and project data are securely housed within a P4 environment in UC Berkeley's implementation of REDCap on Amazon Web Services.

* __User Access Control__: REDCap employs a comprehensive multi-level security system, enabling researchers to easily implement "minimum necessary" data access for their research team, including the specification of data fields containing identifiers. It also provides a streamlined process for completely de-identifying data for analysis or other purposes with a single click. All user activities are logged to facilitate auditing of data access. Access is integrated with Berkeley CalNet IDs, ensuring authentication using credentials for Berkeley researchers and approved affiliates.

* __Data Integrity Assurance__: REDCap at Berkeley is managed by Research IT and DevOps in accordance with data security practices outlined by the Berkeley Information Security Office. All data changes are logged, enabling auditing of all modifications.

## Citing Use of REDCap

For researchers that use UCB’s REDCap platform to collect, gather, and/or manage data for their research, please use the following language to acknowledge use of this platform in your publications, presentations, and/or grant applications:

!!! Citation
    This project makes use of the UC Berkeley REDCap platform, which is developed and operated by Research, Teaching, and Learning (RTL), specifically Research IT and Dev Ops units. Information about REDCap at UC Berkeley can be found here: [https://research-it.berkeley.edu/services-projects/redcap](https://research-it.berkeley.edu/services-projects/redcap).
