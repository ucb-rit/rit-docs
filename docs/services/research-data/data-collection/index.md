---
title: Data Collection
keywords: data collection
tags: [data collection]
---

Data Collection 
===========================

The campus provides access to several data collection and gathering platforms at no cost to campus faculty, students, and staff researchers. These include <a href="https://technology.berkeley.edu/surveys" target="_blank">survey and online form platforms through Berkeley IT</a> in addition to REDCap, which is provided by RTL/Research IT. Determining what your needs are when it comes to collecting data as well as deciding how well (or not) particular data collection and gathering tools/platforms fit into your workflow(s) can be challenging initial steps in a research process. Please reach out to <a href="mailto:researchdata@berkeley.edu">researchdata@berkeley.edu</a> if you would like guidance on the options available and how they can fit your data collection needs.
