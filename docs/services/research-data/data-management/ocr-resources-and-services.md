---
title: OCR Resources and Services
keywords: data management ocr berkeley
tags: [data management ocr berkeley]
---

OCR Resources and Services
===========================

Optical Character Recognition (OCR) is a process of converting text images (e.g., scanned documents, historic print, handwriting samples) into machine readable formats. At the moment, campus’ OCR support is primarily self-service.

Some considerations before engaging with OCR projects:
<ul style="margin-left: 40px;">
<li>Has the content been OCR’d already and made available elsewhere? If you are not sure or have not searched, reaching out to <a href="mailto: librarydataservices@berkeley.edu">librarydataservices@berkeley.edu</a> will connect you with subject librarians who can assist in this process.</li>
<li>How much content is involved? For projects that involve “hundreds” of documents/materials, software such as <a href="https://pdf.abbyy.com">ABBYY</a> FineReader are initial options to explore. If projects involve “millions” of documents/materials then use of <a href="https://github.com/tesseract-ocr/tesseract">Tesseract</a> (or other computational oriented software) might be best to investigate.</li>
</ul>

## Common solutions used on campus

### ABBYY FineReader

An often recommended <a href="https://pdf.abbyy.com">PDF software program</a> due to its accuracy, support for multiple languages, and ability to handle technical/complicated formats.

A campus license is not available. There <a href="https://pdf.abbyy.com/download">is a 7-day trial available</a>, which is recommended to use for a test run and decide if it is worth investing in an ABBYY license. In the past, this software was available on computers in the <a href="https://dlab.berkeley.edu">D-Lab</a> and through <a href="https://research-it.berkeley.edu/services-projects/aeod-virtual-machines">Research IT’s virtual machine service</a>; unfortunately both services are no longer offered/available.

### Tesseract

An open source software that runs through a command line interface to process large scale OCR projects. More information about the Tesseract project can be found here: https://github.com/tesseract-ocr/tesseract

In the past, Tesseract has been used on <a href="https://docs-research-it.berkeley.edu/services/high-performance-computing/overview">Savio (campus’ high performance computing cluster)</a> and documentation on how to do so (via a Singularity container) is here: https://github.com/ucb-rit/singularity-tesseract. If you would like to explore the use of Tesseract on Savio, <a href="https://docs-research-it.berkeley.edu/services/high-performance-computing/getting-account">follow these instructions</a> to gain access to the cluster.

### Scanning
If you have physical materials to be scanned, in order to then perform OCR upon, there are <a href="https://www.lib.berkeley.edu/visit/print-scan">scanners available in the Library</a>. 

## Related Resources

<ul style="margin-left: 40px;">
<li>SimpleOCR (freeware) <a href="https://www.simpleocr.com/">https://www.simpleocr.com/</a></li>
<li>Doxie.ai (paid) <a href="https://doxie.ai/">http://doxie.ai/</a></li>
<li>Resources and support for <a href="https://guides.lib.berkeley.edu/c.php?g=491766&p=7828496">text mining & computational text analysis</a>
</ul>