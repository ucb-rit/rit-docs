---
title: Data Management
keywords: data management
tags: [data management]
---

Data Management Overview
===========================

Data management aligns with processes of collecting/generating, storing, processing, transferring, sharing, and disseminating research data. There are many requirements, policies, and procedures at institutional, regulatory, and funder levels that impact how research data is managed. Across disciplines, there are also recommended practices that can assist with complying and enhancing research data management activities for researchers and research labs. For guidance, questions, or training needs related to research data management requirements and good practices for managing data, please reach out to <a href="mailto: researchdata@berkeley.edu">researchdata@berkeley.edu</a>.

## Read more:
<ul style="margin-left: 40px;">
<li><a href="../../../services/research-data/data-management/ocr-resources-and-services">OCR Resources and Services</a></li>
</ul>