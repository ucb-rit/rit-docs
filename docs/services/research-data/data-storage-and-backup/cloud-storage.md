## Cloud storage options

Here we list some of the key details of these cloud (and related) storage options.

### Amazon Glacier Deep Archive

With this storage, researchers pay a monthly fee for cloud storage. Glacier Deep Archive is a storage tier of Amazon’s S3 storage. Files are stored in units called “buckets”.

This is a very good, cheap option for archive storage, primarily for files you don’t expect to ever need to retrieve, but not suitable for other uses given limited flexibility that can drive up costs.

Some features of this storage include:

 - Pricing (as of mid-2024):
    - $12 per TB per year.
    - 180 day minimum charge; files removed/changed/updated before 180 days incur full 180 day cost for each "version".
    - Very small charge per file transferred (one "PUT" request per file) of $0.005 per 1000 requests that can add up to substantial charge for many small files. 
    - High cost to retrieve data: on the order of $90/TB or more, depending on details.
 - Paid monthly via campus AWS account through campus chartstring.
 - Set up as one or more S3 buckets with a Lifecycle policy configured to immediately move the files into Glacier Deep Archive.
 - Data are stored redundantly across multiple Availability Zones in an AWS region.
 - Suitable for protected data up through and including P4.
 - Can enable file versioning within a bucket to retain history of files.  
 - Rule-of-thumb: files should be bigger than 128 KB.

### Wasabi

With this storage, researchers pay a monthly fee for cloud storage, with flexibility in terms of transferring data into and out of the storage.

This is suitable for warm and cold storage, and could also be used for archiving (but note that AWS Glacier Deep Archive is cheaper for that purpose).

 - Pricing (as of mid-2024):
    - $84 per TB per year 
    - 1 TB minimum
    - 30 day minimum charge; files removed/changed/updated before 30 days incur the full 30 day cost for each “version”
    - No other fees
 - Paid monthly via campus Wasabi account through campus chartstring
 - Data are stored redundantly across multiple regions.
 - Suitable only for protected data at level P2.
 - Can enable file versioning within a bucket to retain history of files.   


### Active Archive Object Storage (Berkeley IT)

With this cloud-like storage option, researchers can purchase a large amount (in increments of 125 TB) of storage through Berkeley IT for a one-time cost that is then available for five years.

This is suitable for active/hot, warm, and cold storage.

Some features of this storage include:

 - Pricing (as of mid-2024):
    - One-time cost of $36,000 for each 125 TB Cloudian storage unit, available for 5 years, approximately $48 per TB per year.
    - Monthly cost of $20 per month per unit.
    - Yearly maintenance cost of $700 per unit.
    - No transfer fees or other costs
 - Suitable for protected data up through and including P4.
 - Storage is on campus (campus data center) or at San Diego Supercomputing Center.
 - Fast access through the campus network when stored on campus.
 - Redundancy across storage nodes/disks in the data center but not across locations, so in many cases one would not want this to be the sole copy of data.
 - BerkeleyIT can help set up backup to cloud storage.
 - This capital purchase is exempt from UC indirect cost expenses on grant-funded research.
