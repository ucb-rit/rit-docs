---
title: Resources for working with sensitive data
keywords: resources, security
tags: [data security]
---

Resources for working with sensitive data
----------------------------------------------

Research IT provides the following services for researchers working with data of varying sensitivity:

### Highly sensitive compute and storage

#### Computing
The <a href="https://research-it.berkeley.edu/projects/secure-research-data-and-computing" target="_blank">Secure Research Data and Compute</a> (SRDC) platform is designed for researchers working with highly sensitive (P4) data. The SRDC platform includes high performance computing and virtual machines that allow interactive computing in a familiar desktop environment. Secure storage is available with both. Researchers working with medical information protected under HIPAA, biometric data used for authentication, genetic data, or government issued ID numerical data are strong candidates for SRDC.

SRDC virtual machines are available now. <a href="https://research-it.berkeley.edu/consulting" target="_blank">Contact Research IT</a> to get started. The SRDC HPC cluster will be online in 2021.

#### Data Collection
<a href="../../data-collection/redcap/overview/redcap-at-berkeley">REDCap</a> is a data collection platform used worldwide. Researchers can create instruments (data entry forms and surveys) for various study designs, including those requiring data from multiple sites as well as those that are longitudinal or repeating in nature. REDCap at Berkeley supports collection of highly sensitive (P4) data, including the import of HIPAA-protected data. Researchers working with REDCap can export their data to SRDC for further processing and analysis.
### Moderately sensitive compute and storage

#### High Performance Computing

<a href="https://research-it.berkeley.edu/programs/berkeley-research-computing#savio" target="_blank">Savio</a>, UC Berkeley's high performance computing cluster, is available for P1 data. Researchers may request a separate account to use it for <a href="https://docs-research-it.berkeley.edu/services/high-performance-computing/user-guide/sensitive-data">moderately sensitive (P2/P3) data</a>. For example, researchers working with de-identified public health or human genetic data are strong candidates for Savio, especially if their analyses can run in parallel or benefit from access to a large number of processors.

#### Virtual Machines

<a href="https://research-it.berkeley.edu/services/analytics-environments-demand" target="_blank">Analytics Environment on Demand (AEoD)</a>, a Windows-friendly virtual machine service, may be used for computation over <a href="https://research-it.berkeley.edu/services/sensitive-and-protected-data" target="_blank">moderately sensitive (P2/P3) data](</a>. Researchers performing data analytics or geospatial analysis with moderately sensitive individually-identifiable human subjects research data, public health information, or data related to animal research are strong candidates for AEoD. AEoD is most suitable for researchers needing interactive computing using common software packages (Stata, ArcGIS, RStudio, etc.) in a familiar desktop environment. AEoD virtual machines are scaled to meet computational needs, and are available in different sizes, from 2 - 20 cores, 4 - 256 GB RAM plus performant storage.

Research IT consultants are happy to meet with you to understand your data and computing needs and help match you to appropriate resources. <a href="https://research-it.berkeley.edu/consulting" target="_blank">Visit our office hours or get in touch</a> to schedule an appointment.

### Resources

<a href="https://security.berkeley.edu/resources/researcher-resources" target="_blank">Resources for Researchers (Information Security Office)</a>

<a href="http://cphs.berkeley.edu/" target="_blank">Human Research Protection Program</a>

<a href="https://researchdata.berkeley.edu/data-security/best-practices-working-sensitive-data-qa" target="_blank">Best practices for working with sensitive data (Q&A)</a>
