---
title: Determining the sensitivity of your data
keywords: data sensitivity, security
tags: [data security]
---

Determining the sensitivity of your data
---------------------------------------------

### Agreements and governing frameworks

The first step in determining the level of security appropriate for your data is to understand the agreements, regulations, and other considerations that govern how your data must be protected. These can include:

-   Committee for the Protection of Human Subjects (human subjects, "IRB") protocols

-   Data use agreements

-   Federal and state laws and regulations (e.g. FERPA, HIPAA)

-   Funder requirements

-   Adhering to campus <a href="https://security.berkeley.edu/data-classification-standard#table" target="_blank">data classification standards</a> (P1, P2, P3, P4)

-   General Data Protection Regulation (GDPR) for subjects located in the EU and similar regulations for other countries

-   Intellectual property concerns

-   Privacy and ethical concerns (e.g. confidentiality, vulnerable research subjects)

#### If you are producing data

Consider the elements of your data:

-   What types of information do you collect? Think about all of the collection methods you will use, from machines, surveys, etc.

-   Are any elements personally-identifiable, confidential, or restricted use that could be "notice-triggering" (see, for example, <a href="https://its.ucsc.edu/security/pii.html" target="_blank">this UC Santa Cruz webpage on Personal Identity Information</a>)?

-   What protections do your informed consent agreement and Committee for the Protection of Human Subjects (IRB) protocol guarantee? This will include how you will gather consent, collect, store, and share the data.    

-   Have you signed or made any other agreements that control the security that's needed to protect the data?

#### If you are using existing data

Often there is an agreement between you and whoever owns the data. A common form is a data use agreement that specifies the terms under which you can and cannot use the data and the protections that are required. The data owner could be:

-   A federal agency (e.g., National Institutes of Health, Centers for Medicare and Medicaid Services, Department of Education, US Census Bureau)

-   A state agency (e.g., Department of Public Health or Social Services)

-   Local police, fire, corrections or other office

-   Utilities

-   Private companies, such as health care providers, marketing firms, social media platforms, research organizations, research & development units  

-   Publishers, archives, and other third-parties who have curated the data or otherwise organized the data in a useful form

Typically, it is the superset of all of the above factors that define the protections that you need to put in place. At a minimum, the campus data classification standard sets the bar. Research IT can help you understand how your data are being governed and how to protect them.  

### Classifying your data at UC Berkeley

-   <a href="https://security.berkeley.edu/data-classification-standard" target="_blank">Data classification standard</a>

-   <a href="https://security.berkeley.edu/data-classification-guideline" target="_blank">Data classification guideline</a>

UC Berkeley's Information Security Office provides a <a href="https://security.berkeley.edu/data-classification-standard" target="_blank">standard</a> for classifying data sensitivity. This also encompasses the equipment on which the data are stored and computed. The data classification standard defines four <a href="https://security.berkeley.edu/data-classification-standard#plclassification" target="_blank">protection levels</a>, P1 - P4 (formerly PL0-PL3). The levels are characterized by the adverse impact that would be caused by unwanted disclosure or modification of the data. Notice how the frameworks discussed above -- IRB protocols, HIPAA rules, and the GDPR protections, for example -- all inform the standard.

The security office web site offers a step-by-step <a href="https://security.berkeley.edu/data-classification-guideline" target="_blank">guideline for classifying data</a> and a separate <a href="https://security.berkeley.edu/education-awareness/best-practices-how-tos/how-classify-research-data" target="_blank">guide focusing on research data</a>. Both are decision trees to help researchers identify the level appropriate to their data. Once you understand the agreements, regulations, and other frameworks that govern the sensitivity of your data, step through this guideline to determine its correct classification. <a href="https://research-it.berkeley.edu/consulting" target="_blank">Research IT</a> can navigate this process with you and help talk to the Information Security Office if needed.
