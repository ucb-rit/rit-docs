---
title: Securing your computing and storage systems
keywords: computing, storage, security
tags: [data security]
---

Securing your computing and storage systems
-----------------------------------------------

Knowing the appropriate classification for your data, you can properly secure your research environment. Again, the Information Security Office provides direction via the <a href="https://security.berkeley.edu/minimum-security-standards-electronic-information" target="_blank">Minimum Security Standards for Electronic Information</a> (MSSEI). These standards define the technical, administrative, and physical controls that must be in place for systems that handle data at each protection level. If controls are specified in a Data Use Agreement or other document that go beyond what campus requires, they can be added to the campus's baseline.

The MSSEI also defines three categories of device or use: Individual, Privileged and Institutional. A workstation or laptop that handles a small or moderate number of records would generally be considered an Individual device. A machine on which the user must have administrator ('root') privileges is classified as a Privileged device. A server on which more than a thousand records are processed is deemed to be an Institutional device. Each ascending category carries a greater set of protection requirements.

Armed with the protection level (from the Data Classification Standard) and the device/use category, you can now move to the MSSEI <a href="https://security.berkeley.edu/minimum-security-standards-electronic-information#baseline-profile-summary" target="_blank">Baseline Data Protection Profile Summary</a> to see a list of the required controls, as shown below.

![](https://lh5.googleusercontent.com/u03u4NDae6fZcvJCJ9JrDqwBdcw67nM2Y6nxY50vrBlS8-eDdxBLlSMXdUNnih8Wj3ohk2306s4swieBVR892P2B3BXpK1dKmpUkXsbQQD2XJRPHN8YugfyXTIHUmk8hI1hOgTCq)

Controls -- nearly four dozen in all -- range from removal of data that is not needed at the moment to security incident response plans and incident reporting training. The MSSEI provides detailed information about each.

Assessing your environment can be a daunting process to navigate. Research IT consultants have guided researchers through the process for years. Please <a href="https://research-it.berkeley.edu/consulting" target="_blank">contact us</a>.
