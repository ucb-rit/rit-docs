---
title: Data Classification & Security
keywords: data security, classification
tags: [data security]
---

Data Classification & Security 
===========================

Increasingly, data security frames our conversations about research data. Data security protects against the loss of confidentiality, integrity and availability of data. Your data are valuable and Research IT can help you take steps in order to protect them and the campus. We, as members of the campus workforce, are all responsible for information security. (New <a href="https://security.berkeley.edu/roles-and-responsibilities-policy" target="_blank">policies and practices</a> are coming soon!)

## Read More

* [Determining the sensitivity of your data](../../../services/research-data/data-classification-security/determining-sensitivity)
* [Securing your computing and storage systems](../../../services/research-data/data-classification-security/securing-systems)
* [Resources for working with sensitive data](../../../services/research-data/data-classification-security/resources)
