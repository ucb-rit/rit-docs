---
title: Globus at UC Berkeley
keywords: data transfer globus berkeley
tags: [data transfer globus berkeley]
---

Globus at UC Berkeley
===========================

Globus is a data transfer and storage service that allows one to easily and quickly move data between different resources (e.g., a personal computer, the Savio campus cluster, bDrive, and various others) and to share data on those resources with others. UC Berkeley has a Globus subscription (i.e., <a href="https://www.globus.org/subscriptions#:~:text=High%20Assurance%20and%20HIPAA%2FBAA,and%20controlled%20but%20unclassified%20data.">"High Assurance and HIPAA/BAA subscription"</a>) that provides a wide variety of functionality to all campus affiliates.


## Accessing Globus

The Globus user interface (UI) can be accessed through a web browser, such as Google Chrome. The following steps allow you access to the Globus UI:

1. In a browser (e.g., Chrome, Firefox, Safari), navigate to <a href="https://app.globus.org" target="_blank">app.globus.org</a>.
2. Search for **University of California, Berkeley**.
3. Select **Continue** to agree to the Globus terms of service and privacy policy (if this is the first time you are using Globus).
4. You will see the CalNet login screen. 
5. Log in with your individual CalNet credentials or your Special Purpose Account (SPA) from here. <a href="https://calnetweb.berkeley.edu/calnet-departments/special-purpose-accounts-spa/log-spa" target="_blank">Click here if you need help logging on to a SPA account</a>.
6. Once you log on, you will see a CILogon Information Release page. Generally you can leave the default selection and click **Accept**.
7. You will see a welcome screen acknowledging that you have Logged in. There is an option to link other accounts if this is not the first time that you are using Globus, but generally you can click **Continue**. 
8. If this is the first time you are using Globus, you will need to complete your sign up for your account on the next page by checking that you have read the Terms of Service and Privacy policy and click **Continue**. 
9. The following page will confirm the access the Globus Web App will have to your accounts. Click **Allow**. 
10. The following screen will display the File Manager. Click in the Collection search field and search for the Collection you would like to access. 
11.  If this is the first time you are accessing this collection, authentication will be required. Click **Continue**. For example, depending on the specific Collection you are trying to access, you will be sent to a page titled "Authentication/Consent Required" if this is the first time you are accessing this collection. You will be asked to consent to allow for Globus services to access data on your behalf and for the Globus endpoint at UC Berkeley to manage your credentials on that collection. Make sure to select **Allow** and **Continue** on the corresponding pages that come up to provide consent and register your credential. 
12. Once your credentials are registered and permission granted, you can now navigate back to File Manager and access your files and folders within the collection. 

It is generally recommended that an “incognito” browser window (e.g., a Google Chrome “incognito” or “private” browser window) be used when accessing the Globus UI so other credentials are ignored. This can also help avoid sending (old) login cookies. Incognito will also automatically forget your passwords when you close out at the end of a session. You can access an Incognito Google Chrome browser by clicking on the three vertical dots in the upper right corner of the browser and clicking “New Incognito Window” in the drop-down menu. You’ll know you are incognito when the address bar turns black at the top of the window and you’ll see the word “Incognito” in the top right of the browser window. For more instructions on this, please see <a href="https://support.google.com/chrome/answer/95464?hl=en&co=GENIE.Platform%3DDesktop" target="_blank">here</a> and <a href="https://www.computerworld.com/article/3356840/how-to-go-incognito-in-chrome-firefox-safari-and-edge.html" target="_blank">here</a>.

For additional general instructions and examples, including screenshots,  on how to log in and transfer files with Globus, see also Globus's <a href="https://docs.globus.org/how-to/get-started/" target="_blank">Step-by-Step Getting Started Guide</a>.


## Data Transfer Between Various Resources

With Globus, you can transfer data between various resources. These include:
<ul style="margin-left: 40px;">
<li>Your personal computer, using <a href="https://www.globus.org/globus-connect-personal">Globus Connect Personal</a></li>
<li>A lab or shared computer (either using Globus Connect Personal, setting up the computer as a Globus endpoint, and/or setting up <a href="https://www.globus.org/globus-connect-server"> Globus Connect Server version 5 (GCSv5)</a> on the computer to share collections with users)</li>
<li><a href="https://docs-research-it.berkeley.edu/services/high-performance-computing/user-guide/data/transferring-data/using-globus-connect-savio">Savio</a>, including home directories, scratch directories, and condo storage</li>
<li><a href="https://docs-research-it.berkeley.edu/services/srdc/srdc_user_guide/#transferring-data">Research IT’s SRDC secure computing environment</a></li>
<li>Departmental computing facilities, including the Statistical Computing Facility and Econometrics Laboratory</li>
<li>An AEoD virtual machine, using Globus Connect Personal</li>
<li>bDrive/<a href="https://docs.globus.org/how-to/access-google-storage">Google Drive</a></li>
<li>Box (<a href="https://bconnected.berkeley.edu/collaboration-services/box">available via bConnected</a>)</li>
<li>Various other cloud platforms, including <a href="https://www.globus.org/partners/wasabi">Wasabi</a>, <a href="https://docs.globus.org/how-to/access-aws-s3">AWS</a>, and <a href="https://docs.globus.org/how-to/gcsv5.3/access-google-cloud-storage/">Google Cloud Storage</a></li>
</ul><p>&nbsp;</p>


## Sharing Your Data with Collaborators

Users can set up <a href="https://docs.globus.org/globus-connect-server/v5/reference/collection/#guest_collections">guest collections</a> (also termed shared endpoints in some cases) on a resource that they have access to in order to share data with collaborators that they choose. This gives the collaborator access to specific directories that you choose. The collaborator with whom you share does not have to have an account on the resources. This includes:
<ul style="margin-left: 40px;">
<li><a href="https://docs-research-it.berkeley.edu/services/high-performance-computing/user-guide/data/transferring-data/using-globus-share-savio/">Sharing directories in your Savio home, scratch, or condo storage directory</a></li>
<li><a href="https://docs.globus.org/how-to/share-files/">Sharing folders in bDrive / Google Drive</a></li>
<li><a href="https://docs.globus.org/how-to/share-files/">Sharing directories/folders on various cloud resources, including Box, Wasabi, and Google Cloud Storage</a></li>
</ul><p>&nbsp;</p>

<a href="https://www.globus.org/subscriptions#:~:text=High%20Assurance%20and%20HIPAA%2FBAA,and%20controlled%20but%20unclassified%20data.">UC Berkeley’s Globus High Assurance and HIPAA/BAA subscription</a> also allows you to set up <a href="https://docs.globus.org/how-to/guest-collection-share-and-access/">guest collections</a> (for sharing data) on your personal or lab computer that are discoverable by other Globus users. We can upgrade any user at UC Berkeley to <a href="https://docs.globus.org/faq/subscriptions/#what_is_globus_plus_and_how_do_i_get_globus_plus">Globus Plus</a> as part of the subscription. Globus Plus users can, for example, create guest collections on their personal computers (using <a href="https://www.globus.org/globus-connect-personal">Globus Connect Personal</a>) and transfer files between Globus Connect Personal endpoints (e.g., on their personal computers). Moreover, Globus Plus allows Globus Connect Personal to be used on endpoint devices as identifiable endpoints. The Globus subscription also allows researchers, for example, to install <a href="https://www.globus.org/globus-connect-server">Globus Connect Server (version 5)</a> on a lab computer so they could set up endpoints and collections that others could find and access, and so share data with other researchers. Note that you do not need Globus Plus if transferring files to/from a Globus server endpoint (e.g. your campus cluster or supercomputing center – such as Savio), or if you want to share files from a Globus server endpoint. 

When do you need Globus Plus?

<ul style="margin-left: 40px;">
<li>To transfer data between two Globus Connect Personal endpoints (e.g. your desktop system and a laptop).</li>
<li>To share data from a Globus Connect Personal endpoint (e.g. your laptop or a desktop system).</li>
<li>To transfer data from/to a guest collection that is hosted on a Globus Connect Personal endpoint.</li>
</ul>

When do you <b>not</b> need Globus Plus?

<ul style="margin-left: 40px;">
<li>To transfer or share data between two Globus managed endpoints (e.g. two multi-user systems at different universities, each running a Globus server).</li>
<li>To transfer data between a managed endpoint (e.g. Savio) to a Globus Connect Personal endpoint (e.g. your desktop).</li>
</ul>

Note that if your collaborator needs Globus Plus to download data, and is not at UC Berkeley, we cannot provide Globus Plus to that person.

Also note that, by default, files on a Globus Connect Personal endpoint (e.g. your laptop or desktop) may not be shareable. You will need to configure that via the instructions at these links: <a href="https://docs.globus.org/how-to/globus-connect-personal-mac/#configuration">Mac</a>, <a href="https://docs.globus.org/how-to/globus-connect-personal-windows/#configuration">Windows</a>, <a href="https://docs.globus.org/how-to/globus-connect-personal-linux/#config-paths">Linux</a>.

If you need to upgrade your Globus account to Globus Plus, please contact us at brc-hpc-help@berkeley.edu  to request a Globus Plus invite. For more details see <a href="https://docs.globus.org/faq/subscriptions/#what_is_globus_plus_and_how_do_i_get_globus_plus">“What is Globus Plus and How do I get Globus Plus?”</a>


## Getting help with Globus

Follow these links for help with:
<ul style="margin-left: 40px;">
<li>Downloading and installing <a href="https://www.globus.org/globus-connect-personal">Globus Connect Personal (free)</a></li>
<li><a href="https://docs-research-it.berkeley.edu/services/high-performance-computing/user-guide/data/transferring-data/using-globus-connect-savio/">Using Globus to transfer data to/from Savio</a></li>
<li><a href="https://www.globus.org/globus-connect-personal">Using Globus with your personal computer</a></li>
<li><a href="https://docs.google.com/document/d/1yPFJd7g8HQL9_PPorFxNhSzKrT83kG_Mg_dfhklRiiI/edit?usp=sharing">Migrating Data from Google Drive/bDrive to Box using Globus (bConnected document)</a></li>
<li><a href="https://docs.globus.org/how-to/access-aws-s3/">How to Access Your Files on AWS S3 with Globus</a></li>
<li><a href="https://docs.globus.org/how-to/access-google-storage/">How to Access Your Files on Google Drive or Google Cloud with Globus</a></li>
<li><a href="https://knowledgebase.wasabi.com/hc/en-us/articles/360029802471-How-do-I-use-Globus-Connect-Server-with-Wasabi-#Step4">Transferring Data between Wasabi S3 Buckets and your Personal Computer (Wasabi documentation)</a></li>
</ul><p>&nbsp;</p>


For the following, please contact <a href="mailto:research-it-consulting@berkeley.edu">research-it-consulting@berkeley.edu</a>:
<ul style="margin-left: 40px;">
<li><a href="https://www.globus.org/globus-connect-server">Setting up a lab computer/facility as an endpoint</a> that can manage collections accessible and discoverable by collaborators.</li>
<li><a href="https://docs.globus.org/faq/globus-connect-endpoints/#are_transfers_between_globus_connect_personal_endpoints_possible">Setting up Globus Connect Personal to connect two personal computers</a> to transfer data between or to share data with collaborators.</li>
</ul><p>&nbsp;</p>


## UCB Globus Collections/Endpoints

<a href="https://www.globus.org/subscriptions#:~:text=High%20Assurance%20and%20HIPAA%2FBAA,and%20controlled%20but%20unclassified%20data.">UC Berkeley’s Globus High Assurance and HIPAA/BAA subscription</a> provides users with access to <a href="https://www.globus.org/connectors">premium storage connectors</a> (which support storage systems such as Google Drive, AWS S3, Google Cloud Storage, Wasabi, and Cloudian), and thus <a href="https://docs.globus.org/high-assurance/#mapped_collections">mapped collections</a>, which are created by the <a href="https://docs.globus.org/globus-connect-server/v5.4/reference/endpoint/role/#administrator">endpoint administrator</a>. Access to mapped collections requires an account on the endpoint’s host system. Mapped collections created on <a href="https://docs.globus.org/globus-connect-server/v5/reference/storage-gateway/#storage_gateways">storage gateways</a> that are flagged for <a href="https://docs.globus.org/high-assurance/#using_high_assurance_collections">high-assurance</a> data are automatically configured for use with protected data.

The endpoint administrator specifies the identities required for access and an authentication assurance timeout period. If a user attempts access without having authenticated as required within the timeout period, the user will be prompted to authenticate with the required identity.

The steps for discovering and using mapped collections to access data are described in the Globus how-to guide, "<a href="https://docs.globus.org/how-to/mapped-collection-find-and-use">Find and use a mapped collection from the Globus web app</a>".

The names of the mapped collections users should use to transfer data and set up <a href="https://docs.globus.org/globus-connect-server/v5/reference/collection/#guest_collections">guest collections</a> for the various services/connectors are specified in the table below. Users can search for the collections within the Globus web app when they wish to transfer files among collections (and to set up guest collections), including their own Globus Connect Personal endpoint. Users can also bookmark these for later reuse without having to search for them.

| UCB System | UCB Globus Collection/Endpoint Name(s) |
| --------- | --------- |
| [BRC (Savio)](https://docs-research-it.berkeley.edu/services/high-performance-computing/overview/) | [ucb#brc](https://app.globus.org/file-manager/collections/8dfcaab6-fe4e-4376-9430-785631512d4a)<br>[ucb#brc-basic](https://app.globus.org/file-manager/collections/3af02473-4810-46e4-a330-4885da0cead5)<br>[UCB BRC Savio Posix Data](https://app.globus.org/file-manager/collections/d8539a62-821f-477d-a592-43b2af60ab28) |
| [Google Cloud Storage](https://docs.globus.org/how-to/access-google-storage/) | [UCB Google Cloud Storage Collection II](https://app.globus.org/file-manager/collections/013233f0-0ba5-48fc-a928-c22b96e73eba) | 
| [Wasabi](https://www.globus.org/partners/wasabi) | [UCB S3 Wasabi Data](https://app.globus.org/file-manager/collections/43a85636-487e-4df4-8945-60c2876d6682) | 
| [Amazon Web Services (AWS)](https://docs.globus.org/how-to/access-aws-s3/) | [UCB AWS S3 Collections](https://app.globus.org/file-manager/collections/5f952031-43e3-4d43-8c46-3ac29b1d4c28) |
| [Google Drive (bDrive)](https://docs.globus.org/how-to/access-google-storage/) | [UCB Google Drive Collections](https://app.globus.org/file-manager/collections/5306d778-c60e-42c7-bfe0-7789e18da53b) | 
| Cloudian Storage | [UCB Cloudian Storage Collections](https://app.globus.org/file-manager/collections/6f01a53e-c0a4-4860-a42c-ee0d0f8938b3) | 
| [Box](https://www.globus.org/connectors/box) | [UCB Box Collections](https://app.globus.org/file-manager/collections/85379fb5-0593-4a9b-9faa-c260c9f5960a) | 

Note that the <a href="https://app.globus.org/file-manager/collections/8dfcaab6-fe4e-4376-9430-785631512d4a">ucb#brc</a> and <a href="https://app.globus.org/file-manager/collections/d8539a62-821f-477d-a592-43b2af60ab28">UCB BRC Savio Posix Data</a> endpoints/collections point to the same data on Savio. Keep in mind also that there currently may be multiple Savio Globus endpoints/collections with the same name (<code>ucb#brc</code> or similar). Users can also do a collections search on "8dfcaab6-fe4e-4376-9430-785631512d4a" (which is the Globus UUID associated with the <code>ucb#brc</code> collection) in the globus collections search box in the Globus UI, which is labeled as "Managed Mapped Collection on UCB/BRC DTN01 Endpoint" in the globus UI. This is the new Globus Connect Server version 5 (GCSv5) connector and this should work for all users. 

Also note that in some instances, when users are trying to access Savio Globus collections or the other endpoints/collections in the table above, they may be prompted to re-authenticate first, and they might be given a choice to login with various different identities. In that case it is usually best to login with an identity that includes the username as part of the ID, e.g. username@identity.69b1b.8443.data.globus.org.


## Automating and Scheduling Data Transfers with Globus

Globus provides multiple methods to automate and schedule transfers, including the  <a href="https://docs.globus.org/faq/transfer-sharing/#how_can_i_automate_or_schedule_transfers_with_the_globus_service">Globus Timer service</a> (see <a href="https://services.northwestern.edu/TDClient/30/Portal/KB/ArticleDet?ID=2020">here</a> also), which allows you to schedule data workflows for a certain date and time with the option to repeat these workflows on a specified schedule. Note that, as of December 2023, this service is also supported for high assurance collections as well, which means that it can also be used with the Savio <a href="https://app.globus.org/file-manager/collections/8dfcaab6-fe4e-4376-9430-785631512d4a">ucb#brc</a> endpoint or with the other mapped collections listed in the table in the previous section.

It is also possible to automate transfers with the Globus service using scripts that make use of the <a href="https://docs.globus.org/api">Globus API</a> or the <a href="https://docs.globus.org/cli">Globus CLI</a>. Examples of such scripts are available <a href="https://github.com/globus/automation-examples">here</a>. These scripts can also be run on a schedule using cron or some other system scheduler service. Globus users also now have an option to <a href="https://docs.globus.org/how-to/automate-with-service-account/">use application credentials or service accounts to automate data transfers</a> as well.


