---
title: rClone at Berkeley
keywords: data transfer rclone
tags: [data transfer rclone]
---

rClone at Berkeley
===========================

to - add General information about rClone at UCB

!!! note
    rclone is currently available as a software module on the Savio DTN. HPCS is planning to update it to a newer version soon.

## rClone on Savio

* <a href="https://docs-research-it.berkeley.edu/services/high-performance-computing/user-guide/data/transferring-data/rclone-box-bdrive/">bDrive/Box to Savio DTN</a>
