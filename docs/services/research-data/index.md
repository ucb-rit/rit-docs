---
title: Research Data
keywords: research data
tags: [research data]
---

# Research Data

## Mission
Research IT supports the research mission of UC Berkeley by helping
faculty, students, and staff, across disciplines and departments, work with
research data. In partnership with the Library, Research IT provides the
Research Data Management (RDM) Program aimed at helping researchers
navigate the complex landscape of managing data before, during, and after
research. Through consultations, workshops/trainings, and the documentation
on this site, RDM Program team members advise on best practices when it
comes to research data management and work with researchers to identify
appropriate (and secure) tools and systems that meet their research data
needs.

## People

<a href="https://researchdata.berkeley.edu/people" target="_blank" rel="noopener noreferrer"> Research Data Management Program Team</a>

Program: <a href="https://researchdata.berkeley.edu/" target="_blank" rel="noopener noreferrer"> Research Data Management</a>

Partnership: <a href="https://www.lib.berkeley.edu/" target="_blank" rel="noopener noreferrer"> The Library </a>  
