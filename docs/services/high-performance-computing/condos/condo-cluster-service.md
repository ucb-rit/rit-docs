---
title: Condo Cluster Service
keywords: high performance computing, berkeley research computing, status, announcements
tags: [hpc]
---

# Condo Cluster Service 

!!! summary
    BRC manages <strong>Savio</strong>,&nbsp;the high-performance computational cluster for research computing. Designed as a turnkey computing resource, it features flexible usage and business models, and professional system administration. Unlike traditional clusters, <strong>Savio</strong> is a collaborative system wherein the majority of nodes are purchased and shared by the cluster users, known as&nbsp;<em>condo</em>&nbsp;owners.

    The model for sustaining computing resources<strong>&nbsp;</strong>is premised on faculty and principal investigators purchasing compute nodes (individual servers)&nbsp;from their grants or other available funds which are then added to the cluster. This allows PI-owned nodes to take advantage of the high speed Infiniband interconnect and high performance Lustre parallel filesystem storage associated with <strong>Savio</strong>. Operating costs for managing and housing PI-owned compute nodes are waived in exchange for letting other users make use of any idle compute cycles on the PI-owned nodes. PI owners have priority access to computing resources equivalent to those purchased with their funds, but can access more nodes for their research if needed. This provides the PI with much greater flexibility than owning a standalone cluster.<strong><strong><strong><a name="Program Details" id="Program Details"></a></strong></strong></strong>

## Program Details

<p>Compute node equipment is purchased and maintained based on a 5-year lifecycle. PIs owning the nodes will be notified during year 4 that the nodes will have to be upgraded before the end of year 5. If the hardware is not upgraded by the end of 5 years, the PI may donate the equipment to <strong>Savio</strong> or take possession of the equipment (removal of the equipment from <strong>Savio</strong> and transfer to another location is at the PI's expense); nodes left in the cluster after five years may be removed and disposed of at the discretion of the BRC program manager</p>
<p>Once a PI has decided to participate, the PI or their designate works with the HPC Services manager and IST teams to procure the desired number of compute nodes and allocate the needed storage. There is a 4-node minimum buy-in for any given compute pool (and all 4 nodes must be the same whether it be the Standard, HTC, Bigmem, or GPU nodes. GPU nodes are the most expensive; therefore, if a group has already purchased the 4-node minimum of any other type of node, they can purchase and add single GPU nodes to their Condo).&nbsp; Generally, procurement takes about three months from start to finish. In the interim, a test condo queue with a small allocation will be set up for the PI's users in anticipation of acquiring the new equipment. Users may submit jobs to the general queues on the cluster using their <a href="../../../../services/high-performance-computing/getting-account/faculty-computing-allowance">Faculty Computing Allowance</a>. Jobs are subject to general queue limitations and guaranteed access to contributed cores is not provided until purchased nodes are provisioned.<a name="Hardware" id="Hardware"></a></p>
<p>All group members have equal access to the condo resources, via a condo-specific Slurm QoS (the 'floating reservation' described below). The expectation is that the research group will collectively manage use of the resources by individual members.</p>

## Hardware Information

!!! danger "Warranty" 
    **Each system has a warranty of 5 years**

<p>Basic specifications for the systems listed below:</p>

<!--p>Detailed specifications for each node type:</p-->
??? info "General Computing Node (256 GB RAM)"
    |                  | General Computing Node (256 GB RAM) |
    | ---------------- |----------------------------------- |
    | Processors       | Dual-socket, 28-core, 2.1 GHz Intel Xeon Gold 6330 processors (56 cores/node) |
    | Memory           | 256 GB (16 x 16GB) 2666 Mhz DDR4 RDIMMs |
    | Interconnect     | 100 Gb/s Mellanox ConnectX6 HDR-100 Infiniband interconnect |
    | Hard Drive       | 1.92 TB NVMe SSD drive (Local swap and log files) |
    | Notes            | These come in sets of 4, and the minimum buy-in is 4 nodes |
    | Current Approximate Price (with tax) | ~$42,500 for a Dell C6400 chassis with 4 nodes + 4 EDR 2M cables |

??? info "Big Memory or HTC Computing Node (512 GB RAM)"
    |                  | Big Memory or HTC Computing Node (512 GB RAM) |
    | ---------------- |--------------------------------------- |
    | Processors       | Dual-socket, 28-core, 2.0 GHz Intel Xeon Gold 6330 processors (56 cores/node) |
    | Memory           | 512 GB (16 x 32 GB) 3200 Mhz DDR4 ECC REG |
    | Interconnect     | 100 Gb/s Mellanox ConnectX 6 EDR Infiniband interconnect |
    | Hard Drive       | 1.92 TB NVMe SSD drive (Local swap and log files) |
    | Notes            | These come in sets of 4, and the minimum buy-in is 4 nodes |
    | Current Approximate Price (with tax) | ~$42,000 for 4 nodes + 4 2M cables |

??? info "Very Large Memory Computing Node (1.5 TB RAM)"
    |                  | Very Large Memory Computing Node (1.5 TB RAM) |
    | ---------------- |--------------------------------------- |
    | Processors       | Dual-socket, 26-core, 2.1 GHz Intel Xeon Gold 6230 processors (52 cores/node) |
    | Memory           | 1.5 TB (24 x 64GB) DDR4 |
    | Interconnect     | 100 Gb/s Mellanox ConnectX5 EDR Infiniband interconnect |
    | Hard Drive       | 1 TB HDD 7.2K RPM (Local swap and log files) |
    | Notes            | These can be purchased one by one, but the minimum buy-in is **2 nodes** |
    | Current Approximate Price (with tax) | $16,500 per node + $100 for 1 ea. EDR 2M cable |

??? info "GPU Computing Node"
    We are currently supporting the 8-way L40S server for $71,000 and the 8-way H100 Dell server for $250,000 as a standard node offering for the `savio4_gpu` partition. Please contact us (see below) if you are interested in purchasing GPUs for a condo.

<!--
No longer valid but don't have new (L40S or otherwise) standard
??? info "GPU Computing Node"
    |                  | GPU Computing Node (A40) |
    | ---------------- |--------------------------------------- |
    | Processors       | 1 AMD EPYC 7302P processor, 3 Ghz (16 cores/node) |
    | Memory (CPU)     | 256 GB (8 X 32 GB) 3200 Mhz DDR4 ECC RDIMMs |
    | Interconnect     | 100 Gb/s Mellanox ConnectX5 EDR Infiniband interconnect |
    | GPU              | 2 Nvidia A40 accelerator boards with 48 GB GPU memory each |
    | Hard Drive       | 960 GB SSD drive (Local swap and log files) |
    | Notes            | These can be purchased one by one, and the minimum buy-in is one node |
    | Current Approximate Price (with tax) | $19,100 for a single node |
-->

### Hardware Purchasing

Prospective condo owners should <a href="http://research-it.berkeley.edu/contact" target="_blank">contact us</a> for current pricing and prior to purchasing any equipment to insure compatibility. If you are interested in other hardware configurations (e.g., HTC/Serial nodes), please <a href="http://research-it.berkeley.edu/contact" target="_blank">contact us</a>. BRC will assist with entering a compute node purchase requisition&nbsp;on behalf of UC Berkeley faculty.

### Software

Prospective Condo owners should review the <a href="../../../../services/high-performance-computing/overview/system-overview#System_Software" class="toc-filter-processed">System Software</a> section of the <a href="../../../../services/high-performance-computing/overview/system-overview">System Overview</a> page to confirm that their applications are compatible with Savio's operating system, job scheduler and operating environment.

### Storage

All institutional and condo users have a 30 GB home directory with backups; in addition, each research group is eligible to receive up to 200 GB of shared project space (30 GB for Faculty Computing Allowance accounts and 200 GB for Condo accounts) to hold research specific application software that is shared among the users of a research group. All users have access to the Savio high performance scratch filesystem for non-persistent data. Users or projects needing more space for persistent data can also purchase additional performance tier storage from IST at the current&nbsp;<a href="https://technology.berkeley.edu/storage" target="_blank">rate</a>. For even larger storage needs, Condo partners may also take advantage of the <a href="../../../../services/high-performance-computing/condos/condo-storage-service">Condo Storage service</a>, which provides low-cost storage for very large data needs (minimum 25 TB).

### Network

A Mellanox infiniband 36-port unmanaged leaf switch is used for every 24 ea. compute nodes.

### Job scheduling

We will set up a floating reservation equivalent to the number of nodes that you contribute to the Condo to provide priority access to you and your users. You can determine the run time limits for your reservation. If you are not using your reservation, then other users will be allowed to run jobs on unused nodes. If you submit a job to run when all nodes are busy, your job will be given priority over all other waiting jobs to run, but your job will have to wait until nodes become free in order to run. We do not do pre-emptive scheduling where running jobs are killed in order to give immediate access to priority jobs.

Note that the configuration above means that Condos do not have dedicated/reserved nodes. The basic premise of Condo participation is to facilitate the sharing of unused resources. Dedicating or reserving compute resources works counter to sharing, so this is not possible in the Condo model. As an alternative, PIs can purchase nodes and set them up as a Private Pool in the Condo environment, which will allow a researcher to tailor the access and job queues to meet their specific needs. Private Pool compute nodes will share the HPC infrastructure along with the Condo cluster; however, researchers will have to cover the support costs for BRC staff to manage their compute nodes. Please contact us for rates for Private Pool compute nodes.

## Charter Condo Contributors

The following is a list of those who initially contributed Charter nodes to the Savio Condo, thus helping launch the Savio cluster:

|Contributor                                                                                            | Affiliation                                           |
| ----------------------------------------------------------------------------------------------------- | ----------------------------------------------------- |
| <a href="http://astro.berkeley.edu/faculty-profile/eliot-quataert" target="_blank">Eliot Quataert</a> | Theoretical Astrophysics Center, Astronomy Department |
| <a href="http://astro.berkeley.edu/faculty-profile/eugene-chiang" target="_blank">Eugene Chiang</a>   | Astronomy Department                                  | 
| <a href="http://astro.berkeley.edu/faculty-profile/chris-mckee" target="_blank">Chris McKee</a>       | Astronomy Department                                  |
| <a href="http://astro.berkeley.edu/faculty-profile/richard-klein" target="_blank">Richard Klein</a>   | Astronomy Department                                  |
| <a href="http://physics.berkeley.edu/?textonly=0&amp;option=com_dept_management&amp;Itemid=312&amp;task=view&amp;id=3319" target="_blank">Uros Seljak</a> | Physics Department | 
| <a href="http://astro.berkeley.edu/faculty-profile/jon-arons" target="_blank">Jon Arons</a>           | Astronomy Department                                  |
| <a href="http://chem.berkeley.edu/faculty/cohen/index.php" target="_blank">Ron Cohen</a>              | Department of Chemistry, Department of Earth and Planetary Science | 
| <a href="http://climate.geog.berkeley.edu/~jchiang/Lab/Home.html" target="_blank">John Chiang</a>     | Department of Geography and Berkeley Atmospheric Sciences Center   |
| <a href="http://faculty.ce.berkeley.edu/chow/research.html" target="_blank">Fotini Katopodes Chow</a> | Department of Civil and Environmental Engineering |
| <a href="http://www.nuc.berkeley.edu/people/jasmina_vujic" target="_blank">Jasmina Vujic</a>          | Department of Nuclear Engineering |
| <a href="http://sekhon.berkeley.edu/" target="_blank">Jasjeet Sekhon</a>                              | Department of Political Science and Statistics |
| <a href="http://www.nuc.berkeley.edu/people/rachel-slaybaugh" target="_blank">Rachel Slaybaugh</a>    | Nuclear Engineering |
| <a href="http://www.nuc.berkeley.edu/people/massimiliano_fratoni" target="_blank">Massimiliano Fratoni</a> | Nuclear Engineering |
| <a href="http://mcb.berkeley.edu/faculty/all/nikaidoh" target="_blank">Hiroshi Nikaido,</a>           | Molecular and Cell Biology |
| <a href="http://qb3.berkeley.edu/administration/" target="_blank">Donna Hendrix,</a>                  | Computation Genomics Research Lab |
| <a href="http://dlab.berkeley.edu/justin-mccrary" target="_blank">Justin McCrary,</a>                 | Director D-Lab |
| <a href="http://hubbard.berkeley.edu/" target="_blank">Alan Hubbard,</a>                              | Biostatistics, School of Public Health |
| <a href="https://vanderlaan-group.github.io/" target="_blank">Mark van der Laan,</a>                  | Biostatistics and Statistics, School of Public Health |
| <a href="http://seismo.berkeley.edu/~manga/" target="_blank">Michael Manga,</a>                       | Department of Earth and Planetary Sciences |
| <a href="https://gspp.berkeley.edu/directories/faculty/solomon-hsiang" target="_blank">Sol Shiang</a> | Goldman School of Public Policy |
| <a href="http://physics.berkeley.edu/people/faculty/jeffrey-neaton" target="_blank">Jeff Neaton</a>   | Physics |
| <a href="https://neuscammanlab.com/" target="_blank">Eric Neuscamman</a>                              | College of Chemistry |
| <a href="http://www.me.berkeley.edu/people/faculty/m-reza-alam" target="_blank">M. Alam Reza</a>      | Mechanical Engineering |
| <a href="http://profiles.ucsf.edu/elaine.tseng" target="_blank">Elaine Tseng</a>                      | UCSF School of Medicine |
| <a href="http://surgery.ucsf.edu/faculty/adult-cardiothoracic-surgery/julius-m-guccione,-jr,-phd.aspx" target="_blank">Julius Guccione</a> | UCSF Department of Surgery | 
| <a href="http://statistics.berkeley.edu/ryan-lovett" target="_blank">Ryan Lovett</a>                  | Statistical Computing Facility |
| <a href="http://www.cchem.berkeley.edu/dtlgrp/" target="_blank">David Limmer</a>                      | College of Chemistry |
| <a href="http://ib.berkeley.edu/labs/bachtrog/" target="_blank">Doris Bachtrog</a>                    | Integrative Biology |
| <a href="http://stage.cchem.berkeley.edu/~kranthi/" target="_blank">Kranthi Mandadapu</a>             | College of Chemistry |
| <a href="http://perssongroup.lbl.gov/" target="_blank">Kristin Persson</a>                            | Department of Materials Science and Engineering |
| <a href="http://chrzan.mse.berkeley.edu/" target="_blank">Daryl Chrzan</a>                            | Department of Materials Science and Engineering |
| <a href="http://eps.berkeley.edu/people/william-boos" target="_blank">William Boos</a>                | Earth and Planetary Science |
| <a href="https://astro.berkeley.edu/faculty-profile/daniel-weisz" target="_blank">Daniel Weisz</a>    | Department of Astronomy |
| <a href="https://www.sudmantlab.org/" target="_blank">Peter Sudmant</a>                               | Integrative Biology |
| <a href="https://moorjanilab.org/" target="_blank">Priya Moorjani</a>                                 | Molecular and Cell Biology | 

<!--p><a href="http://astro.berkeley.edu/faculty-profile/eliot-quataert" target="_blank">Eliot Quataert</a>, Theoretical Astrophysics Center, Astronomy Department<br><a href="http://astro.berkeley.edu/faculty-profile/eugene-chiang" target="_blank">Eugene Chiang</a>, Astronomy Department<br><a href="http://astro.berkeley.edu/faculty-profile/chris-mckee" target="_blank">Chris McKee</a>, Astronomy Department<br><a href="http://astro.berkeley.edu/faculty-profile/richard-klein" target="_blank">Richard Klein</a>, Astronomy Department<br><a href="http://physics.berkeley.edu/?textonly=0&amp;option=com_dept_management&amp;Itemid=312&amp;task=view&amp;id=3319" target="_blank">Uros Seljak</a>, Physics Department<br><a href="http://astro.berkeley.edu/faculty-profile/jon-arons" target="_blank">Jon Arons</a>, Astronomy Department<br><a href="http://chem.berkeley.edu/faculty/cohen/index.php" target="_blank">Ron Cohen</a>, Department of Chemistry, Department of Earth and Planetary Science<br><a href="http://climate.geog.berkeley.edu/~jchiang/Lab/Home.html" target="_blank">John Chiang</a>, Department of Geography and Berkeley Atmospheric Sciences Center<br><a href="http://faculty.ce.berkeley.edu/chow/research.html" target="_blank">Fotini Katopodes Chow</a>, Department of Civil and Environmental Engineering<br><a href="http://www.nuc.berkeley.edu/people/jasmina_vujic" target="_blank">Jasmina Vujic</a>, Department of Nuclear Engineering<br><a href="http://sekhon.berkeley.edu/" target="_blank">Jasjeet Sekhon</a>, Department of Political Science and Statistics<br><a href="http://www.nuc.berkeley.edu/people/rachel-slaybaugh" target="_blank">Rachel Slaybaugh</a>, Nuclear Engineering<br><a href="http://www.nuc.berkeley.edu/people/massimiliano_fratoni" target="_blank">Massimiliano Fratoni</a>, Nuclear Engineering<br><a href="http://mcb.berkeley.edu/faculty/all/nikaidoh" target="_blank">Hiroshi Nikaido,</a> Molecular and Cell Biology<br><a href="http://qb3.berkeley.edu/administration/" target="_blank">Donna Hendrix,</a> Computation Genomics Research Lab<br><a href="http://dlab.berkeley.edu/justin-mccrary" target="_blank">Justin McCrary,</a> Director D-Lab<br><a href="http://hubbard.berkeley.edu/" target="_blank">Alan Hubbard,</a>&nbsp;Biostatistics, School of Public Health<br><a href="https://vanderlaan-group.github.io/" target="_blank">Mark van der Laan,</a>&nbsp;Biostatistics and Statistics, School of Public Health<br><a href="http://seismo.berkeley.edu/~manga/" target="_blank">Michael Manga,</a>&nbsp;Department of Earth and Planetary Sciences<br><a href="https://gspp.berkeley.edu/directories/faculty/solomon-hsiang" target="_blank">Sol Shiang</a>, Goldman School of Public Policy<br><a href="http://physics.berkeley.edu/people/faculty/jeffrey-neaton" target="_blank">Jeff Neaton</a>, Physics<br><a href="https://neuscammanlab.com/" target="_blank">Eric Neuscamman</a>, College of Chemistry<br><a href="http://www.me.berkeley.edu/people/faculty/m-reza-alam" target="_blank">M. Alam Reza</a>, Mechanical Engineering<br><a href="http://profiles.ucsf.edu/elaine.tseng" target="_blank">Elaine Tseng</a>, UCSF School of Medicine<br><a href="http://surgery.ucsf.edu/faculty/adult-cardiothoracic-surgery/julius-m-guccione,-jr,-phd.aspx" target="_blank">Julius Guccione</a>, UCSF Department of Surgery<br><a href="http://statistics.berkeley.edu/ryan-lovett" target="_blank">Ryan Lovett</a>, Statistical Computing Facility<br><a href="http://www.cchem.berkeley.edu/dtlgrp/" target="_blank">David Limmer</a>, College of Chemistry<br><a href="http://ib.berkeley.edu/labs/bachtrog/" target="_blank">Doris Bachtrog</a>, Integrative Biology<br><a href="http://stage.cchem.berkeley.edu/~kranthi/" target="_blank">Kranthi Mandadapu</a>, College of Chemistry<br><a href="http://perssongroup.lbl.gov/" target="_blank">Kristin Persson</a>, Department of Materials Science and Engineering<br><a href="http://chrzan.mse.berkeley.edu/" target="_blank">Daryl Chrzan</a>, Department of Materials Science and Engineering<br><a href="http://eps.berkeley.edu/people/william-boos" target="_blank">William Boos</a>, Earth and Planetary Science<br><a href="https://astro.berkeley.edu/faculty-profile/daniel-weisz" target="_blank">Daniel Weisz</a>, Department of Astronomy<br><a href="https://www.sudmantlab.org/" target="_blank">Peter Sudmant</a>, Integrative Biology<br><a href="https://moorjanilab.org/" target="_blank">Priya Moorjani</a>, Molecular and Cell Biology</p-->

## Faculty Perspectives

<p><strong>UC Berkeley Professor of Astrophysics Eliot Quataert</strong> speaks at the BRC Program Launch (22 May 2014) on the need for local high performance computing (HPC) clusters, distinct from national resources such as NSF, DOE (NERSC), and NASA.</p>
<iframe allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/u5aW_USfiaA?rel=0" width="560"></iframe><p>
<strong>UC Berkeley Professor of Integrated Biology Rasmus Nielsen</strong> speaks at the BRC Program Launch (22 May 2014)&nbsp;about the transformative effect of using HPC&nbsp;in genomics research.</p>
<iframe allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/lpVEBxFciw0?rel=0" width="560"></iframe><p>
&nbsp;</p>

