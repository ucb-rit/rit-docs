---
title: Instructional Computing Allowance
keywords: 
tags: [hpc]
---

# Instructional Computing Allowance

<p>Instructional Computing Allowances (ICAs) provide a no-cost allocation on the Savio high-performance computing cluster to instructors who need significant computational resources for their courses.</p>
<p><strong>Instructional Computing Allowances</strong> are intended for any instructor of record of an official UC Berkeley course. Each Instructional Computing Allowance is based upon a partnership agreement between BRC and the instructor. The agreement specifies a designated Point of Contact (POC) for the course (usually a GSI or staff member) who is familiar with high performance computing in Savio, and who handles software installation and all student troubleshooting and support. The designated point of contact can escalate issues to BRC staff, and work with BRC staff on software installation and account provisioning. This model provides Savio access to students learning computationally-intensive research techniques and ensures that BRC support staff are not overwhelmed..</p>

#### Allowance Requests 

To apply for a new ICA allocation, please visit the <a href="https://mybrc.brc.berkeley.edu/" target="_blank">MyBRC User Portal</a> in your web browser. After following the on-screen instructions in the portal and registering and/or logging in to the portal, you must first review and sign the cluster User Access Agreement Form on the Home ("Welcome") page (if you haven't already done so from within the portal previously) by clicking on the "Review" button, and then clicking on the "Create" button to request the creation of an ICA project on the same Home ("Welcome") page. You will then reach the “Create a Project” page where you select the Savio cluster, then on to the “New Savio Project” page where you can continue with the on-screen prompts to set up and manage the ICA. The PI may also ask another person to set up an ICA project on their behalf. 

As of February 2024, a Memorandum of Understanding (MOU) form will be generated automatically when the ICA request is submitted via the MyBRC portal. Once the ICA request is submitted, the details of the request will be reviewed, confirmed, and edited (if and as needed) by administrators. Once this process is completed, the PI/researcher will be notified, and based on the (edited if needed) details of the PI/researcher's purchase request, an unsigned MOU will be generated which the PI/researcher can download to their computer from the MyBRC portal by clicking on the "Download" button on the MyBRC portal ICA request page. The PI/researcher can then review and sign the downloaded MOU and then upload the signed MOU form back up to the MyBRC portal for submission to and review by administrators by clicking on the "Upload" button on the MyBRC portal ICA request page and selecting the signed MOU form to be uploaded from their computer.

Note that renewals of ICAs are not supported via the front end of the MyBRC portal at this time. To renew an existing ICA allocation, please contact us at <a href="mailto:brc@berkeley.edu">brc@berkeley.edu</a>.
