---
title: User Access Agreement Form
keywords:
tags: [hpc, accounts] 
---

# User Access Agreement Form

Every prospective user of the Savio high-performance computing cluster and other clusters managed by Berkeley Research Computing (BRC) must first complete and (electronically) sign a <strong>BRC User Access Agreement form</strong>, in order to obtain credentials to access the cluster. You can access the BRC User Access Agreement form by visiting the <a href="https://mybrc.brc.berkeley.edu/" target="_blank">MyBRC User Portal</a> in your web browser. After following the on-screen instructions in the portal and registering and/or logging in, you can review and submit the cluster user access agreement form on the Home ("Welcome") page by clicking on the "Review" button. Note that even existing BRC cluster users who already have BRC cluster accounts and have completed and submitted the BRC User Access Agreement form via a Google form previously must review and submit the User Access Agreement form within the MyBRC User Portal to be able to join or create projects.
