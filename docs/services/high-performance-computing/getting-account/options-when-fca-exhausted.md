---
title: Options When a Faculty Computing Allowance Is Exhausted
keywords: 
tags: [hpc]
---

# Options When a Faculty Computing Allowance is Exhausted 

!!! summary
    When you’ve used up your <a href="../../../../services/high-performance-computing/getting-account/faculty-computing-allowance">Faculty Computing Allowance</a> of computing time on the campus’s Savio high performance computing cluster, but still need more time on Savio (or an equivalent computing resource) to complete your project, here are some of the options that you might now consider:
    <ol>
        <li><a href="#purchase-savio" class="toc-filter-processed">Purchase additional computing time on Savio</a></li>
        <li><a href="#renew-fca" class="toc-filter-processed">Apply to renew your Faculty Computing Allowance, during the next application period</a></li>
        <li><a href="#contribute-condo" class="toc-filter-processed">Contribute Condo nodes to Savio</a></li>
        <li><a href="#obtain-hpc-at-uc" class="toc-filter-processed">Obtain an allocation on a UC-wide (HPC@UC) resource</a></li>
        <li><a href="#obtain-access" class="toc-filter-processed">Obtain an allocation on a national computing (ACCESS) resource</a></li>
        <li><a href="#purchase-commercial" class="toc-filter-processed">Purchase computing time from a commercial cloud provider</a></li>
    </ol>
    
    To explore any of these options in more depth, please <a href="../../../../services/high-performance-computing/getting-help">contact us</a>, and let's begin the conversation!

## <a name="purchase-savio" id="purchase-savio"></a><strong>1. Purchase additional computing time on Savio</strong>

<p>Via a Memorandum of Understanding (MOU) and a one-time payment using a campus chartstring, you can pay for one or more additional blocks of compute time at an introductory rate of one cent ($0.01) per <a href="../../../../services/high-performance-computing/user-guide/running-your-jobs/service-units-savio">Service Unit (SU)</a>. One SU equals one core hour of compute time on a standard compute node; <a href="../../../../services/high-performance-computing/user-guide/running-your-jobs/service-units-savio">it can vary for other types of nodes</a>.</p>
<p>For instance, if you originally received a Faculty Computing Allowance of 300K SUs, and wished to purchase another, equivalent block of time, you can do so for $3,000. There is a minimum purchase of 50K SUs, at a cost of $500.00.</p>
<p>This compute time does not expire; it will be carried over until it is used up, and can be used alongside any additional no-cost Allowance(s) granted in the future.</p>
<p>To make your purchase, visit the <a href="https://mybrc.brc.berkeley.edu" target="_blank">MyBRC User Portal</a> in your web browser. After following the on-screen instructions in the portal and registering (if you don’t already have a portal account or a BRC cluster account) and logging in, you’ll need to first review and sign the cluster user access agreement form on the Home ("Welcome") page if you haven't already done so from within the portal previously by clicking on the "Review" button, and then clicking on the "Create" button to request the creation of a project on the same Home ("Welcome") page, then follow all of the on-screen prompts and instructions and answer all required questions from there to request a new project (Slurm account) on the Savio cluster. Make sure to select the Recharge Allocation project allocation type on the “Savio: Allocation Type” page and provide responses to the following questions that appear in the different pages in the portal that you will navigate to:</p>
<ol><li>Number of Service Units requested? (E.g., 100K, 300K ..?)</li>
<li><span class="m_-6955682788859526793gmail-tabs2_section m_-6955682788859526793gmail-tabs2_section_1 m_-6955682788859526793gmail-tabs2_section1" id="m_-6955682788859526793gmail-section_tab.991f88d20a00064127420bc37824d385" style="display:block"><span class="m_-6955682788859526793gmail-section" id="m_-6955682788859526793gmail-section-991f88d20a00064127420bc37824d385"><span class="m_-6955682788859526793gmail-sn-widget-textblock-body m_-6955682788859526793gmail-sn-widget-textblock-body_formatted m_-6955682788859526793gmail-ng-binding">Campus chartstring to bill?</span></span></span></li>
<li><span class="m_-6955682788859526793gmail-tabs2_section m_-6955682788859526793gmail-tabs2_section_1 m_-6955682788859526793gmail-tabs2_section1" style="display:block"><span class="m_-6955682788859526793gmail-section"><span class="m_-6955682788859526793gmail-sn-widget-textblock-body m_-6955682788859526793gmail-sn-widget-textblock-body_formatted m_-6955682788859526793gmail-ng-binding">Type of account represented by chartstring? (E.g., is this a contract or grant? What is the funding agency or campus source?)</span></span></span></li>
<li>Name and email address of a departmental business contact, for correspondence about the chartstring?</li>
</ol>

PIs and Managers of existing Recharge Allocation accounts can also see a "Purchase Service Units" button on their MyBRC portal project page, by which they may purchase additional SUs. 

As of February 2024, the MOU will be generated automatically when the recharge allocation purchase request is submitted via the MyBRC portal. Once the recharge allocation purchase request (or recharge allocation purchase renewal request) is submitted, the details of the request will be reviewed, confirmed, and edited (if and as needed) by administrators. Once this process is completed, the researcher will be notified, and based on the (edited if needed) details of the researcher's recharge allocation purchase request, an unsigned MOU will be generated which the researcher can download to their computer from the MyBRC portal by clicking on the "Download" button on the MyBRC portal recharge allocation request page. The researcher can then review and sign the downloaded MOU and then upload the signed MOU form back up to the MyBRC portal for submission to and review by administrators by clicking on the "Upload" button on the MyBRC portal recharge allocation project request page and selecting the signed MOU form to be uploaded from their computer.

Keep in mind that if a researcher has exhausted the allocated service units on their FCA, they should not request the creation of a new FCA, but, rather, they should renew their already existing FCA or purchase additional computing time on Savio, which will be added into a recharge allocation (i.e., ac_projectname). Note that when you purchase additional SUs, they will be added to a (separate) recharge allocation and not into an existing FCA project. Additional SUs can only be added to FCA projects when such projects are renewed during the May/June renewal period (see below) or thereafter (via the FCA renewal process and not through the SU purchase process).

## <a name="renew-fca" id="renew-fca"></a><strong>2. Apply to renew your Faculty Computing Allowance, during the next application period</strong>

<p>Each year, beginning in May, you can submit an <a href="https://mybrc.brc.berkeley.edu/project/renew-pi-allocation-landing/" target="_blank">application form</a> to renew your Faculty Computing Allowance via the <a href="https://mybrc.brc.berkeley.edu" target="_blank">MyBRC User Portal</a>. Pooling preferences can be updated there as well. Links to the renewal application form are typically emailed to Allowance recipients (and to other designated project managers on such accounts) by or before mid-May.</p>
<p>Renewal applications submitted in May will be processed beginning June 1st. Those submitted and approved later in the year, after either May or June, will receive pro-rated allowances of computing time, based on the application month within the allowance year.</p>

To minimize job disruptions, all requests submitted and approved before June 1 will be processed on June 1st. You will receive a notification when each request is approved and processed. Renewal will provide you with 300,000 SUs (Service Units). Note that new allowances that are set up in June or old ones that are renewed in June are the only ones that receive the full 300,000 SUs allocation.

Both PIs and project managers can renew their allowance project.  For PIs or project managers wishing to renew their allowance under the same project:

- Login to the MyBRC User Portal.
- Under the "Project" menu, click on "Projects''.
- Click on the ID of the Project the PI is currently a PI of.
- Click on the "Renew Allowance" button.
- Fill out the renewal form.
    - Select the "Allowance Year 2024 - 2025" Allocation Period (for example), which will grant 300,000 Service Units, available starting on June 1st. After June 1st, the number of service units granted will be prorated based on how many months into the allowance year the request is made.

    - Select the PI to renew. (If there are multiple, they must be renewed individually).
    - Fill out the BRC Usage Survey
- Review your selections and submit the form.

For PIs or project managers wishing to update their pooling preferences:

- Login to the MyBRC User Portal.
- Under the "Project" menu, click on "Projects".
- Click on the "Renew a PI's Allowance" button.
- Fill out the renewal form.
    - Select the "Allowance Year 2024 - 2025" Allocation Period (for example), which will grant 300,000 Service Units, available starting on June 1st. After June 1st, the number of service units granted will be prorated based on how many months into the allowance year the request is made.
    - Select the PI to renew.
    - Choose a new pooling preference, which may include:
        - Renewing under the same project,
        - Starting to pool with a different project, or
        - Stopping pooling and renewing under a different or new project.
    - Review your selections and submit the form.

You will receive notifications when each request is approved and processed a day before and on June 1. 


## <a name="contribute-condo" id="contribute-condo"></a><strong>3. Contribute Condo nodes to Savio</strong>

<p>If you expect your computational needs to be large and/or ongoing, another possibility is to purchase compute nodes and contribute them to the Savio Condo.</p>
<p>Purchasing and contributing hardware gives you, as a Condo owner/contributor, the following benefits over the five year life of your contribution:</p>
<ul><li><a href="../../../../services/high-performance-computing/getting-help/frequently-asked-questions#q-what-are-the-benefits-of-participating-in-the-condo-cluster-program-">Priority access</a> to compute resources up to the size of your contribution.</li>
<li>The ability to <a href="http://research-it.berkeley.edu/blog/16/01/29/low-priority-queue-allows-condo-contributors-glean-unused-cycles-savio" target="_blank">run larger jobs across even more nodes</a>.</li>
<li>All the extensive infrastructure that Savio provides - including colocation in the campus data center, power, cooling, high-speed networking, a firewalled subnet, login and data transfer nodes, commercial compilers, and a large, fast parallel filesystem - along with professional system administration of this entire infrastructure.</li>
</ul><p>For more information on this option, please see <a href="../../../../services/high-performance-computing/condos/condo-cluster-service">Condo Cluster Program</a>. You can request the creation of a Condo allocation by visiting the <a href="https://mybrc.brc.berkeley.edu" target="_blank">MyBRC User Portal</a> in your web browser. After following the on-screen instructions in the portal and registering (if you don’t already have a portal account or a BRC cluster account) and logging in, you’ll need to first review and sign the cluster user access agreement form on the Home ("Welcome") page if you haven't already done so from within the portal previously by clicking on the "Review" button, and then clicking on the "Create" button to request the creation of a project on the same Home ("Welcome") page, then follow all of the on-screen prompts and instructions and answer all required questions from there to request a new project (Slurm account) on the Savio cluster. Make sure to select the "Condo allocation" project allocation type on the “Savio: Allocation Type” page and provide responses to the questions that appear in the different pages in the portal that you will navigate to.</p>

## <a name="obtain-hpc-at-uc" id="obtain-hpc-at-uc"></a><strong>4. Obtain an allocation on a UC-wide (<a href="https://www.sdsc.edu/hpc-at-uc.html" target="_blank">HPC@UC</a>) resource</strong>

<p>The <a href="https://www.sdsc.edu/hpc-at-uc.html" target="_blank">HPC@UC program</a> offers no-cost computing time on computational resources at the San Diego Supercomputer Center (SDSC) to University of California-affiliated researchers. In particular SDSC's Expanse supercomputer offers a software environment that is fairly similar to Savio's, reducing the effort required to replicate your current setup there.<br><br>(Please note that you can access SDSC's Expanse resource via either the HPC@UC program or ACCESS program (below) - but not both - so we encourage you to <a href="../../../../services/high-performance-computing/getting-help">contact us</a> for assistance before applying to the HPC@UC program.)</p>
<p>If a researcher is interested in applying for HPC@UC, they should send an email to consult@sdsc.edu with their ACCESS-CI portal username and the HPC@UC team will open up an opportunity for the researcher to submit a request. If a researcher does not already have an ACCESS-CI portal account, they can create one <a href="https://identity.access-ci.org/new-user-direct" target="_blank">here</a>. This will allow the HPC@UC team at SDSC to vet the researcher’s eligibility for this program and ultimately provide them with login credentials for Expanse if the request is approved.</p> 
<p>Researchers can contact the HPC@UC team directly at hpc_uc@sdsc.edu with additional questions about the HPC@UC program and the application process.</p>


## <a name="obtain-access" id="obtain-access"></a><strong>5. Obtain an allocation on a national computing (</strong><a href="https://access-ci.org/" target="_blank">ACCESS</a><strong>) resource</strong>

<p>The NSF-funded <a href="https://access-ci.org/" target="_blank">ACCESS program</a> (formerly XSEDE) offers no-cost computing time on computational centers across the country. Several of these offer clusters whose software environments are fairly similar to Savio's, reducing the effort required to replicate your current setup there.</p>
<p>ACCESS provides a variety of <a href="https://allocations.access-ci.org/project-types" target="_blank">project types</a>. Applications for the smaller project types (which would generally be a good starting point even if more resources are needed eventually) can be applied for at any time and are reviewed regularly.</p>
<p>Our consultants can work with you to help you find and apply for ACCESS computing resources that best fit your needs. We can also help you to get your software working on Savio, to demonstrate readiness to take advantage of ACCESS resources.</p>

## <a name="purchase-commercial" id="purchase-commercial"></a><strong>6. Purchase computing time from a commercial cloud provider</strong>

<p>Amazon, Microsoft, and Google are among a number of commercial vendors offering computing and storage services that you can obtain with relatively little lead time. Several of these services offer computing environments somewhat similar to Savio’s, along with others that use very different computing paradigms.</p>
<p>Berkeley Research Computing (BRC) offers a <a href="http://research-it.berkeley.edu/services/cloud-computing-support" target="_blank">Cloud Computing Support consulting service</a>, which can help you determine whether one or more commercial cloud providers’ offerings might be a good match for your computational work. BRC consultants will also help you apply for vendor grants, where available, that can help defray at least some of your costs.</p>
<p>You can choose - and mix and match - any combination of these options: an MOU for purchasing additional computing time on Savio, a new Faculty Computing Allowance, to be applied for during the next Allowance period; contributing Condo nodes; an allocation on an <a href="http://research-it.berkeley.edu/blog/17/01/23/hpcuc-provides-researchers-rapid-access-supercomputing" target="_blank">HPC@UC</a> or <a href="https://access-ci.org/" target="_blank">ACCESS</a> facility; and/or purchasing services from a commercial cloud provider.</p>
<p>If you're interested in exploring one or more of these options, please <a href="../../../../services/high-performance-computing/getting-help">let us know</a> and let's begin the conversation!</p>
