---
title: Getting an Account
keywords: 
tags: [hpc]
---

# Getting an Account 

!!! summary

    The <strong>Savio</strong> cluster is open to all UC Berkeley PIs who need access to high performance computing. Multi-institutional research collaborations involving at least one UC Berkeley PI are also welcome. PIs can then designate researchers (including those not affiliated with UC Berkeley) working on the project to be users on Savio. Instructors of record can get access to Savio for their course through an Instructional Computing Allowance. Savio users are associated with one or more projects under which they do their computations. Projects include Faculty Computing Allowances, Condos, Instructional Computing Allowances, and other specialized projects. 

Before one can use Savio or set up a Savio project, one must set up a MyBRC portal account to manage your access to Savio projects, as discussed below under <a href="#setting-up-a-mybrc-user-portal-account">Setting up a MyBRC User Portal account</a>. 

Each individual user must be associated with at least one project. If a project exists, you can <a href="#getting-access-to-a-project">request access to the project</a>. If you don't yet have a user account, one will be created for you when your access is granted. If you already have an account, your request will be associated with your existing account. Please be aware of the potential delay if the project you join is new (i.e., in a pending state and waiting to be approved).

### Setting up a MyBRC User Portal account

In order to use Savio, including setting up Savio projects, one must have a MyBRC User Portal account. To create one, simply visit the <a href="https://mybrc.brc.berkeley.edu/" target="_blank">MyBRC User Portal</a> in your web browser and follow the on-screen instructions to register. 

Users authenticate to MyBRC using single-sign on, provided by CILogon*. Berkeley researchers will be redirected to authenticate using CalNet, while external collaborators can select and authenticate with their home institution's login flow.

Once authenticated, users need to review and sign the cluster <a href="../../../services/high-performance-computing/getting-account/user-access-agreement-form">User Access Agreement Form</a> on the Home ("Welcome") page by clicking on the "Review" button.

*Below are some specific details about authentication:

- Authentication to the MyBRC User Portal was previously password-based; these passwords are no longer in use.
- A user's MyBRC account is associated with one institution. Users must log in to MyBRC using the same institution to connect to their existing account.
    - Existing MyBRC users who are unsure which institution is associated with their existing account should contact us.
- A small percentage of users may not have their institutions listed in CILogon.
    - New MyBRC users whose institutions are not listed should select the Google provider.
    - Existing MyBRC users whose institutions are not listed can authenticate by requesting a short-lived login link from the "Hints" section of the MyBRC login page.
    
### Setting Up a Savio Project for Research or a Course

Please note that we have now replaced the Google forms that were previously used for BRC project requests with the <a href="https://mybrc.brc.berkeley.edu/" target="_blank">MyBRC User Portal</a>.

<p>There are three primary ways for a PI or course instructor to set up a Savio project that allows the PI and their collaborators or students to obtain access to Savio for research or course:</p>
<ol><li>Requesting a block of no-cost computing time via a <b>Faculty Computing Allowance (FCA)</b>. This option is currently offered to UC Berkeley faculty (including assistant,
associate, adjunct, and full professors), as well as researchers in the Professional Research Series (with proof of job
classification), who can then share their Allowance amongst their postdocs, grad students, staff researchers, and other campus research affiliates. Note that graduate students and postdocs are not eligible to be the PIs or co-PIs of FCAs. For details, please see <a href="../../../services/high-performance-computing/getting-account/faculty-computing-allowance">Faculty Computing Allowance</a>. To request an FCA, please visit the <a href="https://mybrc.brc.berkeley.edu/" target="_blank">MyBRC User Portal</a> in your web browser. After following the on-screen instructions in the portal and <a href="#setting-up-a-mybrc-user-portal-account">registering</a> and/or logging in to the portal, click on the "Create" button to request the creation of an FCA project on the Home ("Welcome") page. The PI may also ask another person to set up an FCA project on their behalf. </li>
<li>Purchasing and contributing <b>Condo</b> nodes to the cluster. This option is open to any campus PI, and provides ongoing, priority access to you and your research affiliates who are members of the Condo. For details, please see <a href="../../../services/high-performance-computing/condos/condo-cluster-service">Condo Cluster Service</a>.</li>
<li>Requesting an <b>Instructional Computing Allowance (ICA)</b>, which provides a block of no-cost computing time for an instructor and students in a course. For details, including the use of the <a href="https://mybrc.brc.berkeley.edu/" target="_blank">MyBRC User Portal</a> to request and manage a <b>new</b> ICA, please see <a href="../../../services/high-performance-computing/getting-account/instructional-computing-allowance">Instructional Computing Allowance</a>. To request the <b>renewal of an existing</b> ICA, please fill out the <a href="https://docs.google.com/forms/d/e/1FAIpQLSeNO3sw3YSPYarL70Bd74kz6Wl3rwyXFFtUVU08zryfMSJh_A/viewform" target="_blank">ICA request form</a>.</li></ol>
<p>If you'd like to further discuss any&nbsp;of these options in more detail, or if you have any other questions, you're invited to contact us at <a href="mailto:brc-hpc-help@berkeley.edu">brc-hpc-help@berkeley.edu</a>.</p>
<p>This training video also provides an overview of your options for accessing Savio:</p>
<iframe width="560" height="315" src="https://www.youtube.com/embed/Denj8NyUPVo?start=134&end=209" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<p><a name="Getting-User-Accounts" id="Getting-User-Accounts"></a></p>

### Getting a User Account

In order to have a Savio account, you must <a href="#getting-access-to-a-project">get access</a> to at least one project. Once you are given access to a project, if you do not yet have a user account, one will be created for you. A user account will be created (if needed) and project access given automatically to the user who fills out the new project request form. If that user is not the PI, the PI may later request cluster access from the project page on the MyBRC portal.

Once a new BRC user account has been set up, the new user will receive an email shortly thereafter confirming this and pointing them to instructions on how they can access their new BRC user account, also described <a href="#accessing-user-accounts">below</a>.

### Getting Access to a Project

Please note that we have now replaced the Google forms that were previously used for BRC project access requests with the <a href="https://mybrc.brc.berkeley.edu/" target="_blank">MyBRC User Portal</a>. 

<p>Researchers who want to request access to a project (for example, an FCA, ICA, Partner Computing Allowance, Recharge Allocation project, or Condo project) can do so by visiting the <a href="https://mybrc.brc.berkeley.edu/" target="_blank">MyBRC User Portal</a> in their web browser. After following the on-screen instructions in the portal and <a href="#setting-up-a-mybrc-user-portal-account">registering</a> and/or logging in, click on the "Join" button to request to join an existing project. (Note, however, that if an FCA project has expired, a researcher will not be able to submit a request to join the project. An expired FCA project needs to be 
<a href="../../../services/high-performance-computing/getting-account/faculty-computing-allowance#renewing-your-faculty-computing-allowance">renewed</a> by the PI or project manager before a researcher can submit a request to join it). All project access and new account creation requests require the explicit approval of the PI or project manager for the relevant project on the cluster (i.e., they will not be automatically approved) via the MyBRC User Portal by clicking on the "Review Join Requests" button on the project page, and users should check with the project PI before requesting access to a project. Automatic email reminders will be sent to users and project managers after project access and new account creation requests are received.</p>

<p>Savio users can add themselves to a project (i.e., request to join a project) by logging into the MyBRC User Portal and clicking on the "Join" button on the MyBRC home page next to "2. Create or join a project". Users will then reach the "Join a Project" page, which includes a list of all Savio projects. The user can then click the "Join" button next to the projects on the list that they are requesting to join. A PI or project manager with a MyBRC User Portal account can similarly add a user (who must first have a MyBRC portal account) to their project(s) by selecting “Project --> Projects” in the top menu bar of the MyBRC User Portal after logging in and navigating to the “BRC Cluster Projects” page. Once there, the PI or project manager can select the ID number of the project they would like to add the user to, and then click the “Add Users” button under "Manage Project" or "Users" within the corresponding project page that opens up. The PI or manager can then search for the user(s) to add using the search box in the resulting "Add users to project:..." page that comes up, check the box to the left of the user's username, and then click the "Add Selected Users to Project" button at the bottom of the page.</p>

<p>If a researcher does not already have a BRC user account, one will be created for them when they are given access to the project.</p>

<p>A PI can assign an existing Savio user under a project to be a project manager by navigating to the project page for the project within the MyBRC portal (<a href="https://mybrc.brc.berkeley.edu/project/" target="_blank">https://mybrc.brc.berkeley.edu/project/</a>), clicking on the icon in the "Actions" column of the "Users" table on the project page, updating the "Role" to be "Manager", and then clicking "Update". Similarly, a manager can be removed from a project by changing their role from "Manager" to "User" and then clicking "Update".</p>
 
### Leaving a Project

<p>Savio users with a MyBRC User Portal account can leave a project by logging into the MyBRC User Portal and selecting “Project --> Projects” in the top menu bar of the MyBRC User Portal and navigating to the “My BRC Cluster Projects” page. Once there, the user can select the ID number of the project they would like to be removed from, and then click the "Leave Project" button under "Users" within the corresponding project page that opens up. A PI or project manager with a MyBRC User Portal account can similarly remove a user (who must first have a MyBRC portal account) from their project(s) by selecting “Project --> Projects” in the top menu bar of the MyBRC User Portal after logging in and navigating to the “BRC Cluster Projects” page. Once there, the PI or project manager can select the ID number of the project they would like to remove the user from, and then click the "Remove Users" button under "Users" within the corresponding project page that opens up. The PI or manager can then select the user to remove by clicking on the "Remove" button next to the name of the user listed in the resulting "Remove users from project:..." page that comes up.</p>

### Accessing User Accounts

<p>Please see the <a href="../../../services/high-performance-computing/user-guide/logging-brc-clusters">Logging In</a> page for details regarding access to BRC user accounts. For security reasons, all access is via One-Time Passwords (OTPs), to reduce the risk of a compromise from stolen user credentials.</p>

<p>New users should note that there will be a processing wait time of a few days or so between the time they join their first project via the MyBRC User Portal and the time they receive the confirmation email pointing them to instructions on how they can access their new BRC user account. Users should not attempt to follow any of these instructions (e.g., linking their personal account to their BRC HPC Cluster account, installing and setting up Google Authenticator, setting up their one-time password, etc., and logging into the BRC Cluster) before they have received the confirmation email. Users can also determine the status of their requests to join projects (FCAs, Condo projects, Vector projects, etc.) and their project access on the cluster via the MyBRC User Portal as well.</p>

### Closing User Accounts

<p>The PI for the project is ultimately responsible for notifying Berkeley Research Computing when a user's access to a project should be terminated. If a user does not have access to any other projects, their user account will be closed. In that case, the PI can specify the  disposition of a user's software, files, and data.</p>
<p>For ICAs, all student accounts are closed and files are deleted at the end of the semester when the ICA expires, unless the student accounts are also associated with an FCA or condo.</p>
<p>For FCAs and condos, users’ accounts are not automatically deactivated upon termination of an employee because many people change their employment status, but remain engaged with a research project. In addition, in some cases users share software and data from their home directory, and other users may depend on these files. </p>

