---
title: Faculty Computing Allowance
keywords: 
tags: [hpc]
---

# Faculty Computing Allowance

Download the&nbsp;<a href="../../../../pdf/FCA_Bundle.pdf" target="_blank">Faculty Computing Allowance info packet</a>.

!!! summary
	The Faculty Computing Allowance (FCA) provides up to 300K&nbsp;<a href="../../../../services/high-performance-computing/user-guide/running-your-jobs/service-units-savio">Service Units (SUs)</a>&nbsp;of free compute time per academic year to all qualified UC Berkeley faculty/PIs, where&nbsp;<a href="../../../../services/high-performance-computing/user-guide/running-your-jobs/service-units-savio">one SU is equivalent to one compute cycle on the latest standard hardware</a>. (Allowances are prorated based on month of application during the allowance year, which starts on June 1 and ends on May 31 the following year.) PIs can also pool their allowances if their groups are working together; however, this must be specified when the allowance is initially setup or during the annual renewal period. Please note that allowances can be shared but are not transferable. See our <a href="../../../../services/high-performance-computing/overview/facilities-statement">facilities statement</a>, suitable for inclusion in grant proposals, for a summary of the services provided by Berkeley Research Computing. People needing more compute time beyond their allowance should review the&nbsp;<a href="../../../../services/high-performance-computing/getting-account/options-when-fca-exhausted">options for getting more computing time for a project</a>. Savio&nbsp;cluster project names for these allocations start with a '<code>fc_</code>’ prefix; for example&nbsp;<code>fc_projectname</code>. 


### Setting Up an FCA

To set up an FCA, please follow [these instructions](../../../../services/high-performance-computing/getting-account#setting-up-a-savio-project-for-research-or-a-course) to create a new Savio project for your FCA via the [MyBRC User Portal](https://mybrc.brc.berkeley.edu).

As a service provided at no cost to researchers, an FCA does not guarantee access to the full amount of SUs. SUs spent on jobs killed due to unplanned outages cannot be reimbursed.

### Eligibility

FCAs are available to all qualified UC Berkeley PIs, including

   - Assistant, associate, and full professors
   - Emeriti (retired) professors who still have active PI status (i.e., Professors of the Graduate School)
   - Adjunct professors
   - Researchers in the Professional Research Series (with proof of job classification; please email brc-hpc-help@berkeley.edu)

Please also note that if a researcher is already the PI of an existing FCA project, they can not request the creation of a new FCA account. So, for example, if a researcher has exhausted the allocated service units on their FCA, they should not request the creation of a new FCA, but, rather, they should renew their already existing FCA, purchase additional computing time on Savio, or pursue other <a href="../../../../services/high-performance-computing/getting-account/options-when-fca-exhausted">options for getting more computing time for a project</a>.

### Adding Additional Users to Your Project 

Researchers who want to request access to a project or PIs or project managers who want to add users to a project can do so by visiting the <a href="https://mybrc.brc.berkeley.edu/" target="_blank">MyBRC User Portal</a> in their web browser. Further instructions can be found [here](../../../../services/high-performance-computing/getting-account#getting-access-to-a-project).

### Removing Users from Your Project

Researchers who want to remove their access to a project or PIs or project managers who want to remove users from a project can do so by visiting the <a href="https://mybrc.brc.berkeley.edu/" target="_blank">MyBRC User Portal</a> in their web browser. Further instructions can be found [here](../../../../services/high-performance-computing/getting-account#getting-access-to-a-project).

### Renewing Your Faculty Computing Allowance

<p>Each year, beginning in May, you can submit an <a href="https://mybrc.brc.berkeley.edu/project/renew-pi-allocation-landing/" target="_blank">application form</a> to renew your Faculty Computing Allowance via the MyBRC Portal. Keep in mind that allowance renewals are done per PI and not per project, meaning that the co-PIs of a pooled FCA must submit their FCA renewal requests separately (or have someone on their behalf submit the FCA renewal request separately for each co-PI).</p>
<p>Links to this renewal <a href="https://mybrc.brc.berkeley.edu/project/renew-pi-allocation-landing/" target="_blank">application form</a> are typically emailed to Allowance recipients (and to other designated "main contacts" on such accounts) by or before mid-May each year.</p>

!!! note
    Renewal applications submitted during May will be processed beginning June 1st. Those submitted and approved later in the year, after the May/June period (i.e. after June 30th), will receive pro-rated allowances of computing time, based on month of application during the allowance year. Note that new allowances that are set up in June or old ones that are renewed in June are the only ones that get the full 300,000 SUs allocation.

### Viewing the Usage of Your Allowance

<p>You can <a href="../../../../services/high-performance-computing/user-guide/running-your-jobs/service-units-savio#viewing-your-service-units">view how many Service Units have been used</a> to date under a Faculty Computing Allowance as well as an MOU allocation, or by a particular user account on Savio.</p>

<p>Once you've gone over your Service Unit limit for the year, users of the FCA will no longer be able to run new jobs. Any jobs that started before the limit was reached should finish as usual, even if the usage exceeds the limit.</p>

### Charges for Running Jobs

<p>Each time a computational job is run on the cluster under a&nbsp;Faculty Computing Allowance account, charges are incurred against that account, using abstract measurement units called <a href="../../../../services/high-performance-computing/user-guide/running-your-jobs/service-units-savio">Service Units (SUs)</a>. Please see&nbsp;<a href="../../../../services/high-performance-computing/user-guide/running-your-jobs/service-units-savio">Service Units on Savio</a> to learn more about how these charges are calculated.</p>

### Getting More Compute Time When Your Allowance Is Used Up

<p>Have you exhausted your entire Faculty Computing Allowance? There are a number of <a href="../../../../services/high-performance-computing/getting-account/options-when-fca-exhausted">options for getting more computing time for your project</a>.</p>

### Acknowledgements

<p>Please acknowledge Savio in your publications. (Such acknowledgements are an important factor in helping to justify ongoing funding for, and expansion of, the cluster.) A sample statement is:</p>
<p><em>"This research used the Savio computational cluster resource provided by the Berkeley Research Computing program at the University of California, Berkeley (supported by the UC Berkeley Chancellor, Vice Chancellor for Research, and Chief Information Officer)."</em></p>
<p>As well, we encourage you to <a href="https://docs.google.com/forms/d/e/1FAIpQLSdu-HaywUKiIqfZtQ8vLzc6sb3ZeVh-qgpbvXBXvngVPatsZA/viewform" target="_blank">tell us how BRC impacts your research</a>&nbsp;(Google Form), at any time!</p>
