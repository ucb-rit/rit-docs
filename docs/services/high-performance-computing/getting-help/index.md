---
title: Getting Help
keywords: high performance computing
tags: [hpc, help, consulting]
---

!!! summary
    We can help you with various questions ranging from nitty-gritty Savio problems to providing advice about setting up computing infrastructure in the cloud. Please see below for information on how to contact us. Here are some details on the [kinds of questions we can (and cannot) help with](../../../services/high-performance-computing/getting-help/our-services). 

## Savio Ticketing System

To get help on any topic - such as to ask questions about the BRC clusters (Savio and Vector), report a problem, or provide suggestions - please email us at <a href="mailto:brc-hpc-help@berkeley.edu">brc-hpc-help@berkeley.edu</a>. Doing so will create an issue ticket, and you'll receive email responses whenever the ticket's status changes. We have [some tips on how to file an informative request](../../../services/high-performance-computing/getting-help/good-help-requests) so we can best help you most effectively and quickly.

Support is provided during regular business hours (Monday through Friday, from 8:00 am to 5:00 pm Pacific Time). Support during off-hours, weekends, and holidays is Best-Effort, based on availability of staff and criticality of the problem.

Alternatively, you can attend our virtual office hours, discussed below, to ask Savio-related questions for which in-person interaction would be useful.

We encourage Savio users to familiarize themselves, consult, and refer to our detailed documentation (including [our FAQs page](../../../services/high-performance-computing/getting-help/frequently-asked-questions)) before getting in touch with us. Our documentation has been developed with user questions in mind. Moreover, much of the information in our documentation has been derived and developed from an extensive archive of questions from users that we have collected over the last several  years. Therefore, a lot of the questions that come up as you work on Savio are answered in the documentation.

We also encourage you to ask questions of and seek assistance from members of your own research labs, groups, and among your colleagues at UC Berkeley who are also Savio users when you have questions and when issues come up. For example, more experienced  users can often assist lab colleagues who are newly onboarded users, and this is regular practice among our users. Furthermore, you are encouraged to familiarize yourself with, communicate, and work with other Savio users in your labs, research groups/units, home departments, and/or elsewhere on campus and among your collaborators and colleagues (as well as mentors, supervisors, lab managers, and local IT staff) in troubleshooting and debugging issues that come up, as some of you are collaborating on the same projects and using the same software and software libraries, using the same or similar scripts and code, sharing the same data, etc.


## Research Computing and Research Data Consulting

Research IT offers consulting for UC Berkeley researchers. From identifying the best computing solution to working with sensitive data, Research IT consultants are poised at the intersection of research and technology and can help. We are here to help understand your needs, match you to appropriate resources, and help you get started using them. Our diverse team is made up of experts in both data and computing from a wide variety of domains. 

For consultation, please email <a href="mailto:research-it-consulting@berkeley.edu">research-it-consulting@berkeley.edu</a>.

## Virtual Office Hours

Research IT offers virtual office hours for in-person support for both Savio and more general consulting questions. For times and location, please see <a href="https://research-it.berkeley.edu/consulting" target="_blank">here</a>.

No appointment is necessary. 

## Status and Announcements

Please also see the [Research IT home page](http://research-it.berkeley.edu/services/high-performance-computing/status-and-announcements) and the [Research IT Status and Service Updates page](https://research-it.berkeley.edu/status-and-service-updates) for listings of scheduled downtimes for the cluster, general system status announcements, and a graphical display of live utilization and status information for many of the cluster's compute nodes. We suggest that all Savio users check the Research IT home page on a regular basis. In certain circumstances, for example, when there is a power outage at the Earl Warren Hall Data Center, it may also be helpful to check the [UC Berkeley System Status webpage](https://systemstatus.berkeley.edu/) as well.


