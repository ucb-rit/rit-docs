---
title: What Kinds of Questions Can We Help With?
keywords: high performance computing
tags: [hpc, help, consulting]
---

Research IT consultants will:  

   - Assist in debugging SLURM job scripts on Savio or other environments 
   - Consult about storage options and configure software and scripts for the ongoing transfer of data as resources allow, but the researcher is typically responsible for executing the data transfers
   - Provide assistance in software installation and configuration
   - Identify resources for doing computation and storing data, whether on-campus or cloud-based resources
   - Identify the best computing solution to working with sensitive data
   - Connect researchers to IST staff and aid decision-making by helping researchers understand the relevant technical aspects around the configuration of IST-hosted services

Research IT consultants do not (typically) offer the following types of assistance:

   - Debugging users’ analytical/processing code
   - Optimizing code
   - Data cleaning and “munging” 
   - Ongoing transfer of data
   - Configuring IST-hosted services




