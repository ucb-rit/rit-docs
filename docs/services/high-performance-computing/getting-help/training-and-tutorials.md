---
title: Training and Tutorials
keywords: high performance computing
tags: [hpc] 
---

# Trainings and Tutorials

!!! summary 
    This page provides links to training and tutorial materials pertaining to the BRC&nbsp;high performance computing clusters (Savio and Vector) at the University of California, Berkeley.

## Shell (terminal) tutorials

Having some familiarity with the (bash) shell (also known as the terminal or the commandline) is an important skill for effectively
using Savio (and other HPC systems/Linux clusters). (Although of course our OOD service provides a browser-based option
for interactive work.)

Here are some tutorials that may be useful if you are new to the shell or want a refresher.

 - [Software Carpentry shell introduction](https://swcarpentry.github.io/shell-novice/)
 - [Introduction to UNIX and the shell tutorial (from the Berkeley Statistical Computing Facility)](https://computing.stat.berkeley.edu/tutorial-unix-basics/)
 - [bash shell tutorial (from the Berkeley Statistical Computing Facility)](https://computing.stat.berkeley.edu/tutorial-using-bash/)
 - [Chapters on the bash shell (from the book Research Software Engineering with Python)](https://third-bit.com/py-rse/bash-basics.html)

## Savio - Introductory Training

Training materials from an introductory-level Savio training session in November 2024. These materials provide an overview of the cluster and its basic usage, covering topics such as logging in, accessing software, running jobs, and transferring files.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<ul><li><a href="https://htmlpreview.github.io/?https://github.com/ucb-rit/savio-training-intro-fall-2024/blob/main/intro.html" target="_blank">Outline and text of training</a></li>
<li><a href="https://github.com/ucb-rit/savio-training-intro-fall-2024" target="_blank">Source code files and other materials</a></li>
<li><a href="https://github.com/ucb-rit/savio-training-intro-fall-2024/archive/master.zip">ZIP archive for downloading all files used in this training</a></li>
<li><a href="https://www.youtube.com/watch?v=PkR2jfGTLOI" target="_blank">Recording on YouTube</li>
</ul></li>

## Savio - Slurm and Conda/Mamba tips and tricks

An intermediate training session held in April 2024 on making the most of the Slurm scheduler and of Mamba/Conda environments on Savio. 

<ul><li><a href="https://ucb-rit.github.io/savio-training-slurm-conda-spring-2024" target="_blank">Outline and slides from the training</a></li>
<li><a href="https://github.com/ucb-rit/savio-training-slurm-conda-spring-2024" target="_blank">Source code files and other materials on GitHub</a></li>
<li><a href="https://github.com/ucb-rit/savio-training-slurm-conda-spring-2024/archive/refs/heads/main.zip">GitHub ZIP archive for downloading all files used in this training</a></li>
<li><a href="https://www.youtube.com/watch?v=ig7_NSQGScs" target="_blank">Recording on YouTube</a></li>
</ul></li>

## Savio - Python and Jupyter Notebooks

An intermediate training session on using Python and Jupyter Notebooks on Savio. Content includes an overview and best practices of Python environment and package management using pip and conda, using and debugging Jupyter Notebooks on Savio via Open On Demand, creating custom Jupyter Notebook kernels, and basic parallelization strategies in Python (ipyparallel, Dask, and Ray), with examples on the command line and using Jupyter Notebooks.

<ul><li><a href="https://ucb-rit.github.io/savio-training-python-jupyter-fall-2021/slides.html" target="_blank">Outline and text of training</a></li>
<li><a href="https://github.com/ucb-rit/savio-training-python-jupyter-fall-2021" target="_blank">Source code files and other materials</a></li>
<li><a href="https://github.com/ucb-rit/savio-training-python-jupyter-fall-2021/archive/refs/heads/main.zip">ZIP archive for downloading all files used in this training</a></li>
<li><a href="https://www.youtube.com/watch?v=hZfrI3JdeRY" target="_blank">Recording on YouTube</a></li>
</ul></li>


## Savio - Introduction to Containers on Savio: Creating Reproducible, Scalable, and Portable Workflows

Introduction to the key concepts and tools for using containers, in particular Docker and Singularity containers, held on April 21, 2021. Containers make it easy to install software, move your computation between different computing environments, and make your workflow reproducible. The training focuses on the use of Singularity on Savio, including how to use Docker images by Singularity in a cluster environment. 
<ul><li><a href="https://github.com/ucb-rit/savio-training-containers-2021/blob/main/containers.md" target="_blank">Outline and text of training</a></li>
<li><a href="https://github.com/ucb-rit/savio-training-containers-2021" target="_blank">Source code files and other materials</a></li>
<li><a href="https://github.com/ucb-rit/savio-training-containers-2021/archive/main.zip">ZIP archive for downloading all files used in this training</a></li>
<li><a href="https://www.youtube.com/watch?v=iQF8nIzUPZk" target="_blank">Recording on YouTube</a></li>
</ul></li>

## Savio - Introduction to Job Submission, Scheduling, and Queueing on Savio

Discussion of how the Slurm job scheduler works in general and how it is configured on Savio, held on November 9, 2020. Content includes understanding how jobs are prioritized, why your job isn't running yet (or at all), and when your job will run. We also discuss how to submit various kinds of jobs, including parallel jobs. 
<ul><li><a href="https://github.com/ucb-rit/savio-training-slurm-2020/blob/master/slurm.md" target="_blank">Outline and text of training</a></li>
<li><a href="https://github.com/ucb-rit/savio-training-slurm-2020" target="_blank">Source code files and other materials</a></li>
<li><a href="https://github.com/ucb-rit/savio-training-slurm-2020/archive/master.zip">ZIP archive for downloading all files used in this training</a></li>
<li><a href="https://www.youtube.com/watch?v=a_3M3NXI2BM&t=4245s" target="_blank">Recording on YouTube</a></li>
</ul></li>

## Savio - Running Jobs in Parallel

Introduction to the key concepts and tools for parallelization, held on April 21, 2020. Content includes how to submit jobs to run in parallel on Savio, the campus high performance computing cluster, and how to monitor those jobs to check if they are doing what you expect, with examples, as well as an overview of tools in Python, R and MATLAB that allow you to run code in parallel.
<ul><li><a href="https://github.com/ucb-rit/savio-training-parallel-2020/blob/master/parallel.md" target="_blank">Outline and text of training</a></li>
<li><a href="https://github.com/ucb-rit/savio-training-parallel-2020" target="_blank">Source code files and other materials</a></li>
<li><a href="https://github.com/ucb-rit/savio-training-parallel-2020/archive/master.zip">ZIP archive for downloading all files used in this training</a></li>
<li><a href="https://www.youtube.com/watch?v=7CMKErkbFvg&ab_channel=ResearchITatUCBerkeley" target="_blank">Recording on YouTube</a></li>
</ul></li>

## Savio - Software Installation Training

Training materials from an intermediate-level Savio&nbsp;training session on October&nbsp;18, 2018. These materials provide an overview of installing software on Savio.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<ul><li><a href="https://github.com/ucb-rit/savio-training-install-2018/blob/master/install.md" target="_blank">Outline and text of training</a></li>
<li><a href="https://github.com/ucb-rit/savio-training-install-2018" target="_blank">Source code files and other materials</a></li>
<li><a href="https://github.com/ucb-rit/savio-training-install-2018/archive/master.zip">ZIP archive for downloading all files used in this training</a></li>
</ul></li>

## Savio - Intermediate / Parallelization Training

Training materials from an intermediate-level Savio training session held on September 27, 2016. These materials start with an overview of how to install your own software packages, and then focus extensively on parallelized use of the cluster.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<ul><li><a href="https://github.com/ucberkeley/savio-training-parallel-2016/blob/master/parallel.md" target="_blank">Outline and text of training</a></li>
<li><a href="https://github.com/ucberkeley/savio-training-parallel-2016" target="_blank">Source code files and other materials</a></li>
<li><a href="https://github.com/ucberkeley/savio-training-parallel-2016/archive/master.zip">ZIP archive for downloading all files used in this training</a></li>
</ul>

!!! note
    Additional trainings and tutorials are in planning or development. If you have any interest in working on or testing one of these, or have suggestions for what you'd like to see covered by these, please contact us via our <a href="../../../../services/high-performance-computing/getting-help">Getting Help</a> email address!
