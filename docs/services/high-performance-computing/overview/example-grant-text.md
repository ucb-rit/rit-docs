---
title: Example Grant Text
keywords: high performance computing, berkeley research computing
tags: [hpc]
---

# Example Text from a Grant Proposal

This text asking for support for a condo cluster in Savio is taken from a $2 million NSF grant proposal for a Computational Research Center on campus.

!!! example
    For faculty researchers and research groups the benefits of participating in this campus condo model are: the campus will cover the cost of on-going system administration; the cluster is housed in a state-of-art data center designed to provide the power and cooling needs of computationally intensive clusters; researchers will be provided compute resources based on their cluster contribution; researchers will also have access to burst capacity beyond their compute contribution; researchers do not have to hire and manage their own system administrators; and researchers will have access to consulting and design expertise related to issues of data security and privacy. We are also enthusiastic about the possibility of a partnership between the [Department, or Center for XYZ] and the office of the CIO in providing training for users of this cluster.

    We expect that the compute cluster in this proposal will become part of the campus’ institutional condo cluster. As such we will be able to provide the following campus support yearly for this grant totaling approximately $169,647:

    - System administration costs will be funded by the campus (15% FTE, $27K/year)
    - Professional HPC-IT staff will provide consulting and management services (15% FTE, $21K/year, plus 5% FTE $10.5K/year)
    - The cluster will be housed in the campus data center (collocation $36.5K/year, power and cooling $74K/year, network connections $400/year)

