---
title: Facilities Statement for Grants
keywords: high performance computing, berkeley research computing
tags: [hpc]
---

# Facilities Statement for Grants

The text below provides a summary of the Savio cluster and condo computing model, as well as details on the support provided to researchers. Our goal is that this text be suitable for inclusion in grant proposals for grants that propose to purchase condo resources as well as those that simply use the resources that BRC provides free of charge to all campus PIs.

Please contact us by emailing <a href="mailto:brc@berkeley.edu">brc@berkeley.edu</a> if you need more details or have questions related to grant proposals.

!!! example
    UC Berkeley operates a Linux high-performance computing cluster, called Savio, available to researchers on campus. The system consists of over 600 compute nodes in various configurations to support a wide diversity of research applications, with a total of over 15,300 cores and a peak performance of 540 teraFLOPS (CPU) and 1 petaFLOPS (GPU). Savio offers approximately 3.0 Petabytes of high-speed parallel storage. The cluster can accommodate and support the contribution of local faculty clusters using a “condo” model. Under this model, researchers can contribute compute nodes to the campus-run and campus-subsidized cluster. The researchers then have guaranteed access to the compute resources based on their contribution, as well as burst capacity beyond their contribution, while system administration is provided at no cost. The institutional cluster and its condo nodes are housed in the campus data center and operated by a team of high-performance computing specialists in a partnership between the campus Berkeley Research Computing (BRC) program and the Lawrence Berkeley National Laboratory (LBNL). The University of California also manages LBNL, and its location adjacent to the UC Berkeley campus helps to facilitate such collaborations. The LBNL High Performance Computing Services group has been in operation for over 15 years and has proven its ability to deliver and manage high-performance computational and storage solutions for researchers. BRC operates a consulting service that provides free consulting on topics related to research computing and research data management to campus researchers. This service is free and provides support for work on Savio as well as commercial cloud computing and NSF's ACCESS resources.


