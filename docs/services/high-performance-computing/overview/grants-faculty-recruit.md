---
title: Information for Writing Grants and Faculty Recruitment/Retention
keywords: 
tags: [hpc]
---

!!! summary
    Research IT provides various services to support faculty research. In addition to direct no-cost support, Research IT also provides infrastructure that faculty-purchased resources can leverage. Here we provide information and sample text useful for faculty writing grants and department chairs/deans working on faculty recruitment and retention.

## Facilities Statement for Grants

Please see this <a href="../../../../services/high-performance-computing/overview/facilities-statement">facilities statement</a>, suitable for inclusion in grant proposals, for a summary of the Savio condo program and more generally the services provided by Berkeley Research Computing.</p>

For an example from a $2 million NSF grant proposal for a Computational Research Center on campus, please see this <a href="../../../../services/high-performance-computing/overview/example-grant-text">example text</a>.

## Information for Faculty Recruitment and Retention

### Start-up - Savio Faculty Computing Allowance for Berkeley Faculty

This annual Faculty Computing Allowance (FCA) can be written into start-up and retention letters and can be used as institutional cost-share on grant proposals.

Here is some language suitable for a start-up package description:

!!! example
    The Berkeley campus will provide you an allowance of up to 300,000 Service Units (scalable core-hours) of computation per academic year on UC Berkeley’s high performance computing cluster, called "Savio". Berkeley Research Computing consulting staff will be available at no charge to help you learn about the Berkeley Research Computing facilities and optimize your research software for this environment

### Start-up / Retention - Condo Cluster and Storage Support

In recruitment and retention, deans or chairs can commit the funds needed for the faculty member to buy into the condo. Start-up or retention letters may include commitments to cover the one-time cost of purchasing compute servers or storage. Letters may also note the campus coverage of recurring costs. The campus' support for the condo can also be used as institutional cost-share on grant proposals; federal funding agencies have told us this support is important. Research IT staff will help detail the costs and value of these services.

Here is some language suitable for a start-up/retention package description:

!!! example
    We [referring to the Dean, Chair, or contributing party] will provide you with a $70,000 [or whatever figure is chosen] allocation so that you can purchase up to 8 high performance computing nodes (each of which has 56 cores and 256 GB RAM), as well as 100TB of storage, for your research. This provides approximately 3.9 million core-hours of computation per year to your research team (20 million core-hours over the 5 year life of the nodes). In addition, the campus will cover the one-time costs of associated InfiniBand cables, along with the yearly costs of system administration, high-speed network access (Science DMZ at 100 gbs), parallel storage, and consulting. Berkeley Research Computing (BRC) consulting staff will be available at no charge to help you learn about the BRC facilities and optimize your research workflows for this environment. The approximate annual value of this institutional support is $4,000 ($20,000 over the 5 year life of the nodes).
