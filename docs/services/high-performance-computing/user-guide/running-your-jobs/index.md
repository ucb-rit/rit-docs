---
title: Running Your Jobs
keywords: high performance computing, running jobs
tags: [hpc]
---

<h3 id="Overview">Overview</h3>

To submit and run jobs, cancel jobs, and check the status of jobs on the Savio cluster, you'll use the Simple Linux Utility for Resource Management (SLURM), an open-source resource manager and job scheduling system. (SLURM manages jobs, job steps, nodes, partitions (groups of nodes), and other entities on the cluster.)

There are several basic SLURM commands you'll likely use often:

<ul>
	<li><code>sbatch</code>&nbsp;- Submit a job to the batch queue system, e.g., <code>sbatch myjob.sh</code>, where <code>myjob.sh</code>&nbsp;is a SLURM job script</li>
	<li><code>srun</code>&nbsp;- Submit an interactive job to the batch queue system</li>
	<li><code>scancel</code><code>&nbsp;</code>- Cancel a job, e.g., <code>scancel 123</code>, where 123&nbsp;is a job ID</li>
	<li><code>squeue</code>&nbsp;- Check the current jobs in the batch queue system, e.g., <code>squeue -u $USER</code> to view your own jobs</li>
	<li><code>sq</code>&nbsp;- Check why your job is not running, e.g., <code>module load sq; sq</code></li>
	<li><code>sacctmgr</code>&nbsp;- Check what resources (accounts, partitions, and QoS) you or your FCA or Condo project have access to, e.g., <code>sacctmgr -p show associations user=$USER</code> or <code>sacctmgr -p show associations account=project_name</code></li>
	<li><code>sinfo</code>&nbsp;- View the status of the cluster's compute nodes, including how many nodes - of what types - are currently available for running jobs.</li>
	<li><code>sacct</code>&nbsp;- Display accounting data for your submitted jobs and job steps from the SLURM job accounting log or SLURM database. This command allows one to inspect jobs which are already completed (although you can look at queued and running jobs with this command as well).</li>
</ul>

Please see the following for detailed information on:
<ul>
<li><a href="what-resources">Determining what resources your jobs can use and what the charges are</a></li>
<li><a href="submitting-jobs">Submitting jobs</a></li>
<li><a href="why-job-not-run">Understanding when your job will start or why it is not running</a></li>
<li><a href="monitoring-jobs">Monitoring running jobs and checking finished jobs</a></li>
</ul>

