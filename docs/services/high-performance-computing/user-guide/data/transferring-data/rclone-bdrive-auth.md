---
title: Authenticating rclone to Transfer Data Between Savio and Your UC Berkeley bDrive Account
keywords: high performance computing, berkeley research computing
tags: [hpc]
---

Here are the steps for setting up a bDrive account for access via rclone from Savio.

!!!note
    Research IT strongly recommends that rclone be used with a <a href="https://calnetweb.berkeley.edu/calnet-departments/special-purpose-accounts-spa" target="_blank">Special Purpose Account</a> (SPA), and not with the bDrive storage owned by (and accessible via) your personal CalNet ID. Separating this third-party tool’s access from the login you may use to store sensitive data or intellectual property (e.g., papers and monographs in progress; FERPA-protected student information; etc.) is an effective way of safeguarding your files. Also, use of a SPA account conveniently enables desired access by current -- and future -- colleagues, successors, tech support personnel, et al.

!!!note
    You can set up access to multiple bDrive accounts by following the instructions below and entering unique names at the `name` prompt below.
    
We recommend that you run the following steps from within our <a href="../../../../../../services/high-performance-computing/user-guide/ood/#desktop-app">OOD Desktop App service</a>. The reason is that in step 6 below, you'll need to authenticate to Google Drive in a browser and running the rclone config process and the browser authentication both with OOD Desktop makes it easier to copy the needed authentication text to the rclone config session. However, as discussed below, you could connect to the DTN in a separate terminal window and run the browser on your own machine (where you'll need rclone installed, ideally using the same version of rclone as is on Savio). 

<p>1. Start up a Savio <a href="../../../../../../services/high-performance-computing/user-guide/ood/#desktop-app">OOD Desktop App service session</a> and ssh to the DTN, where rclone is installed. (Alternatively, if you'll use a browser on your own machine in Step 6, you can ssh to Savio and then to the DTN.)</p>
<p><code>[paciorek@n0002 ~]$ ssh dtn</code></p>

<p>2. Configure rclone to access the bDrive account by setting up a new 'remote':</p>
<p><code>[paciorek@dtn ~]$ rclone config<br>
2019/02/13 16:51:04 NOTICE: Config file "/global/home/users/paciorek/.config/rclone/rclone.conf" not found - using defaults<br>
No remotes found - make a new one<br>
n) New remote<br>
s) Set configuration password<br>
q) Quit config<br>
n/s/q&gt; n</code></p>

<p>3. Name the remote a unique name; you can use "bDrive" if only planning to access one bDrive account, but otherwise choose a unique name for each bDrive account. Then enter the number corresponding to Google Drive (17 in this case).</p>
<p><code>name&gt; bDrive<br>
Type of storage to configure.<br>
Choose a number from below, or type in your own value</code></p>
<p><code>[...snip...]<br>
15 / FTP Connection<br>
   \ (ftp)<br>
16 / Google Cloud Storage (this is not Google Drive)<br>
   \ (google cloud storage)<br>
17 / Google Drive<br>
   \ (drive)<br>
18 / Google Photos<br>
   \ (google photos)<br>
19 / Hadoop distributed file system<br>
   \ (hdfs)<br>
20 / Hubic<br>
   \ (hubic)<br>
[...snip...]</code></p>
<p><code>Storage&gt; 17<br>
** See help for drive backend at: https://rclone.org/drive/ **</code></p>

<p>4. Now answer a few questions. (If speed is a concern, you may want
to set up a Google Application Client Id, as suggested.)</p>
<p><code>Google Application Client Id <br>
Setting your own is recommended.<br>
See https://rclone.org/drive/#making-your-own-client-id for how to create your own.<br>
If you leave this blank, it will use an internal key which is low performance.<br>
Enter a string value. Press Enter for the default ("").<br>
client_id><br>
Google Application Client Secret<br>
Setting your own is recommended.<br>
Enter a string value. Press Enter for the default ("").<br>
client_secret><br>
Scope that rclone should use when requesting access from drive.<br>
Enter a string value. Press Enter for the default (""). <br>
Choose a number from below, or type in your own value<br>
 1 / Full access all files, excluding Application Data Folder. <br>
   \ "drive"<br>
 2 / Read-only access to file metadata and file contents. <br>
   \ "drive.readonly"<br>
   / Access to files created by rclone only. <br>
 3 | These are visible in the drive website. <br>
   | File authorization is revoked when the user deauthorizes the app. <br>
   \ "drive.file"<br>
   / Allows read and write access to the Application Data folder. <br>
 4 | This is not visible in the drive website. <br>
   \ "drive.appfolder"<br>
   / Allows read-only access to file metadata but <br>
 5 | does not allow any access to read or download file content. <br>
   \ "drive.metadata.readonly"<br>
 scope><br>
ID of the root folder <br>
Leave blank normally. <br>
<br>
Fill in to access "Computers" folders (see docs), or for rclone to use <br>
a non root folder as its starting point. <br>
<br>
Note that if this is blank, the first time rclone runs it will fill it <br>
in with the ID of the root folder. <br>
<br>
Enter a string value. Press Enter for the default (""). <br>
root_folder_id><br>
Service Account Credentials JSON file path <br>
Leave blank normally. <br>
Needed only if you want use SA instead of interactive login. <br>
Enter a string value. Press Enter for the default (""). <br>
service_account_file> <br>
Edit advanced config? (y/n) <br>
y) Yes <br>
n) No <br>
y/n> n </code></p>

<p> 5. When asked to use 'auto config', make sure to say 'No', because
you need to authenticate with Google via a browser on your local
machine (e.g., your laptop.).</p>
<p><code>Use auto config?<br>
  Say Y if not sure <br>
  Say N if you are working on a remote or headless machine <br>
y) Yes <br>
n) No <br>
y/n> n </code></p>

<p>6. At this point, you're at the authentication step. You'll see this prompt.</p>
<p><code>Option config_token.<br>
For this to work, you will need rclone available on a machine that has<br>
a web browser available.<br>
For more help and alternate methods see: https://rclone.org/remote_setup/<br>
Execute the following on the machine with the web browser (same rclone<br>
version recommended):<br>
	rclone authorize "drive"<br>
Then paste the result.<br>
Enter a value.</code></p>
<p> In order to obtain the value to enter, do the following. You'll need to start a terminal on a machine with a browser available. If using the OOD Desktop app (as we recommend), open a new terminal window in the OOD Desktop. If using your own machine, install rclone on that machine; installation will produce an executable called `rclone` (Mac/Linux) or `rclone.exe` (Windows). Run <code>rclone authorize "drive"</code> in the terminal as follows. If using OOD Desktop app you will probably need to start the Firefox browser in the Desktop yourself and go to the link indicated (the link here is just example - make sure to fill in your own).<br>
<code>[paciorek@n0002 ~]$ rclone authorize drive<br>
2022/07/01 15:32:56 NOTICE: If your browser doesn't open automatically go to the following link: http://127.0.0.1:53682/auth?state=ynpI5lWCT05AUHq238cAg<br>
2022/07/01 15:32:56 NOTICE: Log in and authorize rclone for access<br>
2022/07/01 15:32:56 NOTICE: Waiting for code...<br>
2022/07/01 15:34:20 NOTICE: Got code<br>
Paste the following into your remote machine ---><br>
{access_token:ya29.A0ARrdaM-AeTSNHfutneXsHSyyyGL4S2KdIL4-XGBRj8WYezmECfze0z3oi3w9bhyseO3yzpT-XYHzmRI2rnUh2D0W9v2wgVqgm-lBRTH3QuA5rDqSEHAiGV_DYcmLm-2342sd90DFshxMrYUNnWUtBVEFTQVRBU0ZRRl91NjFWdFRBS1FXR29qdElpUnh2TElkOEhydw0163,token_type:Bearer,refresh_token:1//06r-htFlivTPGCgYIARAAGAYSNwF-L9IrLqkDje8JFdKQXpoRxKvUh0tngiz6I6kPEJRTj4qd6emy4bPIrDxhBNmIn_42fVeIYaA,expiry:2022-07-01T16:34:19.314138579-07:00}</code></p>
<p>7. Paste that string (the string below is just an example - make sure to fill in your own) in as requested:</p>
<p><code>Option config_token.<br>
For this to work, you will need rclone available on a machine that has<br>
a web browser available.<br>
For more help and alternate methods see: https://rclone.org/remote_setup/<br>
Execute the following on the machine with the web browser (same rclone<br>
version recommended):<br>
	rclone authorize "drive"<br>
Then paste the result.<br>
Enter a value.<br>
{access_token:ya29.A0ARrdaM-AeTSNHfutneXsHSyyyGL4S2KdIL4-XGBRj8WYezmECfze0z3oi3w9bhyseO3yzpT-XYHzmRI2rnUh2D0W9v2wgVqgm-lBRTH3QuA5rDqSEHAiGV_DYcmLm-2342sd90DFshxMrYUNnWUtBVEFTQVRBU0ZRRl91NjFWdFRBS1FXR29qdElpUnh2TElkOEhydw0163,token_type:Bearer,refresh_token:1//06r-htFlivTPGCgYIARAAGAYSNwF-L9IrLqkDje8JFdKQXpoRxKvUh0tngiz6I6kPEJRTj4qd6emy4bPIrDxhBNmIn_42fVeIYaA,expiry:2022-07-01T16:34:19.314138579-07:00}</code></p>
<p>8. You can now choose whether to configure this as a team
drive. We'll enter 'No' for a basic setup.</p>
<p><code>Configure this as a team drive?<br>
y) Yes <br>
n) No <br>
y/n> n </code></p>
<p>9. At this point, you'll see something like this, and you can accept the addition of the remote and quit out of the configuration.</p>
<p><code>[bDrive]<br>
type = drive <br>
token = {access_token:ya29.A0ARrdaM-AeTSNHfutneXsHSyyyGL4S2KdIL4-XGBRj8WYezmECfze0z3oi3w9bhyseO3yzpT-XYHzmRI2rnUh2D0W9v2wgVqgm-lBRTH3QuA5rDqSEHAiGV_DYcmLm-2342sd90DFshxMrYUNnWUtBVEFTQVRBU0ZRRl91NjFWdFRBS1FXR29qdElpUnh2TElkOEhydw0163,token_type:Bearer,refresh_token:1//06r-htFlivTPGCgYIARAAGAYSNwF-L9IrLqkDje8JFdKQXpoRxKvUh0tngiz6I6kPEJRTj4qd6emy4bPIrDxhBNmIn_42fVeIYaA,expiry:2022-07-01T16:34:19.314138579-07:00}<br>
--------------------<br>
y) Yes this is OK <br>
e) Edit this remote <br>
d) Delete this remote <br>
y/e/d> y <br>
Current remotes:<br>
Name Type<br>
==== ====<br>
bDrive bDrive<br>
e) Edit existing remote<br>
n) New remote<br>
d) Delete remote<br>
r) Rename remote<br>
c) Copy remote<br>
s) Set configuration password<br>
q) Quit config<br>
e/n/d/r/c/s/q&gt; q</code></p>
<p>Now you should be all set to copy files to/from Savio and bDrive.</p>

### Alternative to using a SPA account for rclone data transfers</h3>

If you are working alone, with no colleagues and no need to make your backups available to others should you leave UC Berkeley, `rclone` provides an "access scope" setting that allows you to use your individual bDrive account while maintaining the safety of your other files by isolating them from the rclone application. This feature is available for rclone version 1.40 and higher.

To utilize this feature, you will need to grant a limited access scope to rclone during the configuration process, as follows:

* During the configuration process you will be asked to choose the `Scope that rclone should use when requesting access from drive`
* Choose the scope `Access to files created by rclone only ... drive.file` instead of `Full access all files ... drive`.

This choice will instruct bDrive to grant access only to files that have been added to bDrive using `rclone`; and `rclone` will not be able to "see," move, delete, or transfer files added to bDrive using the Google Drive web browser interface.

If using this configuration, you may find it convenient to create a single folder within which `rclone` can add files and folders. A command that writes to the folder `rclone-bkp` (creating it if the folder doesn't exist yet), in the account for which the `rclone` configuration is named `my-bdrive` might look something like this:

```sh
rclone copy /home/mylogin/Documents/bkp my-bdrive:rclone-bkp
```

To use a sub-folder somefiles in the rclone-bkp folder:

```sh
rclone copy /home/mylogin/Documents/bkp/somefiles my-bdrive:rclone-bkp/somefiles
```
