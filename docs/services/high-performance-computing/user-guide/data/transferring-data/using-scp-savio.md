---
title: Using SCP with the BRC Supercluster
keywords: high performance computing, berkeley research computing
tags: [hpc]
---

<p>This document describes how to use SCP to transfer data between your computer and the Berkeley Savio and Vector clusters.</p>
<!--break--><p>
This video walks through the process of using SCP:</p>
<iframe width="560" height="315" src="https://www.youtube.com/embed/ScBho4OkyJI?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe><p>
The following examples show how to transfer files using SCP via the <kbd>scp</kbd> command in a shell/terminal window, on your own computer running Mac OS X, Linux, or Unix. Keep in mind that when transferring data using file transfer software such as SCP, you should connect only to the cluster's Data Transfer Node, <code>dtn.brc.berkeley.edu</code>.</p>
<p>To <strong>transfer a single file</strong> called <kbd>myfile.txt</kbd> from your computer to your home directory on the BRC supercluster, enter the following command in the directory on your computer where <kbd>myfile.txt</kbd> is located (substitute your actual username for <kbd>myusername</kbd> in the two places it appears below, and substitute your acutal file name for <kbd>myfile.txt</kbd> where it appears below):</p>
<pre>scp myfile.txt myusername@dtn.brc.berkeley.edu:/global/home/users/myusername</pre><p>
<p>To <strong>transfer a single file</strong> called <kbd>myfile.txt</kbd> from your home directory on the BRC supercluster to your home directory on your computer (substitute your actual username for <kbd>myusername</kbd> and your acutal file name for <kbd>myfile.txt</kbd> where it appears below):</p>
<pre>scp myusername@dtn.brc.berkeley.edu:/myfile.txt ~/.</pre><p>
To <strong>transfer a folder</strong> called <kbd>myfolder</kbd> and its contents from your computer to your home directory on the BRC supercluster, enter the following command in the directory on your computer where <kbd>myfolder</kbd> is located (substitute your actual username for <kbd>myusername</kbd> in the two places it appears below, and substitute the actual directory name for <kbd>myfolder</kbd> where it appears below):</p>
<pre>scp -r myfolder myusername@dtn.brc.berkeley.edu:/global/home/users/myusername</pre><p>
To transfer that same folder on your computer to the shared Scratch filesystem on Savio, enter the following command in the directory on your computer where <kbd>myfolder</kbd> is located (substitute your actual username for <kbd>myusername</kbd> in the two places it appears below):</p>
<pre>scp -r myfolder myusername@dtn.brc.berkeley.edu:/global/scratch/users/myusername</pre>
To <strong>transfer a folder</strong> called <kbd>myfolder</kbd> and its contents from your home directory on the BRC supercluster to your home directory on your computer (substitute your actual username for <kbd>myusername</kbd> and your acutal directory name for <kbd>myfolder</kbd> where it appears below):</p>
<pre>scp -r myusername@dtn.brc.berkeley.edu:~/myfolder ~/.</pre><p>
To transfer that same folder on your computer to the shared Scratch filesystem on Savio, enter the following command in the directory on your computer where <kbd>myfolder</kbd> is located (substitute your actual username for <kbd>myusername</kbd> in the two places it appears below):</p>
<pre>scp -r myfolder myusername@dtn.brc.berkeley.edu:/global/scratch/users/myusername</pre>
