---
title: Savio User Guide
keywords: high performance computing
tags: [hpc]
---

# Hardware Config

## Savio Hardware Configuration

<p>The following table details the hardware configuration of each partition of nodes. Each partition corresponds to a combination of a generation and type of node. </p>

<table id="hardware-config-table" align="center" border="1" cellspacing="0"><tbody><tr><th style="text-align:center">Partition</th>
<th style="text-align:center">Nodes</th>
<th style="text-align:center">Node List</th>
<th style="text-align:center">CPU Model</th>
<th style="text-align:center"># Cores / Node</th>
<th style="text-align:center">Memory / Node</th>
<th style="text-align:center">Infiniband</th>
<th style="text-align:center">Specialty</th>
<th style="text-align:center">Scheduler Allocation</th>
</tr><tr><td style="text-align:center; vertical-align:middle">savio [Retired]</td>
<td style="text-align:center; vertical-align:middle">132</td>
<td style="text-align:center; vertical-align:middle">n0[000-095].savio1</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2670 v2</td>
<td style="text-align:center; vertical-align:middle">20</td>
<td style="text-align:center; vertical-align:middle">64 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">-</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio_bigmem [Retired]</td>
<td style="text-align:center; vertical-align:middle">4</td>
<td style="text-align:center; vertical-align:middle">n0[096-099].savio1</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2670 v2</td>
<td style="text-align:center; vertical-align:middle">20</td>
<td style="text-align:center; vertical-align:middle">512 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">BIGMEM</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2</td>
<td style="text-align:center; vertical-align:middle">124</td>
<td style="text-align:center; vertical-align:middle">n0[027-150].savio2</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2670 v3</td>
<td style="text-align:center; vertical-align:middle">24</td>
<td style="text-align:center; vertical-align:middle">64 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">-</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2</td>
<td style="text-align:center; vertical-align:middle">4</td>
<td style="text-align:center; vertical-align:middle">n0[290-293].savio2</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2650 v4</td>
<td style="text-align:center; vertical-align:middle">24</td>
<td style="text-align:center; vertical-align:middle">64 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">-</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2</td>
<td style="text-align:center; vertical-align:middle">35</td>
<td style="text-align:center; vertical-align:middle">n0[187-210].savio2<br>
n0[230-240].savio2</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2680 v4</td>
<td style="text-align:center; vertical-align:middle">28</td>
<td style="text-align:center; vertical-align:middle">64 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">-</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2_bigmem</td>
<td style="text-align:center; vertical-align:middle">36</td>
<td style="text-align:center; vertical-align:middle">n0[151-182].savio2</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2670 v3</td>
<td style="text-align:center; vertical-align:middle">24</td>
<td style="text-align:center; vertical-align:middle">128 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">BIGMEM</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2_bigmem</td>
<td style="text-align:center; vertical-align:middle">8</td>
<td style="text-align:center; vertical-align:middle">n0[282-289].savio2</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2650 v3</td>
<td style="text-align:center; vertical-align:middle">24</td>
<td style="text-align:center; vertical-align:middle">128 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">BIGMEM</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2_htc</td>
<td style="text-align:center; vertical-align:middle">20</td>
<td style="text-align:center; vertical-align:middle">n0[000-011].savio2<br>
n0[215-222].savio2</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2643 v3</td>
<td style="text-align:center; vertical-align:middle">12</td>
<td style="text-align:center; vertical-align:middle">128 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">HTC</td>
<td style="text-align:center; vertical-align:middle">By Core</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2_gpu  [Retired (GPUs not available)]</td>
<td style="text-align:center; vertical-align:middle">17</td>
<td style="text-align:center; vertical-align:middle">n0[012-026].savio2<br>n0[223-224].savio2</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2623 v3</td>
<td style="text-align:center; vertical-align:middle">8</td>
<td style="text-align:center; vertical-align:middle">64 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">4x Nvidia K80 (12 GB GPU memory per GPU)</td>
<td style="text-align:center; vertical-align:middle">By Core</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2_1080ti</td>
<td style="text-align:center; vertical-align:middle">8</td>
<td style="text-align:center; vertical-align:middle">n0[227-229].savio2<br>n0[298-302]</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2623 v3</td>
<td style="text-align:center; vertical-align:middle">8</td>
<td style="text-align:center; vertical-align:middle">64 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">4x Nvidia 1080ti (11 GB GPU memory per GPU)</td>
<td style="text-align:center; vertical-align:middle">By Core</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio2_knl</td>
<td style="text-align:center; vertical-align:middle">28</td>
<td style="text-align:center; vertical-align:middle">n0[254-281].savio2</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon Phi 7210</td>
<td style="text-align:center; vertical-align:middle">64</td>
<td style="text-align:center; vertical-align:middle">188 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">Intel Phi</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio3</td>
<td style="text-align:center; vertical-align:middle">112</td>
<td style="text-align:center; vertical-align:middle">n0[010-029].savio3<br>n0[042-129].savio3<br>n0[146-149].savio3</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon Skylake 6130 @ 2.1 GHz</td>
<td style="text-align:center; vertical-align:middle">32</td>
<td style="text-align:center; vertical-align:middle">96 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">&nbsp;</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio3</td>
<td style="text-align:center; vertical-align:middle">80</td>
<td style="text-align:center; vertical-align:middle">n0[130-133].savio3<br>n0[139-142].savio3<br>n0[150-157]<br>n0[170-173].savio3<br>n0[193-208].savio3<br>n0[222-257].savio3<br>n0[265-272].savio3</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon Skylake 6230 @ 2.1 GHz</td>
<td style="text-align:center; vertical-align:middle">40</td>
<td style="text-align:center; vertical-align:middle">96 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">&nbsp;</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio3_bigmem</td>
<td style="text-align:center; vertical-align:middle">16</td>
<td style="text-align:center; vertical-align:middle">n0[006-009].savio3<br>n0[030-041].savio3</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon Skylake 6130 @ 2.1 GHz</td>
<td style="text-align:center; vertical-align:middle">32</td>
<td style="text-align:center; vertical-align:middle">384 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">BIGMEM</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio3_bigmem</td>
<td style="text-align:center; vertical-align:middle">4</td>
<td style="text-align:center; vertical-align:middle">n0[162-165].savio3</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon Gold 6230 @ 2.1 GHz</td>
<td style="text-align:center; vertical-align:middle">40</td>
<td style="text-align:center; vertical-align:middle">384 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">BIGMEM</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio3_htc</td>
<td style="text-align:center; vertical-align:middle">24</td>
<td style="text-align:center; vertical-align:middle">n0[166-169].savio3<br>n0[177-192].savio3<br>n0[218-221].savio3</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon Skylake 6230 @ 2.1 GHz</td>
<td style="text-align:center; vertical-align:middle">40</td>
<td style="text-align:center; vertical-align:middle">384 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">HTC</td>
<td style="text-align:center; vertical-align:middle">By Core</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio3_xlmem</td>
<td style="text-align:center; vertical-align:middle">2</td>
<td style="text-align:center; vertical-align:middle">n0[000-001].savio3</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon Skylake 6130 @ 2.1 GHz</td>
<td style="text-align:center; vertical-align:middle">32</td>
<td style="text-align:center; vertical-align:middle">1.5 TB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">XL Memory</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio3_xlmem</td>
<td style="text-align:center; vertical-align:middle">2</td>
<td style="text-align:center; vertical-align:middle">n0[002-003].savio3</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon Skylake 6130 @ 2.1 GHz</td>
<td style="text-align:center; vertical-align:middle">52</td>
<td style="text-align:center; vertical-align:middle">1.5 TB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">XL Memory</td>
<td style="text-align:center; vertical-align:middle">By Node</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio3_gpu</td>
<td style="text-align:center; vertical-align:middle">9</td>
<td style="text-align:center; vertical-align:middle">n0[134-138].savio3<br>n0[158-161].savio3</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon Skylake 6130 @ 2.1 GHz</td>
<td style="text-align:center; vertical-align:middle">8</td>
<td style="text-align:center; vertical-align:middle">96 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">4x GTX 2080ti GPU (11 GB GPU memory per GPU)</td>
<td style="text-align:center; vertical-align:middle">By Core</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio3_gpu</td>
<td style="text-align:center; vertical-align:middle">6</td>
<td style="text-align:center; vertical-align:middle">n0[143-145].savio3<br>n0[174-176].savio3</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon Skylake 6130 @ 2.1 GHz</td>
<td style="text-align:center; vertical-align:middle">32</td>
<td style="text-align:center; vertical-align:middle">384 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">8x TITAN RTX GPU (24 GB GPU memory per GPU)</td>
<td style="text-align:center; vertical-align:middle">By Core</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio3_gpu</td>
<td style="text-align:center; vertical-align:middle">1</td>
<td style="text-align:center; vertical-align:middle">n0004.savio3</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon Silver 4208 @ 2.1 GHz</td>
<td style="text-align:center; vertical-align:middle">16</td>
<td style="text-align:center; vertical-align:middle">192 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">2x Tesla V100 GPU (32 GB GPU memory per GPU)</td>
<td style="text-align:center; vertical-align:middle">By Core</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio3_gpu</td>
<td style="text-align:center; vertical-align:middle">1</td>
<td style="text-align:center; vertical-align:middle">n0005.savio3</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon E5-2623 v3 @ 3.0 GHz</td>
<td style="text-align:center; vertical-align:middle">8</td>
<td style="text-align:center; vertical-align:middle">64 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">2x Tesla V100 GPU (32 GB GPU memory per GPU)</td>
<td style="text-align:center; vertical-align:middle">By Core</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio3_gpu</td>
<td style="text-align:center; vertical-align:middle">14</td>
<td style="text-align:center; vertical-align:middle">n0[209-216].savio3<br>n0[263-264].savio3<br>n0[273-276].savio3</td>
<td style="text-align:center; vertical-align:middle">AMD EPYC 7302P (or 7313P)</td>
<td style="text-align:center; vertical-align:middle">16</td>
<td style="text-align:center; vertical-align:middle">250 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">2x A40 GPU (48 GB GPU memory per GPU)</td>
<td style="text-align:center; vertical-align:middle">By Core</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio3_gpu</td>
<td style="text-align:center; vertical-align:middle">2</td>
<td style="text-align:center; vertical-align:middle">n0217.savio3<br>n0262.savio3</td>
<td style="text-align:center; vertical-align:middle">AMD EPYC 7443P</td>
<td style="text-align:center; vertical-align:middle">24</td>
<td style="text-align:center; vertical-align:middle">250 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">2x A40 GPU (48 GB GPU memory per GPU)</td>
<td style="text-align:center; vertical-align:middle">By Core</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio3_gpu</td>
<td style="text-align:center; vertical-align:middle">4</td>
<td style="text-align:center; vertical-align:middle">n0[277-281].savio3</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon Gold 6338</td>
<td style="text-align:center; vertical-align:middle">64</td>
<td style="text-align:center; vertical-align:middle">512 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">4x A40 GPU (48 GB GPU memory per GPU)</td>
<td style="text-align:center; vertical-align:middle">By Core</td>
</tr><tr><td style="text-align:center; vertical-align:middle">savio3_gpu</td>
<td style="text-align:center; vertical-align:middle">2</td>
<td style="text-align:center; vertical-align:middle">n0[282-283].savio3</td>
<td style="text-align:center; vertical-align:middle">AMD EPYC 7543P</td>
<td style="text-align:center; vertical-align:middle">32</td>
<td style="text-align:center; vertical-align:middle">512 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">4x A40 GPU (48 GB GPU memory per GPU)</td>
<td style="text-align:center; vertical-align:middle">By Core</td>
</tr>

<tr><td style="text-align:center; vertical-align:middle">savio4_htc</td>
<td style="text-align:center; vertical-align:middle">52</td>
<td style="text-align:center; vertical-align:middle">n0[000-027].savio4<br>n0[116-119].savio4<br>n0[146-165].savio4</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon Gold 6330 @ 2.0 GHz</td>
<td style="text-align:center; vertical-align:middle">56</td>
<td style="text-align:center; vertical-align:middle">512 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle"></td>
<td style="text-align:center; vertical-align:middle">By Core</td>
</tr>

<tr><td style="text-align:center; vertical-align:middle">savio4_htc</td>
<td style="text-align:center; vertical-align:middle">104</td>
<td style="text-align:center; vertical-align:middle">n0[028-035].savio4<br>n0[040-115].savio4<br>n0[166-185].savio4</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon Gold 6330 @ 2.0 GHz</td>
<td style="text-align:center; vertical-align:middle">56</td>
<td style="text-align:center; vertical-align:middle">256 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle"></td>
<td style="text-align:center; vertical-align:middle">By Core</td>
</tr>

<tr><td style="text-align:center; vertical-align:middle">savio4_gpu</td>
<td style="text-align:center; vertical-align:middle">26</td>
<td style="text-align:center; vertical-align:middle">n0[120-145].savio4</td>
<td style="text-align:center; vertical-align:middle">Intel Xeon Gold 6326 @ 2.9 GHz</td>
<td style="text-align:center; vertical-align:middle">32</td>
<td style="text-align:center; vertical-align:middle">512 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">8x RTX A5000 (24 GB GPU memory per GPU)</td>
<td style="text-align:center; vertical-align:middle">By Core</td>
</tr>

<tr><td style="text-align:center; vertical-align:middle">savio4_gpu</td>
<td style="text-align:center; vertical-align:middle">1</td>
<td style="text-align:center; vertical-align:middle">n0386.savio4</td>
<td style="text-align:center; vertical-align:middle">AMD EPYC 9554 @ 3.1 GHz</td>
<td style="text-align:center; vertical-align:middle">64</td>
<td style="text-align:center; vertical-align:middle">792 GB</td>
<td style="text-align:center; vertical-align:middle">FDR</td>
<td style="text-align:center; vertical-align:middle">8x L40 (46 GB GPU memory per GPU)</td>
<td style="text-align:center; vertical-align:middle">By Core</td>
</tr>

</tbody></table>

## CGRL Hardware Configuration

<p>The CGRL hardware consists of the Vector cluster (a cluster separate from Savio) and the Rosalind condo within Savio. Vector and Rosalind are heterogeneous, with a mix of several different types of nodes. Please be aware of these various hardware configurations, along with their associated <a href="#Scheduler-Configuration" class="toc-filter-processed">scheduler configurations</a>, when specifying options for running your jobs.</p>

| Cluster | Nodes | Node List | CPU | Cores/Node | Memory/Node | Scheduler Allocation |
|---------|-------|-----------|-----|------------|-------------|----------------------|
| Vector | 11 | n00[00-03].vector0 <br><br> n0004.vector0 <br><br><br><br> n00[05-08].vector0 <br><br> n00[09]-n00[10].vector0 | Intel Xeon X5650, 2.66 GHz <br> AMD Opteron 6176, 2.3 GHz <br> Intel Xeon E5-2670, 2.60 GHz <br> Intel Xeon X5650, 2.66 GHz | 12 <br><br><br> 48 <br><br><br><br> 16 <br><br><br> 12 | 96 GB <br><br><br> 256 GB <br><br><br><br> 128 GB <br><br><br> 48 | By Core |
| Rosalind (savio2_htc) | 8 | floating condo within: <br><br> n0[000-011].savio2, n0[215-222].savio2 | Intel Xeon E5-2643 v3, 3.40 GHz | 12 | 128 GB | By Core |
| Rosalind (savio3_xlmem) | 2 | floating condo within: <br><br> n0[000-003].savio3 | Intel Xeon Skylake 6130 @ 2.1 GHz | 32 | 1.5 TB | By Node |
