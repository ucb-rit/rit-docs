---
title: Using Authy on a desktop computer to generate one-time passwords for Savio
keywords: high performance computing, berkeley research computing
tags: [hpc]
---

# Using Authy and Generating OTP

!!! summary
    <a href="https://www.authy.com/app/" target="_blank">Authy</a>, a free app from Twilio, can generate one-time passwords (OTPs) on your laptop or desktop computer, which you can use when logging into the <a href="https://docs-research-it.berkeley.edu/services/high-performance-computing">Savio</a> high-performance computing cluster at UC Berkeley.

    We recommend using the Authy app if you do not have a mobile device - such as most iOS or Android smartphones and tablets - capable of running the Google Authenticator app. (If you do have one of these, you can find setup instructions for your device in <a href="https://docs-research-it.berkeley.edu/services/high-performance-computing/user-guide/logging-brc-clusters/">Logging into Savio</a>.)

    Authy is a desktop app. This app can be installed on <a href="https://authy.com/download/" target="_blank">MS Windows, OS X / macOS, and Linux</a>.

### Setting up Authy

<p>Setup steps:</p>
!!! note
    This documentation was completed with a Mac OS, using a Chrome browser.
<ol><li><p>Using your browser of choice, <a href="https://authy.com/download/" target="_blank">download Authy</a>.</p></li>
<li>In the following section of the <a href="https://authy.com/download/" target="_blank">download page</a>, click the icon for the device and OS you're using:
<p><img alt="Screenshot: Download Page on Authy Website" class="media-element file-default image-style-none" data-delta="1" typeof="foaf:Image" height="300" src="../../../../img/authy-step2.png"></p></li>
<li><p>Download to your device and, once completed, launch Authy on your device.</p></li>
<li>Follow the on-screen instructions to send a text message to your phone, or to call you. This will give you a code that you will need to enter into Authy.<p>If you have a landline phone, or a cell phone that can't receive text messages, be sure to click the "Call" button - rather than the "SMS" button - at the "Verify my identity via" prompt.</p></li>
<li>Next, in Authy's window, click the "Settings" (gear) icon (lower right):
<p><img alt="Screenshot: Authy Tokens Screen" class="media-element file-default image-style-none" data-delta="1" typeof="foaf:Image" height="500" src="../../../../img/authy-step57.png"></p></li>
<li>Set up a Master password by going to “General”, selecting "Master Password", and then following the onscreen instructions:
<p><img alt="Screenshot: Authy Settings" class="media-element file-default image-style-none" data-delta="1" typeof="foaf:Image" height="500" src="../../../../img/authy-step6.png"></p></li>
<li>After setting a Master Password, navigate back to the “Tokens” screen:
<p><img alt="Screenshot: Authy Tokens Screen" class="media-element file-default image-style-none" data-delta="1" typeof="foaf:Image" height="500" src="../../../../img/authy-step57.png"></p></li>
<li>Select the red Plus ('+') button on that screen to create a new account. You'll now see a screen asking for you to enter a code:
<p><img alt="Screenshot: Authy Add Account" class="media-element file-default image-style-none" data-delta="1" typeof="foaf:Image" height="500" src="../../../../img/authy-step8.png"></p></li>
<li><p>Now you'll leave Authy, go back to your browser window, and get that code. Visit the <a href="https://identity.lbl.gov/otptokens/hpccluster" target="_blank">Non-LBL Token Management</a> web page to access the code you need.</p></li>
<li>Login to the <a href="https://identity.lbl.gov/otptokens/hpccluster" target="_blank">Non-LBL Token Management</a> web page by clicking the button for the external account (UC Berkeley CalNet, Google, Facebook, or LinkedIn) that you previously linked to your Savio/BRC cluster account.
<p>(If, when doing so, you encounter the error message, "Login Error: There was an error logging you in. The account that you logged in with has not been mapped to a system account", <a href="https://docs.google.com/forms/d/e/1FAIpQLSc4JhBBFjIJsGQmrFwomIxW54e4TTMHtil6cKahKfL0whtcIQ/viewform" target="_blank">please complete this form to link your personal account to a BRC cluster account</a>. Then, return right back here, to re-try this step in the Authy instructions.)</p></li>
<li>From the "Token Management" page which appears, create a new token by clicking on "Add an HPC Cluster/Linux Workstation token" and following the onscreen instructions.
<p>IMPORTANT: Remember the PIN that you are setting on the token.
Note: Even if you've already created one or more tokens for use with Google Authenticator on a smartphone or tablet, you'll still need to create a new token for use with Authy.</p></li>
<li>After you've successfully created your new token, a QR code for that token will then be displayed.
<p><img alt="Screenshot: Scan Code with Google Authenticator" class="media-element file-default image-style-none" data-delta="1" typeof="foaf:Image" src="../../../../img/authy-step12.png"></p></li>
<li>Because Authy doesn't have a way to scan the QR code (directly or via a helper app), you'll need to extract the 'secret' from the currently displayed webpage.
<p>(The instructions that follow here are a bit tricky ("fiddly") so please be sure to pay close attention to both the instructions and screenshots.)</p>
<p>To do so, right click on the QR code and select “Source”.</p></li>
<li>Click the "Inspect Element" icon - the icon with the 'arrow in a box,' at the upper left of the right-hand panel.
<p><img alt="Screenshot: Inspect Element Icon" class="media-element file-default image-style-none" data-delta="1" typeof="foaf:Image" src="../../../../img/authy-step14.png"></p></li>
<li>Then click on the QR code, so that code is highlighted:
<p><img alt="Screenshot: Click QR Code to Highlight" class="media-element file-default image-style-none" data-delta="1" typeof="foaf:Image" src="../../../../img/authy-step15.png"></p></li>
<li>Over in the right-hand panel, you'll see some text highlighted, which will most likely begin with <code>img style=....</code> (That's the HTML markup which corresponds to the image of the QR code, in the left-hand panel.)
<p>Press the up arrow key on your keyboard - typically twice - until a block of text just before this is selected: the text that begins with <code>div id="qrcode":</code>
<p><img alt="Screenshot: Navigate HTML Markup" class="media-element file-default image-style-none" data-delta="1" typeof="foaf:Image" src="../../../../img/authy-step16.png"></p></p></li>
<li>Within that block of text, select and copy the "secret" text to the Clipboard. That's the text immediately following <code>secret=</code> and ending before <code>&issuer=</code>, in the token string that begins with <code>otpauth:&#xfeff;//</code>, in the location shown by <code>**secrettexttocopyishere**</code> in the example below:
<p><code>otpauth:&#xfeff;//totp/hpcs%3ATOTP10976BCD?secret=**secrettexttocopyishere**&issuer=Lawrence%20Berkeley%20National%20Laboratory</code></p>
<p>The "secret" text will typically be 32 characters in length, and consist of both uppercase letters and digits. For example:
<p><img alt="Screenshot: Secret Text Example" class="media-element file-default image-style-none" data-delta="1" typeof="foaf:Image" src="../../../../img/authy-step17.png"></p></p>
<p>If you can't easily select just that "secret" text itself, as an alternative, you can paste in the full token and perhaps even some surrounding text into a text editor or word processing application, and select that text there. (If you do so, for optimum security, do not ever save that token - nor the "secret" text within that token - in any document on your disk.)</p></li>
<li>Paste that "secret" text into the "Enter code given by website" field in Authy, right above the "Add Account" button. Once entered, click the “Add account” button.
<p>Be sure to verify that the text pasted into the "Enter Code" field is exactly the same as the "secret" text in the token. (If these differ, even by only a single character, the one-time passwords that Authy generates will not work with Savio.)
<p><img alt="Screenshot: Authy Enter Code" class="media-element file-default image-style-none" data-delta="1" typeof="foaf:Image" height="500" src="../../../../img/authy-step18.png"></p></p></li>
<li>On the next screen, select a logo for your new Authy account and enter a name for that account. ("Savio" - or any similar name - is a reasonable option for an account name.) Then click "Done".
<p><img alt="Screenshot: Authy Select Logo and Name" class="media-element file-default image-style-none" data-delta="1" typeof="foaf:Image" src="../../../../img/authy-step19.png"></p></li>
<li><p>Select the arrow in the upper left to to move from the Settings screen, to the screen where you can generate one-time passwords.</p></li>
<li>On the screen where you can generate OTPs, click on the logo (or name) for the account you just created:
<p><img alt="Screenshot: Click Savio Name" class="media-element file-default image-style-none" data-delta="1" typeof="foaf:Image" src="../../../../img/authy-step21.png"></p></li>
<li>You should now see one-time passwords being generated: a new one will be displayed every 30 seconds:
<p><img alt="Screenshot: New Password Generation" class="media-element file-default image-style-none" data-delta="1" typeof="foaf:Image" src="../../../../img/authy-step22.png"></p>
<p>(Authy displays the one-time password with a space between the first and last three digits. When you click the "Copy" button, however, the password is correctly copied to the clipboard without that space.)</p></li>
</ol><p>Assuming the "secret" text you pasted into Authy's "Enter Code" field in step 18, above, was the correct text from your token, you've now successfully completed the process of setting up Authy to generate one-time passwords for Savio.</p>

### Logging Into Savio

<p>When you want to log into Savio:</p>
<ol><li><p>Use your terminal or SSH application to connect to <code>hpc.brc.berkeley.edu</code></p></li>
<li>At Savio's Password: prompt, enter your token PIN (do not press Return/Enter to add another line).*.
<p>*reminder that you will not see the text you enter into the terminal</p></li>
<li><p>Click Authy's "Copy" button to copy the one-time password to the Clipboard.</p></li>
<li><p>Then, at Savio's Password: prompt, immediately following the token PIN that you've already entered, paste in the one-time password from Authy and press Return/Enter.</p></li>
</ol><p>(For more details on logging in, please see the <a href="https://docs-research-it.berkeley.edu/services/high-performance-computing/user-guide/logging-brc-clusters/">Logging into Savio</a> documentation.)</p>

### Launching the Authy App

<p>To launch the Authy app later on, it will need to be installed on the device you are using to access Savio. Please refer to Step 1 for installing Authy on additional devices.</p>
