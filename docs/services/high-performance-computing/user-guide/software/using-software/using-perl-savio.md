---
title: Using Perl on Savio
keywords: high performance computing, Savio, Perl
tags: [hpc, example, script]
---


# Using Perl on Savio

This document describes how to use Perl on the Savio high-performance computing cluster at the University of California, Berkeley.

## Running Perl

Unlike most software on Savio, to use Perl, you do not need to load a module. Just call `perl` from the shell prompt.

## Installing Perl modules

You can install Perl modules as a non-root user in your home directory using CPAN as follows.

First check to see if you already have `cpanm` installed.

```
which cpanm
```

If `cpanm` is installed, you should see this result:

```
~/perl5/bin/cpanm
```

If `cpanm` is installed you can install Perl modules like this (here installing the `MatrixReal` module as an example):

```
cpanm Math::MatrixReal
```

If `cpanm` is not installed, you'll see this and you'll need to install it as shown below.

```
/usr/bin/which: no cpanm...
```

If you don't already have `cpanm`, you can install it as follows:

```
# Commands from: https://stackoverflow.com/questions/2980297/how-can-i-use-cpan-as-a-non-root-user)
wget -O- http://cpanmin.us | perl - -l ~/perl5 App::cpanminus local::lib
eval `perl -I ~/perl5/lib/perl5 -Mlocal::lib`
echo 'eval `perl -I ~/perl5/lib/perl5 -Mlocal::lib`' >> ~/.profile
echo 'export MANPATH=$HOME/perl5/man:$MANPATH' >> ~/.profile
```


