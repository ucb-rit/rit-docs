---
title: Software Available on Savio
keywords: high performance computing, berkeley research computing
tags: [hpc, software, modules]
---

## Overview

Savio provides a variety of software installed by the system administrators and BRC consultants, including compilers, interpreters, and tools for statistical analysis, data visualization, bioinformatics, computational biology, and other sciences. You can access much of the software available on the Savio cluster using [Environment Module commands](#accessing-software-using-environment-modules). These commands allow you to display currently loaded and [available software packages](#software-provided-on-savio) as well as to  load and unload the packages and their associated runtime environments.

With the Rocky Linux 8 OS upgrade on Savio (July 2024), the Software Module Farm (SMF) is now managed by the <a href="https://lmod.readthedocs.io/en/latest/index.html" target="_blank">Lmod</a> Environment Module system to set the appropriate environmental variables in your shell needed to make use of the individual software packages. In addition to previously available `module` commands such as `module avail`, `module show`, `module load`, and `module unload`, a new useful command is now provided, `module spider` (see below), which helps in finding a software module with instructions on any additional modules that you may need to load before loading a particular module. 

The Environment Module system allows users to manage their runtime environments dynamically on the Savio cluster. This is accomplished by loading and unloading modulefiles which contain the application specific information for setting a user’s environment, primarily the shell environment variables, such as `PATH`, `LD_LIBRARY_PATH`, etc. The module system is useful in managing potentially conflicting applications, as well as different versions of the same application, especially in a shared computing environment.

In addition to the software provided on the Savio cluster you are welcome to <a href="../installing-software">install your own software</a>.

## Accessing Software Using Environment Modules

All of the module commands begin with `module` and are followed by a subcommand. (In this list of commands, a vertical bar (`|`) means “or”, e.g., `module add` and `module load` are equivalent. You'll need to substitute actual modulefile names for `modulefile`, `modulefile1`, and `modulefile2` in the examples below.)

*   `module avail|av` - List all available modulefiles in the current `MODULEPATH` (i.e. available to you).
*   `module list` - List modules loaded into your environment.
*   `module add|load `_`modulefile`_` ...` - Load modulefile(s) into your environment.
*   `module rm|unload `_`modulefile`_` ...` - Remove modulefile(s) from your environment.
*   `module swap|switch [`_`modulefile1`_`] `_`modulefile2`_` `- Switch loaded _`modulefile1`_ with _`modulefile2`_.
*   `module show|display `_`modulefile`_` ...` - Display configuration information about the specified modulefile(s).
*   `module overview|ov` - Display the number of modules for each name.
*   `module whatis [`_`modulefile`_` ...]` - Display summary information about the specified modulefile(s).
*   `module keyword [`_`key`_` ...]` - Searches any help message or 'module whatis' description for the key(s) given on the command line; search for all possible modules matching any of the "keys".
*   `module purge` - Unload all loaded modulefiles.

For more detailed usage instructions for the `module` command please run `man module` on the cluster.

### Module Spider

With the Rocky Linux 8 OS upgrade on Savio and the newly configured (as of July 2024) Lmod Environment Module system, a new useful module command is now available to Savio users called `module spider`, whereby the command `module spider SOFTWARE` lists the module(s) named `SOFTWARE` and information on additional modules that you may need to load before `SOFTWARE` is available to load.

For example:

```shell
[username@ln003 ~]$ module spider hdf5

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  hdf5: hdf5/1.14.3
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    You will need to load all module(s) on any one of the lines below before the "hdf5/1.14.3" module is available to load.

      gcc/10.5.0  openmpi/4.1.3
      gcc/10.5.0  openmpi/4.1.6
      gcc/11.4.0  openmpi/4.1.3
      gcc/11.4.0  openmpi/4.1.6
      gcc/13.2.0  openmpi/4.1.3
      gcc/13.2.0  openmpi/4.1.6
      intel-oneapi-compilers/2023.1.0  intel-oneapi-mpi/2021.10.0
 
    Help:
      HDF5 is a data model, library, and file format for storing and managing
      data. It supports an unlimited variety of datatypes, and is designed for
      flexible and efficient I/O and for high volume and complex data.
```

This means that you will need to load the appropriate compiler + mpi library combination before being able to load the corresponding hdf5 module. For example, you can do the following:

```shell
[username@ln003 ~]$ module load gcc/13.2.0
[username@ln003 ~]$ module load openmpi/4.1.6
[username@ln003 ~]$ module load hdf5
[username@ln003 ~]$ module list

Currently Loaded Modules:
  1) emacs/29.1   2) sq/0.1.0   3) gcc/13.2.0   4) openmpi/4.1.6   5) hdf5/1.14.3
```

### Default Modules

Using `module load` command without specifying the version number loads the default version of the package. 
As of November 2022, the default version of a given package will generally be the most recent version that Savio has installed. As of July 2024, the default version of a given package will also be labeled with a (D), and a currently loaded module will be labeled with an (L).

We strongly recommend that users explicitly provide the version number following the package name when using `module load`, e.g.,

```shell
module load gcc/13.2.0
```

This improves reproducibility and (for compiler-related modules) ensures that loaded compilers are compatible wth your code. 


### Modules Organized in a Hierarchical Structure

 Savio uses a hierarchical structure to manage module dependencies, so some modulefiles will only be available after their dependent modulefile is loaded. For example, C libraries will only become available after their parent C compiler has been loaded. Here we can see the modules that are visible only after loading `gcc`:
 
```shell 
[username@ln002 ~]$ module load gcc/13.2.0
[username@ln002 ~]$ module avail

-------------------------------------------------------------------------------- /global/software/rocky-8.x86_64/modfiles/gcc/13.2.0 --------------------------------------------------------------------------------
   antlr/2.7.7    cuda/12.2.1         (D)    geos/3.12.0    intel-oneapi-tbb/2021.10.0    openblas/0.3.24    openmpi/4.1.6  (D)
   cuda/11.8.0    cudnn/8.7.0.84-11.8        gsl/2.7.1      netlib-lapack/3.11.0          openmpi/4.1.3      udunits/2.2.28

---------------------------------------------------------------------------------- /global/software/rocky-8.x86_64/modfiles/langs -----------------------------------------------------------------------------------
   anaconda3/2024.02-1-11.4    java/22.0.1          openjdk/11.0.20.1_1-gcc-11.4.0        perl/5.38.0-gcc-11.4.0       python/3.11.6-gcc-11.4.0 (D)    r/4.4.0-gcc-11.4.0
   intelpython/3.9.19          julia/1.10.2-11.4    openjdk/17.0.8.1_1-gcc-11.4.0  (D)    python/3.10.12-gcc-11.4.0    r-spatial/4.4.0                 rust/1.70.0-gcc-11.4.0

---------------------------------------------------------------------------------- /global/software/rocky-8.x86_64/modfiles/tools -----------------------------------------------------------------------------------
   automake/1.16.5       eigen/3.4.0        gmake/4.4.1             lmdb/0.9.31                      matlab/r2024a            proj/9.2.1                      snappy/1.1.10        texlive/2024
   awscli/1.29.41        emacs/29.1  (L)    gnuplot/6.0.1           m4/1.4.19                        mercurial/6.4.5          protobuf/3.24.3                 spack/0.21.1         tmux/3.3a
   bazel/6.1.1           ffmpeg/6.0         gurobi/10.0.0           mathematica/13.0.1/13.0.1        nano/7.2                 qt/5.15.11                      sq/0.1.0      (L)    unixodbc/2.3.4
   cmake/3.27.7          gdal/3.7.3         imagemagick/7.1.1-11    matlab/r2022a             (D)    ninja/1.11.1             rclone/1.63.1                   swig/4.1.1           vim/9.0.0045
   code-server/4.12.0    glog/0.6.0         leveldb/1.23            matlab/r2023a                    parallel/20220522 (D)    rstudio-server/2024.04.2-764    tcl/8.6.12

-------------------------------------------------------------------------------- /global/software/rocky-8.x86_64/modfiles/compilers ---------------------------------------------------------------------------------
   gcc/10.5.0    gcc/11.4.0 (D)    gcc/13.2.0 (L)    intel-oneapi-compilers/2023.1.0    llvm/17.0.4    nvhpc/23.9


  Where:
   L:  Module is loaded
   D:  Default Module

If the avail list is too long consider trying:

"module --default avail" or "ml -d av" to just list the default modules.
"module overview" or "ml ov" to display the number of modules for each name.

Use "module spider" to find all possible modules and extensions.
Use "module keyword key1 key2 ..." to search for all possible modules matching any of the "keys".
```

To view a list of all modules including those only visible after loading their dependencies enter the following command:

```shell
find /global/software/rocky-8.x86_64/modfiles -type d -exec ls -d {} \;
```
Using this command, in combination with the `grep` command, can also be used to search for and find individual software modules, as in the following example:

```shell
[username@ln002 ~]$ find /global/software/rocky-8.x86_64/modfiles -type d -exec ls -d {} \; | grep hdf5
/global/software/rocky-8.x86_64/modfiles/openmpi/4.1.6-fvizfdj/gcc/13.2.0/hdf5
/global/software/rocky-8.x86_64/modfiles/openmpi/4.1.3-lm66wba/gcc/10.5.0/hdf5
/global/software/rocky-8.x86_64/modfiles/openmpi/4.1.6-xyjyowu/gcc/10.5.0/hdf5
/global/software/rocky-8.x86_64/modfiles/openmpi/4.1.3-o4cey66/gcc/11.4.0/hdf5
/global/software/rocky-8.x86_64/modfiles/openmpi/4.1.3-le36rfg/gcc/13.2.0/hdf5
/global/software/rocky-8.x86_64/modfiles/openmpi/4.1.6-kk4tbvr/gcc/11.4.0/hdf5
/global/software/rocky-8.x86_64/modfiles/intel-oneapi-mpi/2021.10.0-fyiu4qb/oneapi/2023.1.0/hdf5
```

Compare this with the `module spider hdf5` example in an earlier section of this page on the use of the `module spider` command.

### Example Usage

It can be helpful to try out each of the following examples in sequence to more fully understand how environment modules work. Please note that the output here may vary from your own depending on module updates and your particular access privileges. 

```shell
[username@ln002 ~]$ module purge
[username@ln002 ~]$ module avail

------------------------------------------------------------------------------------------------- /global/software/rocky-8.x86_64/modfiles/langs --------------------------------------------------------------------------------------------------
   anaconda3/2024.02-1-11.4    java/22.0.1          openjdk/11.0.20.1_1-gcc-11.4.0        perl/5.38.0-gcc-11.4.0       python/3.11.6-gcc-11.4.0 (D)    r/4.4.0-gcc-11.4.0
   intelpython/3.9.19          julia/1.10.2-11.4    openjdk/17.0.8.1_1-gcc-11.4.0  (D)    python/3.10.12-gcc-11.4.0    r-spatial/4.4.0                 rust/1.70.0-gcc-11.4.0

------------------------------------------------------------------------------------------------- /global/software/rocky-8.x86_64/modfiles/tools --------------------------------------------------------------------------------------------------
   automake/1.16.5    code-server/4.12.0    gdal/3.7.3       gurobi/10.0.0           m4/1.4.19                 matlab/r2023a      ninja/1.11.1         qt/5.15.11                      spack/0.21.1    texlive/2024
   awscli/1.29.41     eigen/3.4.0           glog/0.6.0       imagemagick/7.1.1-11    mariadb/10.8.2     (D)    matlab/r2024a      parallel/20220522    rclone/1.63.1                   sq/0.1.0        tmux/3.3a
   bazel/6.1.1        emacs/29.1            gmake/4.4.1      leveldb/1.23            mathematica/13.0.1        mercurial/6.4.5    proj/9.2.1           rstudio-server/2024.04.2-764    swig/4.1.1      unixodbc/2.3.4
   cmake/3.27.7       ffmpeg/6.0            gnuplot/6.0.1    lmdb/0.9.31             matlab/r2022a      (D)    nano/7.2           protobuf/3.24.3      snappy/1.1.10                   tcl/8.6.12      vim/9.0.0045

----------------------------------------------------------------------------------------------- /global/software/rocky-8.x86_64/modfiles/compilers ------------------------------------------------------------------------------------------------
   boost/1.83.0    gcc/10.5.0    gcc/11.4.0 (D)    gcc/13.2.0    intel-oneapi-compilers/2023.1.0    llvm/17.0.4    mariadb/10.8.2    nvhpc/23.9

-------------------------------------------------------------------------------------------------- /global/software/rocky-8.x86_64/modfiles/apps --------------------------------------------------------------------------------------------------
   bio/abricate/1.0.1-jgrg                 bio/blast-plus/2.14.1-gcc-11.4.0 (D)    bio/hmmer/3.4-gcc-11.4.0          bio/paup/4.0a                    bio/samtools/1.17-gcc-11.4.0       ml/pytorch/2.0.1-py3.11.7
   bio/abricate/1.0.1               (D)    bio/bowtie2/2.5.1-gcc-11.4.0            bio/idba/1.1.3-gcc-11.4.0         bio/phylotool/u20.04             bio/snippy/4.6.0                   ml/pytorch/2.3.1-py3.11.7     (D)
   bio/bcftools/1.16-gcc-11.4.0            bio/ezclermont/0.7.0                    bio/integron_finder/2.0.2         bio/picard/3.0.0-gcc-11.4.0      bio/trimmomatic/0.39-gcc-11.4.0    ml/tensorflow/2.14.0-py3.9.0
   bio/beast/2.6.4                         bio/fastqc/0.12.1-gcc-11.4.0            bio/kallisto/0.48.0-gcc-11.4.0    bio/prodigal/2.6.3-gcc-11.4.0    bio/unicycler/0.5.0                ml/tensorflow/2.15.0-py3.10.0 (D)
   bio/blast-plus/2.13.0-gcc-11.4.0        bio/grace/5.1.25-gcc-11.4.0             bio/mlst/2.19.0                   bio/relion/4.0.1-gcc-11.4.0      bio/vcftools/0.1.16-gcc-11.4.0     ms/ambertools/2022

  Where:
   D:  Default Module

If the avail list is too long consider trying:

"module --default avail" or "ml -d av" to just list the default modules.
"module overview" or "ml ov" to display the number of modules for each name.

Use "module spider" to find all possible modules and extensions.
Use "module keyword key1 key2 ..." to search for all possible modules matching any of the "keys".

[username@ln002 ~]$ module list
No modules loaded

[username@ln002 ~]$ module load gcc/13.2.0  openmpi/4.1.6
[username@ln002 ~]$ module load intel-oneapi-mkl
[username@ln002 ~]$ module list

Currently Loaded Modules:
  1) gcc/13.2.0   2) openmpi/4.1.6   3) intel-oneapi-tbb/2021.10.0   4) intel-oneapi-mkl/2023.2.0

[username@ln002 ~]$ module unload openmpi

Inactive Modules:
  1) intel-oneapi-mkl     2) intel-oneapi-tbb/2021.10.0

[username@ln002 ~]$ module list

Currently Loaded Modules:
  1) gcc/13.2.0

Inactive Modules:
  1) intel-oneapi-tbb/2021.10.0   2) intel-oneapi-mkl

[username@ln002 ~]$ module switch intel-oneapi-mkl netlib-lapack

[username@ln002 ~]$ module list

Currently Loaded Modules:
  1) gcc/13.2.0   2) netlib-lapack/3.11.0

Inactive Modules:
  1) intel-oneapi-tbb/2021.10.0

[username@ln002 ~]$ module spider intel-oneapi-mkl/2023.2.0

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  intel-oneapi-mkl: intel-oneapi-mkl/2023.2.0
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    You will need to load all module(s) on any one of the lines below before the "intel-oneapi-mkl/2023.2.0" module is available to load.

      gcc/10.5.0
      gcc/10.5.0  openmpi/4.1.6
      gcc/11.4.0
      gcc/11.4.0  openmpi/4.1.3
      gcc/11.4.0  openmpi/4.1.6
      gcc/13.2.0  openmpi/4.1.6
      intel-oneapi-compilers/2023.1.0  intel-oneapi-mpi/2021.10.0
 
    Help:
      Intel oneAPI Math Kernel Library (Intel oneMKL; formerly Intel Math
      Kernel Library or Intel MKL), is a library of optimized math routines
      for science, engineering, and financial applications. Core math
      functions include BLAS, LAPACK, ScaLAPACK, sparse solvers, fast Fourier
      transforms, and vector math. LICENSE INFORMATION: By downloading and
      using this software, you agree to the terms and conditions of the
      software license agreements at https://intel.ly/393CijO.

[username@ln002 ~]$ module load openmpi/4.1.6

Activating Modules:
  1) intel-oneapi-tbb/2021.10.0

[username@ln002 ~]$ module show intel-oneapi-mkl/2023.2.0
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   /global/software/rocky-8.x86_64/modfiles/openmpi/4.1.6-fvizfdj/gcc/13.2.0/intel-oneapi-mkl/2023.2.0.lua:
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
whatis("Name : intel-oneapi-mkl")
whatis("Version : 2023.2.0")
whatis("Target : x86_64")
whatis("Short description : Intel oneAPI Math Kernel Library (Intel oneMKL; formerly Intel Math Kernel Library or Intel MKL), is a library of optimized math routines for science, engineering, and financial applications. Core math functions inc
lude BLAS, LAPACK, ScaLAPACK, sparse solvers, fast Fourier transforms, and vector math.")
help([[Name   : intel-oneapi-mkl]])
help([[Version: 2023.2.0]])
help([[Target : x86_64]])
]])
help([[Intel oneAPI Math Kernel Library (Intel oneMKL; formerly Intel Math
Kernel Library or Intel MKL), is a library of optimized math routines
for science, engineering, and financial applications. Core math
functions include BLAS, LAPACK, ScaLAPACK, sparse solvers, fast Fourier
transforms, and vector math. LICENSE INFORMATION: By downloading and
using this software, you agree to the terms and conditions of the
software license agreements at https://intel.ly/393CijO.]])
depends_on("intel-oneapi-tbb/2021.10.0")
depends_on("openmpi/4.1.6")
prepend_path("PKG_CONFIG_PATH","/global/software/rocky-8.x86_64/gcc/linux-rocky8-x86_64/gcc-13.2.0/intel-oneapi-mkl-2023.2.0-veuv2uad3z7ooy66nh4a676vs66vjzmg/lib/pkgconfig")
prepend_path("CMAKE_PREFIX_PATH","/global/software/rocky-8.x86_64/gcc/linux-rocky8-x86_64/gcc-13.2.0/intel-oneapi-mkl-2023.2.0-veuv2uad3z7ooy66nh4a676vs66vjzmg/.")
prepend_path("CPATH","/global/software/rocky-8.x86_64/gcc/linux-rocky8-x86_64/gcc-13.2.0/intel-oneapi-mkl-2023.2.0-veuv2uad3z7ooy66nh4a676vs66vjzmg/mkl/2023.2.0/include")
prepend_path("LD_LIBRARY_PATH","/global/software/rocky-8.x86_64/gcc/linux-rocky8-x86_64/gcc-13.2.0/intel-oneapi-mkl-2023.2.0-veuv2uad3z7ooy66nh4a676vs66vjzmg/mkl/2023.2.0/lib/intel64")
prepend_path("LIBRARY_PATH","/global/software/rocky-8.x86_64/gcc/linux-rocky8-x86_64/gcc-13.2.0/intel-oneapi-mkl-2023.2.0-veuv2uad3z7ooy66nh4a676vs66vjzmg/mkl/2023.2.0/lib/intel64")
setenv("MKLROOT","/global/software/rocky-8.x86_64/gcc/linux-rocky8-x86_64/gcc-13.2.0/intel-oneapi-mkl-2023.2.0-veuv2uad3z7ooy66nh4a676vs66vjzmg/mkl/2023.2.0")
prepend_path("NLSPATH","/global/software/rocky-8.x86_64/gcc/linux-rocky8-x86_64/gcc-13.2.0/intel-oneapi-mkl-2023.2.0-veuv2uad3z7ooy66nh4a676vs66vjzmg/mkl/2023.2.0/lib/intel64/locale/%l_%t/%N")
prepend_path("PATH","/global/software/rocky-8.x86_64/gcc/linux-rocky8-x86_64/gcc-13.2.0/intel-oneapi-mkl-2023.2.0-veuv2uad3z7ooy66nh4a676vs66vjzmg/mkl/2023.2.0/bin/intel64")
prepend_path("PKG_CONFIG_PATH","/global/software/rocky-8.x86_64/gcc/linux-rocky8-x86_64/gcc-13.2.0/intel-oneapi-mkl-2023.2.0-veuv2uad3z7ooy66nh4a676vs66vjzmg/mkl/2023.2.0/lib/pkgconfig")

[username@ln002 ~]$ module whatis intel-oneapi-mkl
intel-oneapi-mkl/2023.2.0                           : Name : intel-oneapi-mkl
intel-oneapi-mkl/2023.2.0                           : Version : 2023.2.0
intel-oneapi-mkl/2023.2.0                           : Target : x86_64
intel-oneapi-mkl/2023.2.0                           : Short description : Intel oneAPI Math Kernel Library (Intel oneMKL; formerly Intel Math Kernel Library or Intel MKL), is a library of optimized math routines for science, engineering, and f
inancial applications. Core math functions include BLAS, LAPACK, ScaLAPACK, sparse solvers, fast Fourier transforms, and vector math.

[myashar@ln002 ~]$ module list

Currently Loaded Modules:
  1) gcc/13.2.0   2) netlib-lapack/3.11.0   3) openmpi/4.1.6   4) intel-oneapi-tbb/2021.10.0

[username@ln002 ~]$ module purge
[username@ln002 ~]$ module list
No modules loaded

[username@ln002 ~]$ module avail

------------------------------------------------------------------------------------------------- /global/software/rocky-8.x86_64/modfiles/langs --------------------------------------------------------------------------------------------------
   anaconda3/2024.02-1-11.4    java/22.0.1          openjdk/11.0.20.1_1-gcc-11.4.0        perl/5.38.0-gcc-11.4.0       python/3.11.6-gcc-11.4.0 (D)    r/4.4.0-gcc-11.4.0
   intelpython/3.9.19          julia/1.10.2-11.4    openjdk/17.0.8.1_1-gcc-11.4.0  (D)    python/3.10.12-gcc-11.4.0    r-spatial/4.4.0                 rust/1.70.0-gcc-11.4.0

------------------------------------------------------------------------------------------------- /global/software/rocky-8.x86_64/modfiles/tools --------------------------------------------------------------------------------------------------
   automake/1.16.5    code-server/4.12.0    gdal/3.7.3       gurobi/10.0.0           m4/1.4.19                 matlab/r2023a      ninja/1.11.1         qt/5.15.11                      spack/0.21.1    texlive/2024
   awscli/1.29.41     eigen/3.4.0           glog/0.6.0       imagemagick/7.1.1-11    mariadb/10.8.2     (D)    matlab/r2024a      parallel/20220522    rclone/1.63.1                   sq/0.1.0        tmux/3.3a
   bazel/6.1.1        emacs/29.1            gmake/4.4.1      leveldb/1.23            mathematica/13.0.1        mercurial/6.4.5    proj/9.2.1           rstudio-server/2024.04.2-764    swig/4.1.1      unixodbc/2.3.4
   cmake/3.27.7       ffmpeg/6.0            gnuplot/6.0.1    lmdb/0.9.31             matlab/r2022a      (D)    nano/7.2           protobuf/3.24.3      snappy/1.1.10                   tcl/8.6.12      vim/9.0.0045

----------------------------------------------------------------------------------------------- /global/software/rocky-8.x86_64/modfiles/compilers ------------------------------------------------------------------------------------------------
   boost/1.83.0    gcc/10.5.0    gcc/11.4.0 (D)    gcc/13.2.0    intel-oneapi-compilers/2023.1.0    llvm/17.0.4    mariadb/10.8.2    nvhpc/23.9

-------------------------------------------------------------------------------------------------- /global/software/rocky-8.x86_64/modfiles/apps --------------------------------------------------------------------------------------------------
   bio/abricate/1.0.1-jgrg                 bio/blast-plus/2.14.1-gcc-11.4.0 (D)    bio/hmmer/3.4-gcc-11.4.0          bio/paup/4.0a                    bio/samtools/1.17-gcc-11.4.0       ml/pytorch/2.0.1-py3.11.7
   bio/abricate/1.0.1               (D)    bio/bowtie2/2.5.1-gcc-11.4.0            bio/idba/1.1.3-gcc-11.4.0         bio/phylotool/u20.04             bio/snippy/4.6.0                   ml/pytorch/2.3.1-py3.11.7     (D)
   bio/bcftools/1.16-gcc-11.4.0            bio/ezclermont/0.7.0                    bio/integron_finder/2.0.2         bio/picard/3.0.0-gcc-11.4.0      bio/trimmomatic/0.39-gcc-11.4.0    ml/tensorflow/2.14.0-py3.9.0
   bio/beast/2.6.4                         bio/fastqc/0.12.1-gcc-11.4.0            bio/kallisto/0.48.0-gcc-11.4.0    bio/prodigal/2.6.3-gcc-11.4.0    bio/unicycler/0.5.0                ml/tensorflow/2.15.0-py3.10.0 (D)
   bio/blast-plus/2.13.0-gcc-11.4.0        bio/grace/5.1.25-gcc-11.4.0             bio/mlst/2.19.0                   bio/relion/4.0.1-gcc-11.4.0      bio/vcftools/0.1.16-gcc-11.4.0     ms/ambertools/2022

  Where:
   D:  Default Module

If the avail list is too long consider trying:

"module --default avail" or "ml -d av" to just list the default modules.
"module overview" or "ml ov" to display the number of modules for each name.

Use "module spider" to find all possible modules and extensions.
Use "module keyword key1 key2 ..." to search for all possible modules matching any of the "keys".



```

### Bioinformatics Software

All Savio users can use the bioinformatics software available system-wide, using the commands discussed in previous sections and looking for `bio/XYZ` (e.g., `bio/bowtie2`) in the `/global/software/rocky-8.x86_64/modfiles/app` set of modules.

## Software Provided on Savio

Research IT provides and maintains a set of broadly used and general purpose software modules. Our aim is to provide an ecosystem that most users can rely on to accomplish their research and studies. 

For a comprehensive and up-to-date list of software provided on the cluster, run the `module avail` command [as described above](#accessing-software-using-environment-modules), or `find /global/software/rocky-8.x86_64/modfiles -type d -exec ls -d {} \;`.

<table align="center" border="1" cellpadding="0" cellspacing="0">
<thead>
<tr>
<th style="text-align:center">Category</th>
<th style="text-align:center">Application/Library Name</th>
</tr>
</thead>
<tbody>
<tr style="background-color:#D3D3D3">
<td align="center" colspan="2">Development Tools</td>
</tr>
<tr>
<th scope="row">Editor/IDE</th>
<td>Emacs, Vim, Nano, make, cmake, gmake, bazel, spack, automake, swig, Ninja, cscope, ctags, Jupyter, rstudio-server</td>
</tr>
<tr>
<th scope="row">SCM</th>
<td>Git, Mercurial</td>
</tr>
<tr>
<th scope="row">Debugger/Profiler/Tracer</th>
<td>GDB, Valgrind, time, Allinea DDT</td>
</tr>
<tr>
<th scope="row">Benchmarking</th>
<td>HPL, IOR, OSU Micro-Benchmarks, </td>
</tr>
<tr>
<th scope="row">Languages/Platforms & Compilers</th>
<td>Anaconda, Apptainer, GCC, Intel, LLVM, mariadb, Mathematica, oneAPI, Perl, Python / IPython, Java, OpenJDK, Boost, CUDA, Open MPI, TBB, MPI4Py, R, Rstudio, MATLAB, Julia, Qt, Rust, Tcl</td>
</tr>
<tr>
<th scope="row">Math & Statistics Libraries</th>
<td>MKL, CUDNN, FFTW, FFTW3, GEOS, GSL, LAPACK, MUMPS, PETSc, ScaLAPACK, NumPy, OpenBLAS, SciPy, Eigen, Gurobi, proj</td>
</tr>
<tr>
<th scope="row">I/O Libraries</th>
<td>ADIOS, HDF5, LevelDB, NetCDF, NCO, NCL, protobuf, glog, snappy</td>
</tr>
<tr>
<th scope="row">Macro Processing & Text Processing</th>
<td>ant, ANTLR, GNU M4</td>
</tr>
<tr>
<th scope="row">System Utilities</th>
<td>ant, iperf3, GNU Parallel, time, tmux, udunits</td>
</tr>
<tr>
<th scope="row">Database Connectivity</th>
<td>sqlite, unixodbc</td>
</tr>
<tr style="background-color:#D3D3D3">
<td align="center" colspan="2">Data processing, Visualization, and Machine Learning Tools</td>
</tr>
<tr>
<th scope="row">Data Processing/Visualization</th>
<td>FFmpeg, GDAL, Gnuplot, Grace, ImageMagick, MATLAB, protobuf, Python / IPython, R, Matplotlib</td>
</tr>
<tr>
<th scope="row">Machine Learning</th>
<td>Tensorflow, Keras, CUDNN, PyTorch, Scikit-learn</td>
</tr>
<tr>
<th scope="row">Bioinformatics</th>
<td>ABRicate, AmberTools, BCFtools, BEAST, BLAST, Bowtie, cutadapt, EzClermont, FastQC, GROMACS, HMMER, IDBA, Integron_Finder, kallisto, mlst, PAUP, phylotools, Picard, Prodigal, RELION, SAMtools, Snippy, Trimmomatic, Unicycler, VCFtools</td>
</tr>
<tr>
<tr style="background-color:#D3D3D3">
<td align="center" colspan="2">Scientific Computing Software</td>
</tr>
<tr>
<th scope="row">Chemistry & Computational Materials Science</th>
<td>BerkeleyGW, LAMMPS, ORCA, quantum-espresso</td>
</tr>
<tr>
<tr style="background-color:#D3D3D3">
<td align="center" colspan="2">Access & File Transfer Tools</td>
</tr>
<tr>
<th scope="row">Data Transfer/Sync Tools</th>
<td>sftp, lftp, rclone, scp, curl, wget, awscli, rsync, globus, ucx, iperf3</td>
</tr>
<tr>
<tr style="background-color:#D3D3D3">
<td align="center" colspan="2">Typesetting and Publishing Tools</td>
</tr>
<tr>
<th scope="row">Typesetting</th>
<td>TeX Live, Doxygen</td>
</tr>
</tbody>
</table>

## Requesting New Modules or Updates

Software installed within the Environment Module system must meet the requirements specified under <a href="../installing-software#requirements-for-software-on-savio">"Installing Software"</a>. Modules are updated at most biannually, and we request that you install and maintain packages that require more frequent updates yourself.

To request additional software be installed or existing packages to be upgraded please use our [Software Request Form](https://docs.google.com/forms/d/e/1FAIpQLSeBR2MsIEgzPq9yvamqYpABBDYZbFccUx85ZjRD1E87s327Ig/viewform?usp=sf_link). Expect to be contacted regarding your request within three business days (except academic holidays).
